package Bloco1.com.company;

public class Bloco1 {

    //Exercício 1_1
    public static double percentagemDeRapazes(int rapazes, int raparigas) {
        //Estrutura de dados
        double total, percentagem;
        //Processamento
        total = rapazes + raparigas;
        percentagem = (rapazes / total) * 100;
        return percentagem;
    }

    public static double percentagemDeRaparigas(int rapazes, int raparigas) {
        //Estrutura de dados
        double total, percentagem;
        //Processamento
        total = rapazes + raparigas;
        percentagem = (raparigas / total) * 100;
        return percentagem;
    }

    //Exercício 2_1
    public static double precoTotal(int rosas, int tulipas, double precoRosa, double precoTulipa) {
        //Estrutura de dados
        double preco;
        //Processamento
        preco = (rosas * precoRosa) + (tulipas * precoTulipa);
        return preco;
    }

    //Exercício 3_1
    public static double volumeCilindro(double raio, double altura) {
        //Estrutura de dados
        double volume, conversaoParaLitros;
        //Processamento
        volume = Math.PI * Math.pow(raio, 2) * altura;
        conversaoParaLitros = volume * 1000;
        return conversaoParaLitros;
    }

    //Exercício 4_1
    public static double kmDeDistancia(double tempoSeg) {
        //Estrutura de dados
        double velocidadeKmHora, velocidadeKmSeg, distancia;
        //Tratamento de dados e processamento
        velocidadeKmHora = 1224;
        velocidadeKmSeg = velocidadeKmHora * 0.000278;
        distancia = tempoSeg * velocidadeKmSeg;
        return distancia;
    }

    //Exercício 5_1
    public static double alturaEdificio(int tempoSeg) {
        //Estrutura de dados
        double distancia, velocidadeInicial, aceleracao;
        velocidadeInicial = 0;
        aceleracao = 9.8; //gravidade = 9.8m/s^^2
        //Processamento
        distancia = velocidadeInicial * tempoSeg + ((aceleracao * Math.pow(tempoSeg, 2) / 2));
        return distancia;
    }

    //Exercício 6_1
    public static double alturaEdificioSombra(double sombraEdificio, double alturaPessoa, double sombraPessoa) {
        //Estrutura de dados
        double altEdi;
        //Processamento
        altEdi = (sombraEdificio / sombraPessoa) * alturaPessoa;
        return altEdi;
    }

    //Exercicio 7_1
    public static double corrida(double distCompleta, double tempoTotal, double tempoParcial) {
        //Estrutura de dados
        double distParcial, velocidadeAmbos;
        //Processamento
        velocidadeAmbos = distCompleta / tempoTotal;
        distParcial = tempoParcial * velocidadeAmbos;
        return distParcial;
    }

    //Exercicio 8_1
    public static double leiCossenosCompDeUmLado (double compAC, double compBC, double angulo){
        //Estrutura de dados
        double compAB, anguloRad;
        //Processamento
        anguloRad = Math.toRadians(angulo);
        compAB = Math.sqrt(Math.pow(compAC, 2) + Math.pow(compBC, 2) - (2 * compAC * compBC) * Math.cos(anguloRad));
        return compAB;
    }

    //Exercício 9_1
    public static double perimetroRectangulo (double a, double b){
        //Estrutura de dados
        double perimetro;
        //Processamento
        perimetro = (2 * a) + (2 * b);
        return perimetro;
    }

    //Exercício 10_1
    public static double hipotenusaTrianguloRectangulo (double cateto1, double cateto2) {
        //Estrutura de dados
        double hipotenusa;
        //Processamento
        hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
        return hipotenusa;
    }

    //Exercício 11_1
    public static double formulaResolvente1 (double b, double c){
        //Estrutura de dados
        double a, radicando, resultado;
        //Processamento
        a = 1;
        radicando = Math.pow(Math.abs(b), 2) - (4 * a * c);
        resultado = ((-b) + Math.sqrt(radicando)) / (2 * a);

        return resultado;
    }
    public static double formulaResolvente2 (double b, double c){
        //Estrutura de dados
        double a, radicando, resultado;
        //Processamento
        a = 1;
        radicando = Math.pow(Math.abs(b), 2) - (4 * a * c);
        resultado = ((-b) - Math.sqrt(radicando)) / (2 * a);


        return resultado;
    }

    //Exercício 12_1
    public static double celsiusToFahrenheit (double celsius){
        //Estrutura de dados
        double fahrenheit;
        //Processamento
        fahrenheit = 32 + (1.8 * celsius);

        return fahrenheit;
    }

    //Exercício 13_1
    public static double horasMinutosToMinutos (double horas, double minutos){
        //Estrutura de dados
        double minutosTotais, horasToMinutos;
        //Processamento
        horasToMinutos = horas * 60.0;
        minutosTotais = horasToMinutos + minutos;

        return minutosTotais;
    }
}