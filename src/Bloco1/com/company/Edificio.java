package Bloco1.com.company;

public class Edificio {

    private double altura;
    private double velocidadeInicial = 0;
    private double aceleracao = 9.8;

    public Edificio(int tempoSegundo) {
        this.altura = cacularAltura(tempoSegundo);
    }

    public double cacularAltura(int tempoSeg) {
        return velocidadeInicial * tempoSeg + ((aceleracao * Math.pow(tempoSeg, 2) / 2));
    }
}
