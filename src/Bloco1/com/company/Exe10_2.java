package Bloco1.com.company;

import java.util.Scanner;

public class Exe10_2 {
    public static void main (String[] args){

        //estrutura de dados
        double hipotenusa, cateto1, cateto2;

        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Comprimento cateto #1:");
        cateto1 = ler.nextDouble();
        System.out.print("Comprimento cateto #2:");
        cateto2 = ler.nextDouble();

        //processamento
        hipotenusa = Bloco1.hipotenusaTrianguloRectangulo(cateto1, cateto2);

        //Output

        System.out.println("O o comprimento da hipotenusa é " + hipotenusa );
        System.out.print("unidades de medida");
    }
}
