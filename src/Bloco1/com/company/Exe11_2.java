package Bloco1.com.company;

import java.util.Scanner;

public class Exe11_2 {
    public static void main (String[] args){
        //estrutura da dados

        double b, c, resultado1, resultado2;

        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Valor de b:");
        b = ler.nextDouble();
        System.out.print("Valor de c:");
        c = ler.nextDouble();

        //Processamento
        resultado1 = Bloco1.formulaResolvente1(b,c);
        resultado2 = Bloco1.formulaResolvente2(b, c);

        //Output
        System.out.println("a = " + resultado1);
        System.out.println("ou " + resultado2);
    }
}
