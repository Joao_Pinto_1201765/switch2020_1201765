package Bloco1.com.company;

import java.util.Scanner;

public class Exe12_2 {
    public static void main (String[] args){
        //Estrutura de dados
        double fahrenheit, celsius;

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Graus Celsius =");
        celsius = ler.nextDouble();

        //Processamento
        fahrenheit = Bloco1.celsiusToFahrenheit(celsius);

        //Output
        System.out.println("Fahrenheit = " + fahrenheit);
    }
}
