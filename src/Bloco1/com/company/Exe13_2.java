package Bloco1.com.company;

import java.util.Scanner;

public class Exe13_2 {
    public static void main (String[] args){
        //Estrutura de dados
        double horas, minutos, minutosTotais;

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Horas = ");
        horas = ler.nextDouble();
        System.out.print("Minutos = ");
        minutos = ler.nextDouble();

        //Processamento
        minutosTotais = Bloco1.horasMinutosToMinutos(horas, minutos);

        //Output
        System.out.println(minutosTotais + "minutos");
    }
}
