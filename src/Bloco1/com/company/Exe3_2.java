package Bloco1.com.company;

import java.util.Scanner;

public class Exe3_2 {
    public static void main(String[] args) {

        //estrutura de dados
        double raio, altura, volume;

        //leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o raio em metros");
        raio = ler.nextDouble();
        System.out.println("Introduza a altura em metros");
        altura = ler.nextDouble();

        //processamento
        volume = Bloco1.volumeCilindro(raio, altura);

        //output
        System.out.printf("%.2f", volume);
        System.out.print(" Litros");
    }
}
