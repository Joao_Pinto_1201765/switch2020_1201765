package Bloco1.com.company;

import java.util.Scanner;

public class Exe4_2 {
    public static void main(String[] args) {

        //estrutura de dados
        int tempoSeg;
        double distancia;

        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Quanto tempo, em segundos, demorou a ouvir o relâmpago desde que viu o trovão?");
        tempoSeg = ler.nextInt();

        //processamento
        distancia = Bloco1.kmDeDistancia(tempoSeg);

        //saída de dados
        System.out.println("A trovoada encontra-se a:");
        System.out.printf("%.3f", distancia);
        System.out.println(" Km de distância");
    }
}
