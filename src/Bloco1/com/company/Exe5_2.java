package Bloco1.com.company;

import java.util.Scanner;

public class Exe5_2 {
    public static void main(String[] args) {

        //estrutura de dados
        double distancia;
        int tempoSeg;

        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Quanto tempo, em segundos, demorou a pedra a cair no chão?");
        tempoSeg = ler.nextInt();

        //processamento
        distancia = Bloco1.alturaEdificio(tempoSeg);

        //output
        System.out.println("O edifício tem " + distancia + " metros");

    }
}