package Bloco1.com.company;

import java.util.Scanner;

public class Exe6_2 {
    public static void main (String[] args) {

        //estrutura de dados
        double alturaEdificio, sombraEdificio, alturaPessoa, sombraPessoa;

        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Quanto é, em metros, a sombra do edifício?");
        sombraEdificio = ler.nextDouble();
        System.out.println("Quanto é, em metros, a altura da pessoa?");
        alturaPessoa = ler.nextDouble();
        System.out.println("Quanto é, em metros, a sombra da pessoa?");
        sombraPessoa = ler.nextDouble();

        //processamento
        alturaEdificio = Bloco1.alturaEdificioSombra(sombraEdificio, alturaPessoa, sombraPessoa);

        //output
        System.out.println("O edifício tem " + alturaEdificio + " metros");
    }
}
