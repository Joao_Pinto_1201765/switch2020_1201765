package Bloco1.com.company;

import java.util.Scanner;

public class Exe7_2 {
    public static void main (String[] args){

        //estrutura de dados e variáveis
        double distParcial, distanciaCompleta, tempoTotal, tempoParcial;

        //entrada de daddos
        Scanner ler = new Scanner(System.in);
        System.out.println("Quanto foi, em metros, a distância total percorrida?");
        distanciaCompleta = ler.nextDouble();
        System.out.println("Quanto foi, em horas, o tempo total?");
        tempoTotal = ler.nextDouble();
        System.out.println("Quanto foi, em horas, o tempo parcial?");
        tempoParcial = ler.nextDouble();

        //processamento
         distParcial = Bloco1.corrida(distanciaCompleta, tempoTotal, tempoParcial);

        //output
        System.out.printf("%.0f", distParcial);
        System.out.print(" metros");
    }
}
