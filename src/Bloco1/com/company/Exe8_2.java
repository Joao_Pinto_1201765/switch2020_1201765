package Bloco1.com.company;

import java.util.Scanner;

public class Exe8_2 {
    public static void main (String[] args){

        //estrutura de dados e variáveis
        double compAC, compBC, compAB, angulo;

        //entrada de daddos
        Scanner ler = new Scanner(System.in);
        System.out.println("Qual é a distância de A a C?");
        compAC = ler.nextDouble();
        System.out.println("Qual é a distância de B a C");
        compBC = ler.nextDouble();
        System.out.println("Quanto é o ângulo A?");
        angulo = ler.nextDouble();

        //processamento
        compAB = Bloco1.leiCossenosCompDeUmLado(compAC, compBC, angulo);

        //output
        System.out.printf("%.1f", compAB);
        System.out.print(" unidades de distância");
    }
}
