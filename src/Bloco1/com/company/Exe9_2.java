package Bloco1.com.company;

import java.util.Scanner;

public class Exe9_2 {
    public static void main (String[] args){

        //estrutura de dados
        double perimetro, a, b;

        //Entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Comprimento lado a:");
        a = ler.nextDouble();
        System.out.print("Comprimento lado b:");
        b = ler.nextDouble();

        //Processamento
        perimetro = Bloco1.perimetroRectangulo(a, b);

        //Output
        System.out.println("O rectângulo tem " + perimetro );
        System.out.print("unidades de perimetro");
    }
}
