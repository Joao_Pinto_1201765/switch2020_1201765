package Bloco1.com.company;

public class Main {

    public static void main(String[] args) {
        //Altura Edificio
        Edificio edUm = new Edificio(1);

        System.out.println(edUm.cacularAltura(1));

        //Perimetro Rectangulo
        Rectangulo rectanguloUm = new Rectangulo(2, 1);

        System.out.println(rectanguloUm.perimetroRetangulo());

        // Triangulo Hipotenusa
        Triangulo triang = new Triangulo(1, 1);

        System.out.println(triang.hipotenusa());

        Triangulo triangDois = new Triangulo(-1, 1);

    }
}
