package Bloco1.com.company;

public class Rectangulo {

    private double ladoMaior;
    private double ladoMenor;

    public Rectangulo(double ladoMaior, double ladoMenor) {
        this.ladoMaior = ladoMaior;
        this.ladoMenor = ladoMenor;
    }

    public double perimetroRetangulo(){
        return (2*ladoMaior) + (2*ladoMenor);
    }
}
