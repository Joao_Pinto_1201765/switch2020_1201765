package Bloco1.com.company;

import java.security.InvalidParameterException;

public class Triangulo {

    private double catetoUm;
    private double catetoDois;
    private double hipotenusa;

    private Triangulo() {
    }

    public Triangulo(double catetoUm, double catetoDois) {
        if (catetoUm <= 0 || catetoDois <= 0) {
            throw new InvalidParameterException("Catetos têm de ter valores positivos");
        }

        this.catetoUm = catetoUm;
        this.catetoDois = catetoDois;
        this.hipotenusa = hipotenusaE();
    }

    private double hipotenusaE() {
        return Math.sqrt(Math.pow(catetoUm, 2) + Math.pow(catetoDois, 2));
    }

    public double hipotenusa() {
        return this.hipotenusa;
    }
}
