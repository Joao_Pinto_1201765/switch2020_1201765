package Bloco2.com.company;

public class Bloco2 {
    //Exercício 1

    //a) Calculo média ponderada de três disciplinas
    //b) |Inicio| > |nota1, nota2, nota3, peso1, peso2, peso3| > |mediaPonderada = (nota1*peso1 + nota2*peso2 + nota3*peso3) / (peso1 + peso2 + peso3)| > | "Média ponderada =" + mediaPonderada |
    //c)
    public static double mediaPonderada(double nota1, double nota2, double nota3, double peso1, double peso2, double peso3) {
        //ED
        double mediaPonderada;
        //Processamento
        mediaPonderada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

        return mediaPonderada;
    }

    //Exercício 2 Dá para fazer tudo numa unica função!!!!!!!!!!!! Consultar pasta

    //a)Classificar o numero como tendo ou não 3 digitos
    //b)|Inicio| > |numero, digito1, digito2, digito3| > |Se menor que 100 ou maior que 900 retornar "Numero não tem 3 digitos"; Se numero = 3 digitos: digito1 = MOD 10, digito2 = (DIV 10) MOD 10, digito3 = (DIV 100) MOD 10 | > |escrever "digito1, digito2, digito3"|
    //c)
    public static int tresDigitos3(int numero) {
        int digito3 = 0;

        if (numero < 100 || numero > 999) {
            System.out.println("Tente de novo");
        } else {
            digito3 = numero % 10;
        }
        return digito3;
    }

    public static int tresDigitos2(int numero) {
        int digito2 = 0;

        if (numero < 100 || numero > 999) {
            System.out.println("Insira um numero com 3 digitos");
        } else {
            digito2 = (numero / 10) % 10;
        }
        return digito2;
    }

    public static int tresDigitos1(int numero) {
        int digito1 = 0;

        if (numero < 100 || numero > 999) {
            System.out.println("Numero não tem 3 digitos");
        } else {
            digito1 = (numero / 100) % 10;
        }
        return digito1;
    }

    public static String parOuImpar(int numero) {
        if (numero % 2 == 0) {
            return "Número é par";
        } else
            return "Número é impar";
    }

    //Exercicio 3

    public static double distanciaDoisPontos(double x1, double x2, double y1, double y2) {
        //ED
        double distancia;
        //Processamento
        distancia = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

        return distancia;
    }

    //Exercicio 4

    public static int funcaoDeX(int x) {

        if (x < 0) {
            return x;
        } else if (x == 0) {
            return 0;
        } else {
            return 0;
        }
    }

    //Exercicio 5

    public static double volumeCubo(double area) {
        double aresta, volumeCm3, volumeDm3;

        //Processamento
        if (area > 0) {
            aresta = Math.sqrt(area / 6);
            volumeCm3 = Math.pow(aresta, 3);
            volumeDm3 = volumeCm3 * 0.001;

            return volumeDm3;
        } else {
            return -1;
        }
    }

    public static String classificacaoVolumeCubo(double volumedm3) {
        if (volumedm3 < 0)
            return "Área com valor negativo";

        if (volumedm3 <= 1.0) {
            return "Pequeno";
        } else if (volumedm3 > 1.0 && volumedm3 <= 2.0) {
            return "Médio";
        } else {
            return "Grande";
        }
    }

    // Exercicio 6

    public static String segundosToHMS(int segundos) {
        int toSegundos, toMinutos, toHoras;

        toSegundos = segundos % 60;
        toMinutos = (segundos / 60) % 60;
        toHoras = segundos / 3600;

        return String.format("%02d:%02d:%02d", toHoras, toMinutos, toSegundos);
    }

    public static String saudacao(int segundo) {
        if (segundo >= 21600 && segundo <= 43201) {
            return "Bom dia";
        } else if (segundo > 43201 && segundo <= 72001) {
            return "Boa tarde";
        } else {
            return "Boa noite";
        }
    }

    // Exercicio 7

    public static double custoMaoObra(double area, double salarioHora) {
        double diasDePintura, numeroDePintores = 0, salarioMaoObra;

        if (area <= 100) {
            numeroDePintores = 1;
        } else if (area > 100 && area <= 300) {
            numeroDePintores = 2;
        } else if (area > 300 || area <= 1000) {
            numeroDePintores = 3;
        } else if (area > 1000) {
            numeroDePintores = 4;
        }

        diasDePintura = ((area / 2) / 8) / numeroDePintores;
        salarioMaoObra = (salarioHora * 8) * diasDePintura;

        return salarioMaoObra;
    }

    public static double custoDaTinta(double area, double custoLitro, double redimento) {
        double custoDaTinta;

        custoDaTinta = custoLitro * (area / redimento);

        return custoDaTinta;
    }

    public static double custoTotal(double salarioMaoObra, double custoDaTinta) {
        double custoTotal;

        custoTotal = salarioMaoObra + custoDaTinta;

        return custoTotal;
    }

    // Exercicio 8

    public static String multiploDivisor(double x, double y) {
        if (x % y == 0) {
            return "X é multiplo de Y";
        } else if (y % x == 0) {
            return "Y é multiplo de X";
        } else {
            return "X não é multiplo nem divisor de Y";
        }
    }

    // Exercício 9

    public static String sequenciaCrescente(int numero) {
        int unidades, dezenas, centenas;

        unidades = numero % 10;
        dezenas = (numero % 100) / 10;
        centenas = (numero % 1000) / 100;

        if (centenas < dezenas && dezenas < unidades) {
            return "Sequência crescente";
        } else {
            return "Sequência não crescente";
        }
    }

    // Exercicio 10

    public static double artigoComSaldo(double valorSemSaldo) {
        double valorComSaldo;

        if (valorSemSaldo > 200) {
            valorComSaldo = valorSemSaldo * 0.40;
        } else if (valorSemSaldo > 100) {
            valorComSaldo = valorSemSaldo * 0.60;
        } else if (valorSemSaldo > 50) {
            valorComSaldo = valorSemSaldo * 0.70;
        } else {
            valorComSaldo = valorSemSaldo * 0.80;
        }

        return valorComSaldo;
    }

    // Exercicio 11

    public static double percentagemAprovados(double numAprovados, double numTotalTurma) {
        double percentagemAprovados;

        percentagemAprovados = numAprovados / numTotalTurma;

        return percentagemAprovados;
    }

    public static String classificacaoAprovadoNaoAprovado(double percentagemAprovados, double percentagemTurmaMa, double percentagemTurmaFraca, double percentagemTurmaRazoavel, double percentagemTurmaBoa) {

        if (percentagemAprovados < 0 || percentagemAprovados > 1) {
            return "Valor inválido";
        } else {
            if (percentagemAprovados <= percentagemTurmaMa) {
                return "Turma Má";
            } else if (percentagemAprovados <= percentagemTurmaFraca) {
                return "Turma Fraca";
            } else if (percentagemAprovados <= percentagemTurmaRazoavel) {
                return "Turma Razoável";
            } else if (percentagemAprovados <= percentagemTurmaBoa) {
                return "Turma Boa";
            } else
                return "Turma Excelente";
        }
    }

    // Exercicio 12

    public static String poluicaoGrupos(int grupoIndustrial, double indicePoluicao) {
        String notificacao;

        if (grupoIndustrial > 3 || grupoIndustrial < 0) {
            return "Grupo Industrial não existente";
        } else {
            if (grupoIndustrial == 1 && indicePoluicao > 0.3) {
                notificacao = "Suspender atividade";
            } else if (grupoIndustrial == 2 && indicePoluicao > 0.4) {
                notificacao = "Suspender atividade";
            } else if (grupoIndustrial == 3 && indicePoluicao > 0.5) {
                notificacao = "Suspender atividade";
            } else
                notificacao = "Continuar atividade";

            return notificacao;
        }
    }

    // Exercicio 13

    public static double custoJardinagem(double ervaMQuadrado, double arvores, double arbustos) {
        double custoJardinagem;

        custoJardinagem = (ervaMQuadrado * 10) + (arvores * 20) + (arbustos * 15);

        return custoJardinagem;
    }

    public static double tempoJardinagemHoras(double ervaMQuadrado, double arvores, double arbustos) {
        double tempoJardinagem;

        tempoJardinagem = ((ervaMQuadrado * 300) + (arvores * 600) + (arbustos * 400)) / 3600;

        return tempoJardinagem;
    }

    public static double custoTotalMaisHoras(double custoJardinagem, double tempoJardinagem) {
        double custoTotalMaisHoras;

        custoTotalMaisHoras = custoJardinagem + (tempoJardinagem * 10);

        return custoTotalMaisHoras;
    }

    //Exercicio 14
    public static double mediaCincoDiasDistanciaEstafeta(double distacaniaUm, double distanciaDois, double distanciaTres, double distanciaQuatro, double distanciaCinco) {
        double mediaCincoDiasDistanciaEstafeta, milhasParaKm;

        mediaCincoDiasDistanciaEstafeta = (distacaniaUm + distanciaDois + distanciaTres + distanciaQuatro + distanciaCinco) / 5;
        milhasParaKm = mediaCincoDiasDistanciaEstafeta * 1.609;

        return milhasParaKm;
    }

    //Exercicio 15

    public static String classificacaoTrianguloLados(double a, double b, double c) {
        String classificacao;

        if (a < 0 || b < 0 || c < 0) {
            return "Triangulo impossivel";
        } else if (a > b + c || b > a + c || c > a + b) {
            return "Triangulo impossivel";
        } else {
            if (a == b && b == c) {
                classificacao = "Equilátero";
            } else if (a != b && a != c && b != c) {
                classificacao = "Escaleno";
            } else {
                classificacao = "Isóceles";
            }
            return classificacao;
        }
    }

    //Exercicio 16

    public static String classificacaoTrianguloAngulos(double a, double b, double c) {
        String classificacao;

        if (a < 0 || b < 0 || c < 0) {
            return "Triangulo Impossivel";
        } else if (a + b + c != 180) {
            return "Triangulo Impossivel";
        } else {
            if (a == 90 || b == 90 || c == 90) {
                classificacao = "Retângulo";
            } else if (a >= 90 || b >= 90 || c >= 90) {
                classificacao = "Obtusangulo";
            } else {
                classificacao = "Acutangulo";
            }
            return classificacao;
        }
    }

    // Exercicio 17

    public static String horaComboio(int horasPartida, int minPartida, int horasViagem, int minViagem) {
        int totalMinutosPartida, totalMinutosViagem, totalMinutosChegada, horaChegada, minChegada;
        String dia;

        totalMinutosPartida = (minPartida % 60) + (horasPartida * 60);
        totalMinutosViagem = (minViagem % 60) + (horasViagem * 60);
        totalMinutosChegada = totalMinutosPartida + totalMinutosViagem;
        minChegada = totalMinutosChegada % 60;
        horaChegada = totalMinutosChegada / 60;

        if (horaChegada > 23) {
            dia = "amanhã";
            horaChegada = horaChegada - 24;
        } else
            dia = "hoje";

        return horaChegada + ":" + String.format("%02d", minChegada) + dia;
    }

    // Exercicio 18

    public static String tempoProcessamento(int horasInicioProcessamento, int minInicioProcessamento, int segInicioProcessamento, int tempoProcessamentoSeg) {
        int segToSegFinal, segToMinFinal, segToHoraFinal, tempoTotalEmSeg, segTotalInicio;

        segTotalInicio = (horasInicioProcessamento * 60 * 60) + (minInicioProcessamento * 60) + segInicioProcessamento;

        tempoTotalEmSeg = tempoProcessamentoSeg + segTotalInicio;

        segToSegFinal = tempoTotalEmSeg % 60;
        segToMinFinal = (tempoTotalEmSeg / 60) % 60;
        segToHoraFinal = tempoTotalEmSeg / 3600;

        return String.format("%02d:%02d:%02d", segToHoraFinal, segToMinFinal, segToSegFinal);
    }

    // Exercicio 19

    public static double salario(int horasTrabalho) {
        double salarioSemanal;

        if (horasTrabalho <= 36) {
            salarioSemanal = 36 * 7.5;
        } else if (horasTrabalho <= 41) {
            salarioSemanal = (36 * 7.5) + ((horasTrabalho - 36) * 10);
        } else
            salarioSemanal = (36 * 7.5) + (5 * 10) + ((horasTrabalho - 41) * 15);

        return salarioSemanal;
    }

    // Exercicio 20

    public static double precoJardinagemDomicilio(String tipoDia, String tipoKit, double kmDistancia) {
        double precoJardinagemDomicilio = 0, precoKmDistancia;

        precoKmDistancia = kmDistancia * 2;

        switch (tipoKit) {
            case "A":
                if (tipoDia.equals("Util")) {
                    precoJardinagemDomicilio = 30 + precoKmDistancia;
                } else
                    precoJardinagemDomicilio = 40 + precoKmDistancia;

                break;
            case "B":
                if (tipoDia.equals("Util")) {
                    precoJardinagemDomicilio = 50 + precoKmDistancia;
                } else
                    precoJardinagemDomicilio = 70 + precoKmDistancia;

                break;
            case "C":
                if (tipoDia.equals("Util")) {
                    precoJardinagemDomicilio = 100 + precoKmDistancia;
                } else
                    precoJardinagemDomicilio = 140 + precoKmDistancia;
                break;
        }

        return precoJardinagemDomicilio;
    }
}