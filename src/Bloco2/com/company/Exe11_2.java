package Bloco2.com.company;

import java.util.Scanner;

public class Exe11_2 {
    public static void main(String[] args) {
        //ED
        double numAprovados, numTotalTurma, percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa;
        //String classificacao;

        //IN
        Scanner ler = new Scanner(System.in);
        System.out.println("Numero de alunos aprovado:");
        numAprovados = ler.nextDouble();
        System.out.println("Numero total de alunos da turma:");
        numTotalTurma = ler.nextDouble();
        System.out.println("Percentagem de aprovacao (menor ou igual) para classificacao como Turma Ma:");
        percentagemTurmaMa = ler.nextDouble();
        System.out.println("Percentagem de aprovacao (menor ou igual) para classificacao como Turma Fraca:");
        percentagemTurmaFraca = ler.nextDouble();
        System.out.println("Percentagem de aprovacao (menor ou igual) para classificacao como Turma Razoável:");
        percentagemTurmaRazoavel = ler.nextDouble();
        System.out.println("Percentagem de aprovacao (menor ou igual) para classificacao como Turma Boa:");
        percentagemTurmaBoa = ler.nextDouble();

        //Processamento

        percentagemAprovados = Bloco2.percentagemAprovados(numAprovados, numTotalTurma);

        //classificacao = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        //OUT

        System.out.println("A turma é classifica como:" + Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa));
    }
}
