package Bloco2.com.company;

import java.util.Scanner;

public class Exe1_2 {
    public static void main (String[] args){
        //ED
        double nota1, nota2, nota3,  peso1, peso2, peso3, mediaPonderada;

        //IN
        Scanner ler = new Scanner(System.in);
        System.out.println("Nota da cadeira #1");
        nota1 = ler.nextDouble();
        System.out.println("Peso da cadeira 1#");
        peso1 = ler.nextDouble();
        System.out.println("Nota da cadeira #2");
        nota2 = ler.nextDouble();
        System.out.println("Peso da cadeira #2");
        peso2 = ler.nextDouble();
        System.out.println("Nota da cadeira #3");
        nota3 = ler.nextDouble();
        System.out.println("Peso da cadeira #3");
        peso3 = ler.nextDouble();

        //Processamento
        mediaPonderada = Bloco2.mediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        //Out
        if (mediaPonderada>=8)
            System.out.println("Aprovado");
        else
            System.out.println("Reprovado");
    }
}
