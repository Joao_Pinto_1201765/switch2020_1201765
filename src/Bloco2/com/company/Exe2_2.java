package Bloco2.com.company;

import java.util.Scanner;

public class Exe2_2 {
    public static void main (String[] args){
        //ED
        int numero;
        double digito1, digito2, digito3;

        //IN
        Scanner ler = new Scanner(System.in);
        System.out.println("introdução um número com 3 digitos:");
        numero = ler.nextInt();

        //Processamento
        digito1 = Bloco2.tresDigitos1(numero);
        digito2 = Bloco2.tresDigitos2(numero);
        digito3 = Bloco2.tresDigitos3(numero);

        //Out
        System.out.println(digito1 + " " + digito2 + " " + digito3);
    }
}
