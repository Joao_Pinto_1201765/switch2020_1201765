package Bloco3.com.company;

public class Bloco3 {

    // Exercicio 1
    //a) Imprimir uma vez "O resultado é:" + res

    public static String exercicioUm(int num) {
        int res = 1;
        int x = num;

        while (x <= 1) {
            res *= x;
            x--;
        }
        return "O resultado é:" + res;
    }

    // Exercicio 2

    public static double lerNotasMediaTotal(int nAlunos, int[] notas) {
        double media;

        double soma = 0.0;

        for (int i = 0; i <= nAlunos - 1; i++) {
            soma += notas[i];
        }

        media = soma / nAlunos;

        return media;
    }

    public static String lerNotasMediaNegativasPercentagemNegativas(int nAlunos, int[] notas) {
        double somaNotasNegativas = 0, numAlunosNotasNegativas = 0, numAlunosNotasPositivas = 0;

        double mediaNotasNegativas;
        double percentagemNotasPositivas;

        for (int i = 0; i <= nAlunos - 1; i++) {
            if (notas[i] < 10) {
                numAlunosNotasNegativas++;
                somaNotasNegativas = somaNotasNegativas + notas[i];
            } else {
                numAlunosNotasPositivas++;
            }
        }

        if (numAlunosNotasNegativas == 0) {
            mediaNotasNegativas = 0;
        } else {
            mediaNotasNegativas = somaNotasNegativas / numAlunosNotasNegativas;
        }

        percentagemNotasPositivas = numAlunosNotasPositivas / nAlunos;

        return mediaNotasNegativas + " " + percentagemNotasPositivas;
    }

    // Exercicio 3

    public static String percentagemParesMediaImpares(int[] seqNumeros) {
        double percentagemPares, mediaImpares, arredondamentoMediaImpares;

        double counterNumPares = 0.0;
        double counterNumImpares = 0.0;
        double somaNumImpar = 0.0;

        int indiceOndeTemosImpar = 0;

        for (int i = 0; seqNumeros[i] >= 0; i++) {
            indiceOndeTemosImpar++;
        }

        for (int i = 0; i < indiceOndeTemosImpar; i++) {
            if (seqNumeros[i] % 2 == 0) {
                counterNumPares++;
            } else if (seqNumeros[i] != 0) {
                somaNumImpar += seqNumeros[i];
                counterNumImpares++;
            }
        }

        percentagemPares = counterNumPares / indiceOndeTemosImpar;
        mediaImpares = somaNumImpar / counterNumImpares;
        arredondamentoMediaImpares = Math.round(mediaImpares * 10) / 10.0;

        return percentagemPares + " " + arredondamentoMediaImpares;
    }

    // Exercicio 4
    //a)

    public static int numMultriplosTres(int numInicial, int numFinal) {
        int counterNumeroMultiplosTres = 0, counterIntervalo = 0, counterNumInicial;
        counterNumInicial = numInicial;

        for (int i = numInicial; i <= numFinal; i++) {
            counterIntervalo++;
        }

        int[] intervalo = new int[counterIntervalo];

        for (int i = 0; i <= intervalo.length - 1; i++) {
            intervalo[i] = counterNumInicial++;
        }

        for (int i = 0; i < intervalo.length - 1; i++) {
            if (intervalo[i] % 3 == 0) {
                counterNumeroMultiplosTres++;
            }
        }

        return counterNumeroMultiplosTres;
    }

    //b)
    public static int numMultriplosNumDado(int numInicial, int numFinal, int numDado) {
        int counterNumeroMultiplosNumDado = 0, counterIntervalo = 0, counterNumInicial;
        counterNumInicial = numInicial;

        for (int i = numInicial; i <= numFinal; i++) {
            counterIntervalo++;
        }

        int[] intervalo = new int[counterIntervalo];

        for (int i = 0; i <= intervalo.length - 1; i++) {
            intervalo[i] = counterNumInicial++;
        }

        for (int i = 0; i < intervalo.length - 1; i++) {
            if (intervalo[i] % numDado == 0) {
                counterNumeroMultiplosNumDado++;
            }
        }

        return counterNumeroMultiplosNumDado;
    }

    //c)

    public static String numMultriplosTresECicno(int numInicial, int numFinal) {
        int counterNumeroMultiplosTres = 0, counterMultiplosCinco = 0, counterIntervalo = 0, counterNumInicial;
        counterNumInicial = numInicial;

        for (int i = numInicial; i <= numFinal; i++) {
            counterIntervalo++;
        }

        int[] intervalo = new int[counterIntervalo];

        for (int i = 0; i <= intervalo.length - 1; i++) {
            intervalo[i] = counterNumInicial++;
        }

        for (int j : intervalo) {
            if (j % 3 == 0) {
                counterNumeroMultiplosTres++;
            }
        }

        for (int j : intervalo) {
            if (j % 5 == 0) {
                counterMultiplosCinco++;
            }
        }

        return counterNumeroMultiplosTres + " " + counterMultiplosCinco;
    }

    // d)

    public static String numMultriplosDoisNumDado(int numInicial, int numFinal, int numDadoUm, int numDadoDois) {
        int counterNumeroMultiplosNumDadoUm = 0, counterNumeroMultiplosNumDadoDois = 0, counterIntervalo = 0, counterNumInicial;
        counterNumInicial = numInicial;

        if (numInicial < numFinal) {
            for (int i = numInicial; i <= numFinal; i++) {
                counterIntervalo++;
            }

            int[] intervalo = new int[counterIntervalo];

            for (int i = 0; i <= intervalo.length - 1; i++) {
                intervalo[i] = counterNumInicial++;
            }

            for (int j : intervalo) {
                if (j % numDadoUm == 0) {
                    counterNumeroMultiplosNumDadoUm++;
                }
            }

            for (int j : intervalo) {
                if (j % numDadoDois == 0) {
                    counterNumeroMultiplosNumDadoDois++;
                }
            }
        } else
            return "Número inicial superior ao numero final";

        return counterNumeroMultiplosNumDadoUm + " " + counterNumeroMultiplosNumDadoDois;
    }

    // e)

    public static int somaMultiplosDoisNumDadoIntervalo(int numInicial, int numFinal, int numDadoUm, int numDadoDois) {
        int counterIntervalo = 0, counterNumInicial, somaMultiNumDadoUm = 0, somaMultiNumDadoDois = 0;

        counterNumInicial = numInicial;

        if (numInicial < numFinal) {
            for (int i = numInicial; i <= numFinal; i++) {
                counterIntervalo++;
            }

            int[] intervalo = new int[counterIntervalo];

            for (int i = 0; i < intervalo.length; i++) {
                intervalo[i] = counterNumInicial++;
            }

            for (int j : intervalo) {
                if (j % numDadoUm == 0) {
                    somaMultiNumDadoUm += j;
                }
            }

            for (int k : intervalo) {
                if (k % numDadoDois == 0) {
                    somaMultiNumDadoDois += k;
                }
            }

        } else
            return -1;

        return somaMultiNumDadoDois + somaMultiNumDadoUm;
    }

    // Exercicio 5 a)

    public static int somaNumerosParesIntervaloDado(int numDadoUm, int numDadoDois) {
        int counterNumDado, somaTodosNumerosPares = 0, dimensao;

        if (numDadoDois > numDadoUm) {
            dimensao = numDadoDois - numDadoUm + 1;
            counterNumDado = numDadoUm;
        } else {
            dimensao = numDadoUm - numDadoDois + 1;
            counterNumDado = numDadoDois;
        }

        int[] arr = new int[dimensao];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = counterNumDado++;
        }

        for (int j : arr) {
            if (j % 2 == 0) {
                somaTodosNumerosPares += j;
            }
        }

        return somaTodosNumerosPares;
    }

    // b)

    public static int quantidadeNumerosParesIntervaloDado(int numDadoUm, int numDadoDois) {
        int counterNumDado, quantidadeTodosNumerosPares = 0, dimensao;

        if (numDadoDois > numDadoUm) {
            dimensao = numDadoDois - numDadoUm + 1;
            counterNumDado = numDadoUm;
        } else {
            dimensao = numDadoUm - numDadoDois + 1;
            counterNumDado = numDadoDois;
        }

        int[] arr = new int[dimensao];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = counterNumDado++;
        }

        for (int j : arr) {
            if (j % 2 == 0) {
                quantidadeTodosNumerosPares++;
            }
        }

        return quantidadeTodosNumerosPares;
    }

    // c)

    public static int somaNumerosIpmaresIntervaloDado(int numDadoUm, int numDadoDois) {
        int counterNumDado, somaTodosNumerosImpares = 0, dimensao;

        if (numDadoDois > numDadoUm) {
            dimensao = numDadoDois - numDadoUm + 1;
            counterNumDado = numDadoUm;
        } else {
            dimensao = numDadoUm - numDadoDois + 1;
            counterNumDado = numDadoDois;
        }

        int[] arr = new int[dimensao];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = counterNumDado++;
        }

        for (int j : arr) {
            if (j % 2 != 0) {
                somaTodosNumerosImpares += j;
            }
        }

        return somaTodosNumerosImpares;
    }

    // d)

    public static int quantidadeNumerosImparesIntervaloDado(int numDadoUm, int numDadoDois) {
        int counterNumDado, quantidadeTodosNumerosImpares = 0, dimensao;

        if (numDadoDois > numDadoUm) {
            dimensao = numDadoDois - numDadoUm + 1;
            counterNumDado = numDadoUm;
        } else {
            dimensao = numDadoUm - numDadoDois + 1;
            counterNumDado = numDadoDois;
        }

        int[] arr = new int[dimensao];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = counterNumDado++;
        }

        for (int j : arr) {
            if (j % 2 != 0) {
                quantidadeTodosNumerosImpares++;
            }
        }

        return quantidadeTodosNumerosImpares;
    }

    // e)

    public static int somaMultiplosNumeroDadoIntervaloDado(int numeroInicial, int numeroFinal, int numDado) {
        int somaMultiplos = 0, intervalo;

        if (numeroInicial > 0 && numeroFinal > 0) {
            if (numeroFinal > numeroInicial) {
                intervalo = numeroFinal - numeroInicial + 1;
                for (int i = numeroInicial; i < intervalo; i++) {
                    if (i % numDado == 0) {
                        somaMultiplos += i;
                    }
                }
            } else if (numeroInicial > numeroFinal) {
                intervalo = numeroInicial - numeroFinal + 1;
                for (int i = numeroFinal; i < intervalo; i++) {
                    if (i % numDado == 0) {
                        somaMultiplos += i;
                    }
                }
            }
        } else
            return -1;

        return somaMultiplos;
    }

    // f)

    public static int produtoNumerosMultiplosNumeroDado(int numeroInicial, int numeroFinal, int numeroDado) {
        int produtoNumerosMultiplos = 1;

        if (numeroInicial > 0 && numeroFinal > 0) {
            if (numeroFinal > numeroInicial) {
                for (int i = numeroInicial; i < numeroFinal; i++) {
                    if (i % numeroDado == 0) {
                        produtoNumerosMultiplos *= i;
                    }
                }
            } else if (numeroInicial != numeroFinal) {
                for (int i = numeroFinal; i < numeroInicial; i++) {
                    if (i % numeroDado == 0) {
                        produtoNumerosMultiplos *= i;
                    }
                }
            } else
                return -1;
        } else
            return -1;

        return produtoNumerosMultiplos;
    }

    // g)

    public static double mediaMultiplosNumeroDadoIntervaloDado(int numInicial, int numFinal, int numDado) {
        double mediaMultiplosNumeroDado, counterMultiplosNumDado = 0, somaMultiplosNumDado = 0;

        if (numInicial > 0 && numFinal > 0) {
            if (numFinal > numInicial) {
                for (int i = numInicial; i < numFinal; i++) {
                    if (i % numDado == 0) {
                        somaMultiplosNumDado += i;
                        counterMultiplosNumDado++;
                    }
                }
            } else if (numInicial != numFinal) {
                for (int i = numFinal; i < numInicial; i++) {
                    if (i % numDado == 0) {
                        somaMultiplosNumDado += i;
                        counterMultiplosNumDado++;
                    }
                }
            } else
                return -1;
        } else
            return -1;

        mediaMultiplosNumeroDado = somaMultiplosNumDado / counterMultiplosNumDado;

        return mediaMultiplosNumeroDado;
    }

    // h)

    public static String mediaMultiplosXeY(int x, int y, int numInicial, int numFinal) {
        int numIteradorUm, numIteradorDois, counterMultiplosX = 0, counterMultiplosY = 0;
        double mediaX, mediaY, somaMultiplosX = 0, somaMultiplosY = 0;

        if (numFinal >= numInicial) {
            numIteradorUm = numInicial;
            numIteradorDois = numFinal;
        } else {
            numIteradorUm = numFinal;
            numIteradorDois = numInicial;
        }

        for (int i = numIteradorUm; i < numIteradorDois; i++) {
            if (x % i == 0) {
                somaMultiplosX += i;
                counterMultiplosX++;
            }
            if (y % i == 0) {
                somaMultiplosY += i;
                counterMultiplosY++;
            }
        }

        mediaX = somaMultiplosX / counterMultiplosX;
        mediaY = somaMultiplosY / counterMultiplosY;

        return mediaX + " " + mediaY;
    }

    // Exercicio 6 a)

    public static int numeroAlgarismosDeNumeroInteiroLongo(long numero) {
        int counterNumeroDigitos = 0;

        while (numero != 0) {
            numero = (numero / 10);
            counterNumeroDigitos++;
        }

        return counterNumeroDigitos;
    }

    // 6.b)

    public static int numeroAlgarismosParesDeUmNumeroLongo(long numero) {
        int counterAlgarismosPar = 0;

        while (numero != 0) {
            if (numero % 2 == 0)
                counterAlgarismosPar++;
            numero = (numero / 10);
        }

        return counterAlgarismosPar;
    }

    // 6.c)

    public static int numeroAlgarismosImparesDeUmNumeroLongo(long numero) {
        int counterAlgarismosImpar = 0;

        while (numero != 0) {
            if (numero % 2 != 0)
                counterAlgarismosImpar++;
            numero = (numero / 10);
        }

        return counterAlgarismosImpar;
    }

    // 6.d)

    public static int somaAlgarismosDeUmNumero(long numero) {
        int somaAlgarismosDeUmNumero = 0;

        while (numero > 0) {
            somaAlgarismosDeUmNumero += (numero % 10);
            numero /= 10;
        }

        return somaAlgarismosDeUmNumero;
    }

    // 6.e)

    public static int somaAlgarismosParesDeUmNumero(long numero) {
        int somaAlgarismosParesDeUmNumero = 0, sobra;

        while (numero > 0) {
            sobra = (int) (numero % 10);
            numero /= 10;
            if (sobra % 2 == 0) {
                somaAlgarismosParesDeUmNumero += sobra;
            }
        }

        return somaAlgarismosParesDeUmNumero;
    }

    // 6.f)

    public static int somaAlgarismosImparesDeUmNumero(long numero) {
        int somaAlgarismosImparesDeUmNumero = 0, sobra;

        while (numero > 0) {
            sobra = (int) (numero % 10);
            numero /= 10;
            if (sobra % 2 != 0) {
                somaAlgarismosImparesDeUmNumero += sobra;
            }
        }

        return somaAlgarismosImparesDeUmNumero;
    }

    // 6.g)

    public static double mediaAlgarismosDeUmNumeroLongo(long numero) {
        double mediaAlagrismosDeUmNumero, somaAlgarismosDeUmNumero = 0, counterAlgarismos = 0;

        while (numero > 0) {
            somaAlgarismosDeUmNumero += (numero % 10);
            numero = (numero / 10);
            counterAlgarismos++;

        }

        mediaAlagrismosDeUmNumero = somaAlgarismosDeUmNumero / counterAlgarismos;

        return mediaAlagrismosDeUmNumero;

    }

    // 6.h)

    public static double mediaAlgarismosParesDeUmNumeroLongo(long numero) {
        double mediaAlgarismosParesDeUmNumero, somaAlgarismoParesDeUmNumero = 0, counterAlgarismosPares = 0;
        int sobra;

        while (numero > 0) {
            sobra = (int) (numero % 10);
            if (sobra % 2 == 0) {
                somaAlgarismoParesDeUmNumero += sobra;
                counterAlgarismosPares++;
            }
            numero /= 10;
        }

        mediaAlgarismosParesDeUmNumero = somaAlgarismoParesDeUmNumero / counterAlgarismosPares;

        return mediaAlgarismosParesDeUmNumero;
    }

    // 6.i)

    public static double mediaAlgarismosImparesDeUmNumeroLongo(long numero) {
        double mediaAlgarismosImparesDeUmNumero, somaAlgarismoImparesDeUmNumero = 0, counterAlgarismosImpares = 0;
        int sobra;

        while (numero > 0) {
            sobra = (int) (numero % 10);
            if (sobra % 2 != 0) {
                somaAlgarismoImparesDeUmNumero += sobra;
                counterAlgarismosImpares++;
            }
            numero /= 10;
        }

        mediaAlgarismosImparesDeUmNumero = somaAlgarismoImparesDeUmNumero / counterAlgarismosImpares;

        return mediaAlgarismosImparesDeUmNumero;
    }

    // 6.j)

    public static long converterNumeroParaOrdemInversaAlgarismos(long numero) {
        int numeroInvertido = 0;

        while (numero > 0) {
            numeroInvertido *= 10;
            numeroInvertido += numero % 10;
            numero /= 10;
        }

        return numeroInvertido;
    }

    // Exercicio 7
    // 7.a)

    public static boolean isCapicua(long numero) {
        boolean flag = false;

        long numeroInvertido = Bloco3.converterNumeroParaOrdemInversaAlgarismos(numero);

        if (numero == numeroInvertido)
            flag = true;

        return flag;
    }

    // 7.b)

    public static boolean isArmstrong(int numero) {
        boolean flag = false;
        int numeroArmstrong = 0, numeroIterador, sobra;

        numeroIterador = numero;

        while (numeroIterador > 0) {
            sobra = numeroIterador % 10;
            numeroArmstrong += (int) Math.pow((sobra), Bloco3.numeroAlgarismosDeNumeroInteiroLongo(numero));
            numeroIterador /= 10;
        }

        if (numero == numeroArmstrong)
            flag = true;

        return flag;
    }

    // 7.c)

    public static int retornarPrimeiraCapicuaIntrevaloDado(int numInicial, int numFinal) {
        int numeroIteradorInicial, numeroIteradorFinal, primeiraCapicua = 0, counnterCapicua = 0;


        if (numFinal > numInicial) {
            numeroIteradorInicial = numInicial;
            numeroIteradorFinal = numFinal;
        } else if (numInicial > numFinal) {
            numeroIteradorInicial = numFinal;
            numeroIteradorFinal = numeroIteradorInicial;
        } else
            return -1;

        if (numeroIteradorInicial > 99) {
        /*
        for (int i = numeroIteradorInicial; i < numeroIteradorFinal && counnterCapicua != 0; i++) {
            if (Bloco3.verificarNumeroECapicua(i))
                primeiraCapicua = i;
                counnterCapicua++;
        }
        */

        /*
        while (numeroIteradorFinal > numeroIteradorInicial && counnterCapicua != 0)
            if (Bloco3.verificarNumeroECapicua(numeroIteradorInicial)) {
                primeiraCapicua = numeroIteradorInicial;
                counnterCapicua++;
            }
            numeroIteradorInicial++;
        */

            do {
                if (Bloco3.isCapicua(numeroIteradorInicial)) {
                    primeiraCapicua = numeroIteradorInicial;
                    counnterCapicua++;
                }
                numeroIteradorInicial++;

            } while (numeroIteradorFinal > numeroIteradorInicial && counnterCapicua == 0);

        } else
            primeiraCapicua = numeroIteradorInicial;


        return primeiraCapicua;
    }

    // 7.d) IGUAL A ANTERIOR SEM PARAR NA PRIMEIRA CAPICUA, LÊ TODAS E FICA COM A ULTIMA LEITURA (MAIOR)
    //      OU ENTÃO i-- e parar à primeira capicua
    /*
        for (int i = numeroIteradorInicial; i < numeroIteradorFinal; i++) {
            if (Bloco3.verificarNumeroECapicua(i))
                primeiraCapicua = i;
                counnterCapicua++;
        }
    */

    // 7.e)

    public static int numeroCapicuasIntervaloDado(int limiteInferior, int limiteSuperior) {
        int counterCapicuas = 0;

        for (int i = limiteInferior; i < limiteSuperior; i++) {
            if (isCapicua(i)) {
                counterCapicuas++;
            }
        }

        return counterCapicuas;
    }

    // 7.f)

    public static int retornarPrimeiroNumeroArmstrongNumIntrevaloDado(int limiteInferior, int limiteSuperior) {
        int primeiraCapicua = 0, counterCapicua = 0;

        /*for (int i = limiteInferior; i < limiteSuperior && counterCapicua != 0; i++) {
            if (verificarNumeroECapicua(i))
                primeiraCapicua = i;
                counterCapicua++;
        }*/
        do {
            if (isArmstrong(limiteInferior)) {
                primeiraCapicua = limiteInferior;
                counterCapicua++;
            }
            limiteInferior++;
        } while (limiteInferior < limiteSuperior && counterCapicua == 0);

        return primeiraCapicua;
    }

    // 7.g)

    public static int retornarQuantidadeNumerosArmstrongNumDadoIntervalo(int limiteInfeior, int limiteSuperior) {
        int counterArmstrong = 0;

        for (int i = limiteInfeior; i < limiteSuperior; i++) {
            if (isArmstrong(i))
                counterArmstrong++;
        }

        return counterArmstrong;
    }

    // Exercicio 8

    // Exercicio 9

    public static double calcularSalarioMensal(int horasExtra, double salarioBase) {
        double valorHorasExtra, salarioMensal;

        valorHorasExtra = horasExtra * (salarioBase * 0.02);

        salarioMensal = valorHorasExtra + salarioBase;

        return salarioMensal;
    }

    public static double mediaSalariosMensais(double[] salariosMensais) {
        double somaSalariosMensais = 0, mediaSalarioMensais;

        for (double i : salariosMensais) {
            somaSalariosMensais += i;
        }

        mediaSalarioMensais = somaSalariosMensais / salariosMensais.length;

        return mediaSalarioMensais;

    }

    // Exercicio 10

    // Exercicio 11

    public static int numeroDeManeirasParaObterNumeroDadoDeUmAVinte(int numeroDado) {
        int numeroDeManeiras = 0;

        if (numeroDado >= 1 && numeroDado <= 20) {

            for (int i = 0; i <= 10; i++) {
                for (int j = 10; j >= i; j--) {
                    if (i + j == numeroDado) {
                        numeroDeManeiras++;
                    }
                }
            }
        }

        // numeroDeManeiras = (numeroDeManeiras / 2) + 1;

        return numeroDeManeiras;
    }

    // Exercicio 12

    public static String formulaResolventeCompleta(double a, double b, double c) {
        String mensagem;
        double radicando, formulaResolventeSolucaoUm, formulaResolventeSolucaoDois;


        if (a != 0) {

            radicando = Math.pow(b, 2) - (4 * a * c);

            formulaResolventeSolucaoUm = ((-b) + Math.sqrt(radicando)) / (2 * a);
            formulaResolventeSolucaoDois = ((-b) - Math.sqrt(radicando)) / (2 * a);

            if (radicando > 0) {
                mensagem = "A equação tem duas raízes reais x = " + formulaResolventeSolucaoUm + " e x = " + formulaResolventeSolucaoDois;
            } else if (radicando == 0) {
                mensagem = "A equação tem uma raiz dupla x = " + formulaResolventeSolucaoUm;
            } else
                mensagem = "A equação tem raizes imaginárias";


        } else
            mensagem = "Não é equação do segundo grau";

        return mensagem;
    }

    // Exercicio 13

    public static String classificarProduto(int codigoProduto) {
        String classificacao;

        switch (codigoProduto) {
            case 0:
                classificacao = "Leitura dos códigos terminada";
                break;
            case 1:
                classificacao = "Alimento não perecível";
                break;
            case 2:
            case 3:
            case 4:
                classificacao = "Alimento perecível";
                break;
            case 5:
            case 6:
                classificacao = "Vestuário";
                break;
            case 7:
                classificacao = "Higiene pessoal";
                break;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                classificacao = "Limpeza e utensílios domésticos";
                break;
            default:
                classificacao = "Codigo Inválido";
        }

        return classificacao;
    }

    // Exercicio 14

    public static double selecionarMoedaCambio(String moeda) {
        double valorCambioDeUmEuro;

        switch (moeda) {
            case "D":
                valorCambioDeUmEuro = 1.534;
                break;
            case "L":
                valorCambioDeUmEuro = 0.774;
                break;
            case "I":
                valorCambioDeUmEuro = 161.480;
                break;
            case "C":
                valorCambioDeUmEuro = 9.593;
                break;
            case "F":
                valorCambioDeUmEuro = 1.601;
                break;
            default:
                valorCambioDeUmEuro = 1;
        }

        return valorCambioDeUmEuro;
    }

    public static double cambioDaQuantiaDada(double quantia, String moeda) {
        double valorCambiado;

        valorCambiado = quantia * Bloco3.selecionarMoedaCambio(moeda);

        return valorCambiado;
    }
}
