package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio10 {
    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);

        // ED

        int numeroIntroduzido, soma = 1, numeroFinal, maiorNumero = 0;

        // IN
        System.out.println("Número Final");
        numeroFinal = ler.nextInt();

        do {
            System.out.println("Proximo numero positivo");
            numeroIntroduzido = ler.nextInt();
            soma += (numeroIntroduzido * soma);
            if (numeroIntroduzido > maiorNumero) {
                maiorNumero = numeroIntroduzido;
            }
        } while (soma < numeroFinal);

        System.out.println(maiorNumero);
    }
}

