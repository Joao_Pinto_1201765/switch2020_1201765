package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio13 {
    public static void main(String[] args) {
        int codigoProduto;

        Scanner ler = new Scanner(System.in);

        do {
            codigoProduto = ler.nextInt();
            System.out.println(Bloco3.classificarProduto(codigoProduto));
        } while (codigoProduto != 0);
    }
}
