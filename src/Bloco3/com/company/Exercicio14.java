package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio14 {
    public static void main(String[] args) {
        //ED
        double quantia, valorDaConversao;
        String moeda;

        //IN & PRO. & OUt
        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Inicial da Moeda Final");
            moeda = ler.next();

            System.out.println("Quantidade em Euros");
            quantia = ler.nextDouble();

            valorDaConversao = Bloco3.cambioDaQuantiaDada(quantia, moeda);


            System.out.println(valorDaConversao);

        } while (quantia > 0);
    }
}
