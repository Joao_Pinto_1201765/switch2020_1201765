package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio15 {

    public static String notasInteirasParaQualitativas(int nota) {
        String notaQualitativa;

        switch (nota) {
            case 0: case 1: case 2: case 3: case 4:
                notaQualitativa = "Mau";
                break;
            case 5: case 6: case 7: case 8: case 9:
                notaQualitativa = "Medíocre";
                break;
            case 10: case 11: case 12: case 13:
                notaQualitativa = "Suficiente";
                break;
            case 14: case 15: case 16: case 17:
                notaQualitativa = "Bom";
                break;
            case 18: case 19: case 20:
                notaQualitativa = "Muito Bom";
                break;
            default:
                notaQualitativa = "Nota inválida";
        }

        return notaQualitativa;
    }

    public static void main(String[] args) {
        //ED
        int nota;
        String notaQualitativa;

        //IN & PRO. & OUT

        Scanner ler = new Scanner(System.in);

        do {
            System.out.println();
            System.out.println("Nota:");
            nota = ler.nextInt();
            notaQualitativa = Exercicio15.notasInteirasParaQualitativas(nota);
            System.out.println(notaQualitativa);

        } while (nota >= 0);

        System.out.println("Fim da leitura.");
    }
}
