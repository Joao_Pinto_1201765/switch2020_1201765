package Bloco3.com.company;

public class Exercicio16 {

    public static double calcularSalarioLiquido(double salario) {
        double salarioLiquido;

        if (salario > 0){
            if (salario < 500) { //"até 500" não inclusivo
                salarioLiquido = salario * 0.90;
            } else if (salario <= 1000) { //"entre 500 e 1000" 1000 inclusivo
                salarioLiquido = salario * 0.85;
            } else // "acime de 1000 imposto de 20%
                salarioLiquido = salario * 0.80;
        } else
            salarioLiquido = -1;

        return salarioLiquido;
    }
}
