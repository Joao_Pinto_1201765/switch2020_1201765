package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio17 {

    public static double quantidadeDeRacaoEmGramas(double pesoCao) {
        double quantidadeDeRacao;

        if (pesoCao <= 10) { //raca pequena
            quantidadeDeRacao = 100;
        } else if (pesoCao > 10 && pesoCao <= 25) { //raca media
            quantidadeDeRacao = 250;
        } else if (pesoCao > 25 && pesoCao <= 45) { //raca grande
            quantidadeDeRacao = 300;
        } else //raca gigante
            quantidadeDeRacao = 500;

        return quantidadeDeRacao;
    }

    public static void main(String[] args) {
        //ED
        double pesoCao, quantidadeDeRacao;

        //IN & PROC. & OUT
        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Peso do Cão");
            pesoCao = ler.nextDouble();
            quantidadeDeRacao = Exercicio17.quantidadeDeRacaoEmGramas(pesoCao);
            System.out.println(quantidadeDeRacao);

        }while (pesoCao > 0);

        System.out.println("Leitura terminada");
    }
}
