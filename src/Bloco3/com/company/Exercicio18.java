package Bloco3.com.company;

public class Exercicio18 {

    public static boolean verificacaoID(int numeroID) {
        boolean flag = false;
        int algarismo = 0, somaPonderada = 0, numeroIDParaTratar, numeroDeAlgarismos;

        numeroDeAlgarismos = 9; //numero de algarimos do CC, 8 digitos no numero civil + 1 digito controlo

        numeroIDParaTratar = numeroID/10; // numero civil
        int aux = numeroID % 10; // digito controlo

        for (int i = 2; i <= numeroDeAlgarismos; i++) {
            algarismo = numeroIDParaTratar % 10; // algarismo das unidades
            somaPonderada += algarismo * i;
            numeroIDParaTratar /= 10; // retirar o ultimo algarismo
        }

        somaPonderada += aux;


        if (somaPonderada % 11 == 0) //Se soma ponderada multipla de 11, numero CC válido
            flag = true;

        return flag;
    }
}
