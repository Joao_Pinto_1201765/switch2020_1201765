package Bloco3.com.company;

public class Exercicio19 {

    public static int[] retirarOsParesDeUmArray(int[] nNumeirosInteiros) {

        int[] pares = new int[Exercicio19.countEvenElements(nNumeirosInteiros)];

        int j = 0;

        for (int i = 0; i < nNumeirosInteiros.length; i++) {
            if (nNumeirosInteiros[i] % 2 == 0) {
                pares[j] = nNumeirosInteiros[i];
                j++;
            }
        }

        return pares;
    }

    public static int[] retirarOsImparesDeUmArray(int[] nNumeirosInteiros) {

        int[] impares = new int[(nNumeirosInteiros.length) - (Exercicio19.countEvenElements(nNumeirosInteiros))];

        int j = 0;

        for (int i = 0; i < nNumeirosInteiros.length; i++) {
            if (nNumeirosInteiros[i] % 2 != 0) {
                impares[j] = nNumeirosInteiros[i];
                j++;
            }
        }
        return impares;
    }

    public static int countEvenElements(int[] nNumerosInteiros) {
        int counterPar = 0;

        for (int i = 0; i < nNumerosInteiros.length; i++) {
            if (nNumerosInteiros[i] % 2 == 0) {
                counterPar++;
            }
        }

        return counterPar;
    }

    public static int[] paresDireitaImparesEsquerda(int[] impares, int [] pares,int [] numerosInteiros) {
        int [] reorganizado = new int[numerosInteiros.length];

        int k = 0;

        for (int i = 0; i < impares.length; i++) {
            reorganizado[i] = impares[i];
        }

        for (int i = impares.length; i < reorganizado.length; i++) {
            reorganizado[i] = pares[k];
            k++;

        }

        return reorganizado;
    }
}
