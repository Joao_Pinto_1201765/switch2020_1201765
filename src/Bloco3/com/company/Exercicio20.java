package Bloco3.com.company;

public class Exercicio20 {

    public static String classificacaoNumeroInteiroEmPerfeitoAbundanteOuReduzido(int numero) {
        int soma = 0;
        String classificacao;

        for (int i = 1; i < numero; i++) {
            if (numero % i == 0)
                soma += i;
        }

        if (numero == soma) {
            classificacao = "Perfeito";
        } else if (numero < soma) {
            classificacao = "Abundante";
        } else
            classificacao = "Reduzido";

        return classificacao;
    }
}
