package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio8 {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);

        // ED

        int numeroIntroduzido, soma = 0, numeroFinal, menorNumero;

        // IN
        System.out.println("Número Final");
        numeroFinal = ler.nextInt();
        menorNumero = numeroFinal;

        do {
            System.out.println("Proximo numero positivo");
            numeroIntroduzido = ler.nextInt();
            soma += numeroIntroduzido;
            if (numeroIntroduzido < menorNumero) {
                menorNumero = numeroIntroduzido;
            }
        } while (soma < numeroFinal);

        System.out.println(menorNumero);
    }
}
