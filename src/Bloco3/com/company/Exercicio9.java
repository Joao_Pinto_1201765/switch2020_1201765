package Bloco3.com.company;

import java.util.Scanner;

public class Exercicio9 {

    public static void main(String[] args) {
        //ED

        double salarioBase;
        int i = 0, horasExtra = 0, dimensao;

        // IN AND PROCESS
        Scanner ler = new Scanner(System.in);

        System.out.println("Numero de empregados");
        dimensao = ler.nextInt();
        double[] salariosMensais = new double[dimensao];

        do {
            System.out.println("Numero Horas extra do empregado #" + (i + 1));
            horasExtra = ler.nextInt();
            if (horasExtra != -1 && horasExtra >= 0) {
                System.out.println("Salario Base do empregado #" + (i + 1));
                salarioBase = ler.nextDouble();
                if (salarioBase >= 0) {
                    salariosMensais[i] = Bloco3.calcularSalarioMensal(horasExtra, salarioBase);
                }
            }
            i++;
        }while (horasExtra != -1);

        //OUT

        for (int j = 0; j < salariosMensais.length; j++) {
            System.out.println("#" + (j+1) + " " + salariosMensais[j]);
        }
        System.out.println("Media salarios:"  + Bloco3.mediaSalariosMensais(salariosMensais));
    }
}
