package Bloco4.com.company;

public class Exercicio1 {

    public static int numeroDeDigitosDeUmNumero(int numero) {

        int counterDigitos = 0;

        while (numero > 0) {
            numero /= 10;
            counterDigitos++;
        }

        /*for (int i = 0; numero > 0 || i < numero; i++) {
            numero /= 10;
            counterDigitos++;

        }*/

        return counterDigitos;
    }
}
