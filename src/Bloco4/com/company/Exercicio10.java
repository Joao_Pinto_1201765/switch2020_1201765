package Bloco4.com.company;

public class Exercicio10 {

    //a)

    public static int numeroDeMenorValorDeUmVetor(int[] vetor) {

        int numeroDeMenorValor = vetor[0];

        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] < numeroDeMenorValor)
                numeroDeMenorValor = vetor[i];

        }

        return numeroDeMenorValor;
    }

    //b)

    public static int numeroDeMaiorValorDeUmVetor(int[] vetor) {

        int numeroDeMaiorValor = vetor[0];

        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] > numeroDeMaiorValor)
                numeroDeMaiorValor = vetor[i];

        }

        return numeroDeMaiorValor;
    }

    // c)

    public static int somaValoresDeUmVetor(int[] vetor) {
        int somaValoresVetor = 0;

        for (int i = 0; i < vetor.length; i++) {
            somaValoresVetor += vetor[i];

        }

        return somaValoresVetor;
    }

    public static double mediaValoresDeUmVetor(int[] vetor) {
        double mediaValoresDeUmVetor;

        mediaValoresDeUmVetor = (double) somaValoresDeUmVetor(vetor) / vetor.length;

        return mediaValoresDeUmVetor;
    }

    // d)

    public static int somaProdutosValoresDeUmVetor(int[] vetor) {
        int somaProdutos = 1; // Se = 0 multiplica por zero da primeira à ultima iteração

        for (int i = 0; i < vetor.length; i++) {
            somaProdutos *= vetor[i];

        }

        return somaProdutos;
    }

    // e)

    public static int[] retornarConjuntoDeElementosNaoRepetidosDeUmVetorIncluindoZerosDeSubstituicao(int[] vetor) {

        int[] conjuntoDeElementosNaoRepetidos = new int[vetor.length];

        int k = 0;
        int counter;

        for (int i = 0; i < vetor.length; i++) {
            counter = 1;
            for (int j = i + 1; j < vetor.length; j++) {
                if (vetor[i] == vetor[j]) {
                    counter++;
                }
            }
            if (counter == 1) {
                conjuntoDeElementosNaoRepetidos[k] = vetor[i];
            }
            k++;
        }

        return conjuntoDeElementosNaoRepetidos;
    }

    public static int contarValoresNaoZeroNumArray(int[] array) {

        int counter = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0)
                counter++;

        }
        return counter;
    }

    public static int[] retirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidos(int[] array) {

        int[] arrayComZeros = retornarConjuntoDeElementosNaoRepetidosDeUmVetorIncluindoZerosDeSubstituicao(array);

        int[] arraySemZeros = new int[contarValoresNaoZeroNumArray(arrayComZeros)];

        int k = 0;

        for (int i = 0; i < arrayComZeros.length; i++) {
            if (arrayComZeros[i] != 0) {
                arraySemZeros[k] = arrayComZeros[i];
                k++;
            }

        }

        return arraySemZeros;
    }

    public static int[] retirarValoresZeroNumArray(int[] array) {


        int[] arraySemZeros = new int[contarValoresNaoZeroNumArray(array)];

        int k = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                arraySemZeros[k] = array[i];
                k++;
            }

        }

        return arraySemZeros;
    }

    // f)

    public static int[] inverterVetor(int[] array) {

        int[] arrayInvertido = new int[array.length];

        int k = 0;

        for (int i = array.length - 1; i >= 0; i--) {
            arrayInvertido[k] = array[i];
            k++;

        }

        return arrayInvertido;
    }

    // g)

    public static boolean verificarSeEPrimoNumeroNatural(int numero) {
        boolean flag = false;
        int counter = 0;

        if (numero == 1 || numero == 0 || numero < 0) {
            flag = false;

        } else {
            for (int i = 1; i <= numero; i++) {
                if (numero % i == 0) {
                    counter++;
                }
            }
        }
        if (counter == 2) {
            flag = true;
        }

        return flag;
    }

    public static int[] retornarSoElementosPrimosComZerosDeUmVetor(int[] array) {

        int[] soPrimosComZeros = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            if (verificarSeEPrimoNumeroNatural(array[i])) {
                soPrimosComZeros[i] = array[i];
            }
        }

        return soPrimosComZeros;
    }

    public static int[] retornarSoPrimosDeUmVetor(int[] array) {

        int[] primosComZeros = retornarSoElementosPrimosComZerosDeUmVetor(array);

        int[] primosSemZeros = retirarValoresZeroNumArray(primosComZeros);

        return primosSemZeros;
    }
}
