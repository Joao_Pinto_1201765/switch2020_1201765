package Bloco4.com.company;

public class Exercicio11 {

    public static boolean verificarSeDoisVetoresMesmoTamanho(int[] vetorUm, int[] vetorDois ){
        boolean flag = false;

        if (vetorUm.length == vetorDois.length)
            flag = true;

        return flag;
    }


    public static Integer produtoEscalarDeDoisVetores(int[] vetorUm, int[] vetorDois) {

        int produtoEscalar = 0;

        if (verificarSeDoisVetoresMesmoTamanho(vetorUm, vetorDois)) {
            for (int i = 0; i < vetorUm.length; i++) {
                produtoEscalar += vetorUm[i] * vetorDois[i];
            }

        } else
            return null;

        return produtoEscalar;
    }
}
