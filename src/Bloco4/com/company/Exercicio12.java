package Bloco4.com.company;

public class Exercicio12 {

    public static int verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(int[][] matriz) {

        int numeroDeColunas = 0, counterLinhasComColunasIguais = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz[0].length)
                counterLinhasComColunasIguais++;


        }
        if (counterLinhasComColunasIguais == matriz.length) {
            numeroDeColunas = matriz[0].length; //se positivo retornar o numero de colunas

        } else
            numeroDeColunas = matriz[0].length * -1;//se negativo retornar o numero de colunas maximo só que em negativo

        return numeroDeColunas;
    }

    public static int verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthChar(char[][] matriz) {
        int numeroDeColunas = 0, counterLinhasComColunasIguais = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz[0].length)
                counterLinhasComColunasIguais++;


        }
        if (counterLinhasComColunasIguais == matriz.length) {
            numeroDeColunas = matriz[0].length; //se positivo retornar o numero de colunas

        } else
            numeroDeColunas = matriz[0].length * -1;//se negativo retornar o numero de colunas maximo só que em negativo

        return numeroDeColunas;

    }
}
