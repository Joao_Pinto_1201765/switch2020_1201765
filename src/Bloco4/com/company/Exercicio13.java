package Bloco4.com.company;

public class Exercicio13 {

    public static boolean isSquare(int[][] matriz) {
        boolean flag = false;
        int counterDeLinhasComNColunasIguaisANumerodeLinhas = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz.length){
                counterDeLinhasComNColunasIguaisANumerodeLinhas++;
            }
        }
        if (counterDeLinhasComNColunasIguaisANumerodeLinhas == matriz.length && Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz) > 0){
            flag = true;
        }

        return flag;
    }

    public static boolean EMatrizQuadradaChar(char[][] matriz) {
        boolean flag = false;
        int counterDeLinhasComNColunasIguaisANumerodeLinhas = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz.length){
                counterDeLinhasComNColunasIguaisANumerodeLinhas++;
            }
        }
        if (counterDeLinhasComNColunasIguaisANumerodeLinhas == matriz.length && Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthChar(matriz) > 0){
            flag = true;
        }

        return flag;
    }
}
