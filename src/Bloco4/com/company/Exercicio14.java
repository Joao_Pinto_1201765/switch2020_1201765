package Bloco4.com.company;

public class Exercicio14 {

    public static boolean eMAtrizRectangular(int[][] matriz) {
        boolean flag = false;

        if (matriz.length < matriz[0].length || matriz.length > matriz[0].length) {
            flag = true;
        }

        if(Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz) < 0) {
            flag = false;
        }

        return flag;
    }
}
