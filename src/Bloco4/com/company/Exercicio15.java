package Bloco4.com.company;

public class Exercicio15 {

    public static int primeiroValorEncontradoEmMatrizDiferenteDeZero(int[][] matriz) {
        int primeiroValor = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] != 0) {
                    primeiroValor = matriz[i][j];
                    j = matriz[j].length;
                }
            }
        }

        return primeiroValor;

    }

    // a) Retornar elemento de menor valor numa matriz de inteiros

    public static int elementoDeMenorValorNumaMatriz(int[][] matriz) {
        int elementoDeMenorValor;

        elementoDeMenorValor = primeiroValorEncontradoEmMatrizDiferenteDeZero(matriz);

        for (int i = 0; i < matriz.length; i++) {

            for (int j = 0; j < matriz[i].length; j++) {

                if (matriz[i][j] < elementoDeMenorValor) {
                    elementoDeMenorValor = matriz[i][j];
                }
            }
        }

        return elementoDeMenorValor;
    }

    // b) Retornar elemento de maior valor numa matriz de inteiros

    public static int elementoDeMaiorValorNumaMatriz(int[][] matriz) {

        int elementoDeMaiorValor = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] > elementoDeMaiorValor)
                    elementoDeMaiorValor = matriz[i][j];

            }
        }
        return elementoDeMaiorValor;
    }

    // c)
    // Contador de Elementos

    public static int countTotalElementosNumaMatriz(int[][] matriz) {
        int counterElementosDaMatriz = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                counterElementosDaMatriz++;

            }
        }

        return counterElementosDaMatriz;
    }

    // Retornar Valor Médio da Matriz

    public static double mediaMatriz(int[][] matriz) {
        int somaTotal = 0;
        double media;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                somaTotal += matriz[i][j];

            }
        }

        media = (double) somaTotal / countTotalElementosNumaMatriz(matriz);

        return media;
    }

    // d) Retornar o Produto dos Elementos de uma Matriz

    public static int produtoMatriz(int[][] matriz) {
        int produtoTotal = 1;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                produtoTotal *= matriz[i][j];
            }
        }

        return produtoTotal;
    }

    // e)
    // Copiar Matriz
    public static int[][] copiarMatriz(int[][] matriz) {

        int[][] novaMatriz = new int[matriz.length][];

        for (int i = 0; i < matriz.length; i++) {

            novaMatriz[i] = new int[matriz[i].length];

            for (int j = 0; j < matriz[i].length; j++) {
                novaMatriz[i][j] = matriz[i][j];

            }
        }

        return novaMatriz;
    }

    // Replicar matriz com linhas e colunas iguais;

    public static int[][] matrizComLinhasEColunasIguaisComElementosAZero(int[][] matriz) {

        int[][] novaMatriz = new int[matriz.length][];

        for (int i = 0; i < matriz.length; i++) {

            novaMatriz[i] = new int[matriz[i].length];
            for (int j = 0; j < matriz[i].length; j++) {
                novaMatriz[i][j] = 0;

            }
        }

        return novaMatriz;
    }

    // Retornar elementos não Repetidos

    public static int[][] retirarOsNumerosRepetidosPorZerosNumaMatriz(int[][] matriz) {
        int[][] matrizSemNumerosRepetidos = matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        int counter;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {

                counter = 1;
                for (int k = j + 1; k < matriz[i].length; k++) {
                    if (matriz[i][j] == matriz[i][k])
                        counter++;

                }
                if (counter == 1) {
                    matrizSemNumerosRepetidos[i][j] = matriz[i][j];
                }

            }

        }

        return matrizSemNumerosRepetidos;

    }

    // f) Elementos Primos

    public static int[][] elementosPrimosDeUmaMatriz(int[][] matriz) {

        int[][] matrizPrimos = matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (Exercicio10.verificarSeEPrimoNumeroNatural(matriz[i][j]))
                    matrizPrimos[i][j] = matriz[i][j];

            }
        }

        return matrizPrimos;
    }

    // g) Diagonal Principal de Uma matriz

    public static int[][] diagonalPrincipal(int[][] matriz) {
        int[][] diagonalPrincipal = matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        int j = 0;

        if (Exercicio13.isSquare(matriz) || Exercicio14.eMAtrizRectangular(matriz)) {

            if (matriz.length > matriz[0].length) { // Para lidar com Rectagulares com numero de linhas superior ao que colunas

                for (int i = 0; i < matriz.length; i++) {
                    diagonalPrincipal[i][j] = matriz[i][j];
                    j++;
                    if (j == matriz[i + 1].length)
                        i = matriz.length;
                }

            } else { // para lidar com Quadradas e Rectangulares com numero de colunas superior ao de linhas
                for (int i = 0; i < matriz.length; i++) {
                    diagonalPrincipal[i][j] = matriz[i][j];
                    j++;
                }
            }

        } else
            return null;

        return diagonalPrincipal;
    }

    // h) Diagonal Secundária de Uma Matriz

    public static int[][] diagonalSecundaria(int[][] matriz) {
        int[][] diagonalSecundaria = matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        int k = matriz[0].length - 1;

        if (Exercicio13.isSquare(matriz) || Exercicio14.eMAtrizRectangular(matriz)) {

            if (matriz.length == matriz[0].length || matriz[0].length > matriz.length) {

                for (int i = 0; i < matriz.length; i++) {

                    diagonalSecundaria[i][k] = matriz[i][k];
                    k--;
                }
            } else { // Para lidar com Rectagulares com numero de linhas superior ao que colunas

                for (int i = 0; i < matriz.length; i++) {

                    diagonalSecundaria[i][k] = matriz[i][k];
                    k--;
                    if (k < 0) {
                        i = matriz.length;
                    }
                }
            }
        } else
            return null;

        return diagonalSecundaria;
    }

    // i)
    // Criar Matriz de Identidade Para Comparacao

    public static int[][] criarMatrizIdentidadeParaComparacao(int[][] matriz) {

        int[][] matrizIdentidadeParaComparacao = matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        int j = 0;

        for (int i = 0; i < matriz.length; i++) {
            matrizIdentidadeParaComparacao[i][j] = 1;
            j++;
        }

        return matrizIdentidadeParaComparacao;
    }

    // Verificar Se Matriz de Identidade

    public static boolean eMatrizIdentidade(int[][] matriz) {
        boolean flag = true;

        if (!Exercicio13.isSquare(matriz))
            flag = false;

        int[][] comparacao = criarMatrizIdentidadeParaComparacao(matriz);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] != comparacao[i][j]) {
                    flag = false;
                    j = matriz[0].length;
                    //break;
                }
            }
        }

        return flag;
    }

    // j)
    // Matriz inversa
    // Matriz multiplicada pela sua inversa resultará na matriz de identidade A.A1 = I

    public static double[][] matrizInversa(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz.length];


        if (!Exercicio13.isSquare(matriz) || Exercicio16.determinanteMatriz(matriz) == 0 || matriz.length == 1) { // Tem que ser quadrada e o determinante diferente de 0 para ser inversivel, se determinante = 0 é uma matriz singular;
            return null;

        } else if (matriz.length == 2) { // Metodo para matrizes de ordem 2
            matrizInversa = matrizInversaOrdemDois(matriz);

        } else // Matrizes de Ordem 3 ou Superior
            matrizInversa = matrizInversaOrdemTres(matriz);


        return matrizInversa;
    }

    // Matriz inversa de Ordem 2

    public static double[][] matrizInversaOrdemDois(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz.length];

        int i_aux = matriz.length - 1, j_aux = matriz.length - 1;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if ((i + j) % 2 != 0 && (double) matriz[i][j] / Exercicio16.determinanteMatriz(matriz) != 0.0) {
                    matrizInversa[i_aux][j_aux] = ((double) matriz[i][j] / Exercicio16.determinanteMatriz(matriz)) * -1; // inverter para negativo quando i + j é impar
                } else
                    matrizInversa[i_aux][j_aux] = (double) matriz[i][j] / Exercicio16.determinanteMatriz(matriz);

                i_aux--;
            }
            i_aux = matriz.length - 1;
            j_aux--;

        }

        return matrizInversa;
    }

    // Matriz inversa de Ordem 3 ou Superior
    // Menor Complementar
    // Matriz Adjunta

    // referência: https://programmer.help/blogs/using-java-to-describe-the-algorithm-of-matrix-inversion.html
    public static double[][] matrizInversaOrdemTres(int[][] matriz) {
        double[][] matrizInversa = new double[matriz.length][matriz.length];
        double determinante;

        determinante = (double) Exercicio16.determinanteMatriz(matriz);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (Exercicio15.matrizAdjunta(matriz)[i][j] / determinante == -0.0) {
                    matrizInversa[i][j] = 0.0;
                } else
                    matrizInversa[i][j] = Exercicio15.matrizAdjunta(matriz)[i][j] / determinante;

            }
        }

        return matrizInversa;
    }

    // Menor Complementar

    public static int[][] menorComplementar(int[][] matriz, int i, int j) {
        int[][] menorComplementar = new int[matriz.length - 1][matriz.length - 1];
        int linha = 0, coluna = 0;

        // i e j, linha e coluna a retirar; menor complementar de matriz[i][j]

        for (int k = 0; k < matriz.length; k++) {
            if (k == i) continue; // para uma iteração no ciclo se a condição for verdadeira
            // neste caso, não queremos iterar a linha i e coluna j

            for (int l = 0; l < matriz.length; l++) {
                if (l == j) continue;
                menorComplementar[linha][coluna] = matriz[k][l]; // introdução dos valores na matriz complementar menor
                coluna++;

                if (coluna == matriz.length - 1) {
                    coluna = 0;
                    linha++;
                }
            }
        }

        return menorComplementar;
    }

    // Matriz adjunta

    public static double[][] matrizAdjunta(int[][] matriz) {
        double[][] matrizAdjunta = new double[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                double auxiliar = Math.pow(-1, i + j) * (double) Exercicio16.determinanteMatriz(Exercicio15.menorComplementar(matriz, i, j));
                if (Math.abs(auxiliar) <= 10e-6) // converter menor que 0.025 em 0.0
                    auxiliar = 0;
                matrizAdjunta[i][j] = auxiliar;

            }
        }

        matrizAdjunta = Exercicio15.matrizTransposta(matrizAdjunta); // transpor matriz

        return matrizAdjunta;
    }

    // k) Matriz Transposta

    public static double[][] matrizTransposta(double[][] matriz) {
        double[][] matrizTransposta = new double[matriz[0].length][matriz.length]; //inversao das linhas com colunas

        for (int i = 0; i < matrizTransposta[0].length; i++) {
            for (int j = 0; j < matrizTransposta.length; j++) {
                matrizTransposta[j][i] = matriz[i][j];

            }

        }

        return matrizTransposta;
    }

    public static int[][] matrizTransposta(int[][] matriz) {
        int[][] matrizTransposta = new int[matriz[0].length][matriz.length]; //inversao das linhas com colunas

        for (int i = 0; i < matrizTransposta[0].length; i++) {
            for (int j = 0; j < matrizTransposta.length; j++) {
                matrizTransposta[j][i] = matriz[i][j];

            }

        }

        return matrizTransposta;
    }
}
