package Bloco4.com.company;

public class Exercicio16 {

    // Calcular Determinate de Matriz com Teorema de LaPalce

    public static Integer determinanteMatriz(int[][] matriz) {
        int determinante;

        if (Exercicio13.isSquare(matriz)) { // Assegurar que é quadrada

            if (matriz.length == 1) { // Ordem 1 - se so contem um elemento

                determinante = matriz[0][0];

            } else if (matriz.length == 2) { // Ordem 2

                determinante = determinanteMatrizDeOrdemDois(matriz);

            } else if (matriz.length == 3) { // Ordem 3 regra de Sarrus

                determinante = determinanteMatrizDeOrdemTres(matriz);

            } else // Teorema de LaPlace

                determinante = determinanteMatrizDeOrdemQuatroOuSuperior(matriz);


        } else
            return null;


        return determinante;
    }

    public static int determinanteMatrizDeOrdemDois(int[][] matriz) {
        return (matriz[0][0] * matriz[1][1]) - (matriz[0][1] * matriz[1][0]);
    }

    public static int determinanteMatrizDeOrdemTres(int[][] matriz) {
        int determinante;

        determinante = (matriz[0][0] * matriz[1][1] * matriz[2][2]) +
                (matriz[0][1] * matriz[1][2] * matriz[2][0]) +
                (matriz[0][2] * matriz[1][0] * matriz[2][1]) -
                (matriz[0][2] * matriz[1][1] * matriz[2][0]) -
                (matriz[0][0] * matriz[1][2] * matriz[2][1]) -
                (matriz[0][1] * matriz[1][0] * matriz[2][2]);

        return determinante;
    }

    public static int determinanteMatrizDeOrdemQuatroOuSuperior(int[][] matriz) {
        int determinante = 0, i_aux, j_aux;
        int[][] auxiliar = Exercicio15.matrizComLinhasEColunasIguaisComElementosAZero(matriz);
        int[][] copiaMatriz = Exercicio15.copiarMatriz(matriz);

        do {
            // Teorema de LaPlace até Matriz de Ordem 3, depois utilizar Regra de Sarrus
            for (int i = 0; i < copiaMatriz.length; i++) { // linha pivot = Zero
                if (copiaMatriz[0][i] != 0) { // elemento pivot
                    auxiliar = new int[copiaMatriz.length - 1][copiaMatriz.length - 1]; //matriz auxiliar de n-1
                    j_aux = 0;
                    i_aux = 0;
                    for (int j = 1; j < copiaMatriz.length; j++) { // começa no um porque a zero é a linha de pivo
                        for (int k = 0; k < copiaMatriz.length; k++) {
                            if (k != i) { // if para ser diferente do i; não podemos usar elementos da mesma linha ou coluna do pivo escolhido e para cada um a linha e coluna ignorada é diferente
                                auxiliar[i_aux][j_aux] = copiaMatriz[j][k];
                                j_aux++;
                            }
                        }
                        i_aux++;
                        j_aux = 0;
                    }
                    /*
                     calcula a parcela do pivo e soma ao total do determinante
                     para calcular o determinante auxiliar de n-1 o teorema de LaPlace desempilha até ordem 3
                     onde aí utiliza a regra de Sarrus
                     matriz[0][i] elemento pivo da linha pivot fixo matriz[0]
                     */
                    determinante += Math.pow(-1, i) * copiaMatriz[0][i] * Exercicio16.determinanteMatriz(auxiliar);
                }
            }
            copiaMatriz = Exercicio15.copiarMatriz(auxiliar);
        } while (auxiliar.length != 3);

        return determinante;
    }
}


