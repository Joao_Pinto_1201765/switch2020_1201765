package Bloco4.com.company;

public class Exercicio17 {

    // Exercicio 17
    //a)
    //Calcular o Produto de uma Matriz por uma Constante

    public static int[][] produtoMatrizPorConstante(int[][] matriz, int constante) {
        int[][] matrizProdutoConstante = new int[matriz.length][matriz[0].length];

        if (matriz[0].length > 0 && matriz.length > 0) {

            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {

                    matrizProdutoConstante[i][j] = matriz[i][j] * constante;

                }
            }
        } else
            return null;

        return matrizProdutoConstante;
    }

    // b)
    // Soma de Duas Matrizes

    public static int[][] somaDuasMatrizes(int[][] matrizUm, int[][] matrizDois) {
        int[][] somaMatrizes = new int[matrizUm.length][matrizUm.length];

        // Assegurar que as duas matrizes tem o mesmo numero de linhas e colunas
        if (Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matrizUm) == Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matrizDois) && matrizUm.length == matrizDois.length) {

            for (int i = 0; i < matrizUm.length; i++) {
                for (int j = 0; j < matrizUm[i].length; j++) {
                    // soma de [i][j] da matrizUm e matrizDois fica guardada na matriz somaMatrizes[i][j]
                    somaMatrizes[i][j] = matrizUm[i][j] + matrizDois[i][j];

                }
            }
        } else
            return null;

        return somaMatrizes;
    }

    //c)
    // Produto de Duas Matrizes

    public static int[][] produtoDuasMatrizes(int[][] matrizUm, int[][] matrizDois) {

        if (matrizUm == null || matrizDois == null)
            return null;

        int[][] produtoMatrizes = new int[matrizDois[0].length][matrizUm.length];
        //O numero de linhas em matrizProduto = numero colunas da matriz dois
        //O numero de colunas em matrizProduto = numero linhas da matriz um
        if (matrizUm[0].length == matrizDois.length) {
            for (int i = 0; i < matrizUm.length; i++) {
                for (int j = 0; j < matrizDois[i].length; j++) {
                    //para cada produtosMatrizes[i][j]
                    //soma-se o produto da linha(matrizUm) e coluna(matrizDois)
                    //p.e produtoMatrizes[0][0] = matrizUm[0][0] * matrizDois[0][0] + matrizUm[0][1] * matrizDois[1][0]
                    for (int k = 0; k < matrizUm[i].length; k++) {
                        produtoMatrizes[i][j] += matrizUm[i][k] * matrizDois[k][j];

                    }
                }
            }
        } else
            return null;

        return produtoMatrizes;
    }
}
