package Bloco4.com.company;

/**
 * Sudoku
 */
public class Exercicio19 {
    //Exercicio 19
    /**
     * Restriçao geral: linha e coluna é de 1 a 9, o tratamento destes valores em matrizes implica a subtração de um para corresponder à localização certa
     */
    // a), c) e e)

    /**
     * Gerar Matriz Mascara Da Matriz de Jogo Inicial
     *
     * @param matrizBase Matriz de Jogo Inicial
     * @return Matriz Mascara Inicial
     */
    public static int[][] matrizMascaraInicial(int[][] matrizBase) {
        if (Exercicio18.isMatrixNull(matrizBase) || !Exercicio13.isSquare(matrizBase) || !eMatrizNovePorNove(matrizBase))
            return null;

        int identificadorDePresencaDeNumeroNaMatrizBase = 1;
        int[][] matrizMascaraInicial = new int[matrizBase.length][matrizBase.length]; // MatrizQuadrada

        for (int i = 0; i < matrizBase.length; i++) {
            for (int j = 0; j < matrizBase[i].length; j++) {
                if (eDiferenteDeZero(matrizBase[i][j])) // Se !ZERO é numero presente na matrizBase
                    matrizMascaraInicial[i][j] = identificadorDePresencaDeNumeroNaMatrizBase;

            }
        }

        return matrizMascaraInicial;
    }

    public static boolean eMatrizNovePorNove(int[][] matrizBase) {
        return matrizBase.length == 9 && matrizBase[0].length == 9;
    }

    /**
     * Gerar nova Matriz de Jogo Após jogada em local onde não há numero presente na Matriz de Jogo Inicial
     *
     * @param matrizJgo            Matriz de Jogo - no inicio ou a matriz atual apos jogadas
     * @param matrizMascaraInicial Matriz Mascara Inicial -- Definida no inicio do jogo
     * @param linha                Linha a jogar
     * @param coluna               Coluna a jogar
     * @param numeroAJogar         Numero a joagar
     * @return Matriz Atualizada apos a jogada
     */

    public static int[][] matrizAtualizdaComJogada(int[][] matrizJgo, int[][] matrizMascaraInicial, int linha, int coluna, int numeroAJogar) {
        if (foraDosLimitesDeJogo(matrizJgo, linha, coluna) || !eNumeroValidoSudoku(numeroAJogar))
            return null;

        if (!eDiferenteDeZero(matrizMascaraInicial[linha - 1][coluna - 1]))
            matrizJgo[linha - 1][coluna - 1] = numeroAJogar;

        return matrizJgo;
    }

    private static boolean eNumeroValidoSudoku(int numero) {
        return numero > 0 && numero <= 9;
    }

    private static boolean foraDosLimitesDeJogo(int[][] matrizJgo, int linha, int coluna) {
        return linha > matrizJgo.length || coluna > matrizJgo[0].length;
    }

    public static boolean eDiferenteDeZero(int i1) {
        return i1 != 0;
    }

    // b)

    /**
     * Verificar se Existem Celulas Por Preencher
     *
     * @param matrizJogo Matriz de Jogo Atual
     * @return boolean
     */

    public static boolean existeCelulasPorPreencherNaMatriz(int[][] matrizJogo) {
        boolean existeCelulasPorPreencherNaMatriz = false;

        for (int i = 0; i < matrizJogo.length && !existeCelulasPorPreencherNaMatriz; i++) {
            for (int j = 0; j < matrizJogo[i].length && !existeCelulasPorPreencherNaMatriz; j++) {
                if (!eDiferenteDeZero(matrizJogo[i][j])) {
                    existeCelulasPorPreencherNaMatriz = true;
                }
            }

        }

        return existeCelulasPorPreencherNaMatriz;
    }

    // d)

    /**
     * Verificar Validade da Jogada por bloco, linha e coluna
     * Não pode haver numeros iguais no bloco, linha e coluna
     *
     * @param matrizJogo           Matriz de Jogo
     * @param matrizMascaraInicial Matriz Mascara da Matriz de Jogo Inicial
     * @param linha                Linha a jogar
     * @param coluna               Coluna a jogar
     * @param numeroAJogar         Numero a colocar
     * @return boolean
     */

    public static boolean verificarValidadeDaJogada(int[][] matrizJogo, int[][] matrizMascaraInicial, int linha, int coluna, int numeroAJogar) {
        boolean jogadaValida = false;

        if (!eDiferenteDeZero(matrizMascaraInicial[linha - 1][coluna - 1])) {

            if (eJogadaValidaLinha(matrizJogo, coluna, numeroAJogar) && eJogadaValidaColuna(matrizJogo, linha, numeroAJogar) && eJogadaValidaBloco(matrizJogo, linha, coluna, numeroAJogar))
                jogadaValida = true;
        }

        return jogadaValida;
    }

    /**
     * Verificar Se Jogada é Validada por Bloco
     * Divisão da matriz 9X9 por blocos de 3X3, iniciando a contagem no bloco Superior Esquerdo = 1, até bloco Inferior Direito = 9
     *
     * @param matrizJogo   Matriz de Jogo
     * @param linha        Linha a Jogar
     * @param coluna       Coluna a Jogar
     * @param numeroAJogar Numero a Colocar
     * @return boolean
     */

    public static boolean eJogadaValidaBloco(int[][] matrizJogo, int linha, int coluna, int numeroAJogar) {
        boolean jogadaValida = true;
        int bloco = linhaEColunaPertenceAoBloco(linha, coluna);

        switch (bloco) {
            case 1:
                jogadaValida = eJogadaValidaBlocoUm(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 2:
                jogadaValida = eJogadaValidaBlocoDois(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 3:
                jogadaValida = eJogadaValidaBlocoTres(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 4:
                jogadaValida = eJogadaValidaBlocoQuatro(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 5:
                jogadaValida = eJogadaValidaBlocoCinco(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 6:
                jogadaValida = eJogadaValidaBlocoSeis(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 7:
                jogadaValida = eJogadaValidaBlocoSete(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 8:
                jogadaValida = eJogadaValidaBlocoOito(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 9:
                jogadaValida = eJogadaValidaBlocoNove(matrizJogo, numeroAJogar, jogadaValida);
                break;
        }


        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoNove(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoOito(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoSete(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoSeis(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoCinco(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoQuatro(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoTres(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoDois(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoUm(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaColuna(int[][] matrizJogo, int linha, int numeroAJogar) {
        boolean jogadaValida = true;
        for (int i = 0; i < matrizJogo[linha - 1].length; i++) {
            if (matrizJogo[linha - 1][i] == numeroAJogar)
                jogadaValida = false;

        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaLinha(int[][] matrizJogo, int coluna, int numeroAJogar) {
        boolean jogadaValida = true;
        for (int i = 0; i < matrizJogo.length; i++) {
            if (matrizJogo[i][coluna - 1] == numeroAJogar)
                jogadaValida = false;

        }
        return jogadaValida;
    }

    /**
     * Classificar atraves da linha e coluna o bloco pertencente
     *
     * @param linha  Linha a Jogar
     * @param coluna Coluna a Jogar
     * @return int = numero do bloco
     */

    public static int linhaEColunaPertenceAoBloco(int linha, int coluna) {
        int bloco;

        if (blocoUm(linha, coluna)) {
            bloco = 1;

        } else if (blocoDois(linha, coluna)) {
            bloco = 2;

        } else if (blocoTres(linha, coluna)) {
            bloco = 3;

        } else if (blocoQuatro(linha, coluna)) {
            bloco = 4;

        } else if (blocoCinco(linha, coluna)) {
            bloco = 5;

        } else if (blocoSeis(linha, coluna)) {
            bloco = 6;

        } else if (blocoSete(linha, coluna)) {
            bloco = 7;

        } else if (blocoOito(linha, coluna)) {
            bloco = 8;

        } else if (blocoNove(linha, coluna)) {
            bloco = 9;

        } else
            bloco = -1; // retorna -1 quando coordenadas excedem os limites da matriz de jogo

        return bloco;
    }

    public static boolean blocoNove(int linha, int coluna) {
        return linha < 10 && coluna < 10;
    }

    public static boolean blocoOito(int linha, int coluna) {
        return linha < 10 && coluna < 7;
    }

    public static boolean blocoSete(int linha, int coluna) {
        return linha < 10 && coluna < 4;
    }

    public static boolean blocoSeis(int linha, int coluna) {
        return linha < 7 && coluna < 10;
    }

    public static boolean blocoCinco(int linha, int coluna) {
        return linha < 7 && coluna < 7;
    }

    public static boolean blocoQuatro(int linha, int coluna) {
        return linha < 7 && coluna < 4;
    }

    public static boolean blocoTres(int linha, int coluna) {
        return linha < 4 && coluna < 10;
    }

    public static boolean blocoDois(int linha, int coluna) {
        return linha < 4 && coluna < 7;
    }

    public static boolean blocoUm(int linha, int coluna) {
        return linha < 4 && coluna < 4;
    }

    // f)

    /**
     * Veririfcar se o jogo terminou com sucesso (matriz de jogo totalmente preenchida e válida)
     * Assegurando que foram feitas verificações a cada jogada por linha, coluna e bloco
     *
     * @param matrizJogo Matriz de jogo final/a verificar
     * @return boolean
     */

    public static boolean verificarSeJogoTerminouComSucesso(int[][] matrizJogo) {
        boolean jogoTerminouComSucesso = false;

        if (!existeCelulasPorPreencherNaMatriz(matrizJogo)) {
            if (verificarSeMatrizDeJogoEValida(matrizJogo))
                jogoTerminouComSucesso = true;
        }

        return jogoTerminouComSucesso;
    }

    public static boolean verificarSeMatrizDeJogoEValida(int[][] matrizJogo) {
        boolean matrizValidada = false;
        double produto = 0;
        double SOMA_MATRIZ_SUDOKU_VALIDA = 405;

        for (int i = 0; i < matrizJogo.length; i++) {
            for (int j = 0; j < matrizJogo[i].length; j++) {
                produto += matrizJogo[i][j];
            }
        }

        if (produto == SOMA_MATRIZ_SUDOKU_VALIDA)
            matrizValidada = true;

        return matrizValidada;
    }
}
