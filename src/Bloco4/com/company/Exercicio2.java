package Bloco4.com.company;

public class Exercicio2 {

    public static int [] retornarVetorComElementosDeNumeroDado(int numeroDado) {

        int[] vetor = new int[Exercicio1.numeroDeDigitosDeUmNumero(numeroDado)];
        int algarismo;


        for (int i = vetor.length - 1; i >= 0; i--) {
            algarismo = numeroDado % 10;
            vetor[i] = algarismo;
            numeroDado /= 10;
        }

        return vetor;
    }
}
