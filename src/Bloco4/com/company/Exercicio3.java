package Bloco4.com.company;

public class Exercicio3 {

    public static int somaDosElementosDeUmVetor(int [] vetor) {

        int soma = 0;

        for (int i = 0; i < vetor.length; i++) {
            soma += vetor[i];
        }

        return soma;
    }
}
