package Bloco4.com.company;

import Bloco3.com.company.Exercicio19;

public class Exercicio4 {

    public static int [] retornarArrayApenasComParesDeArrayRecebido(int [] numeros) {

        int [] vetorPares = Exercicio19.retirarOsParesDeUmArray(numeros);

        return vetorPares;
    }

    public static int [] retornarArrayApenasComImparesDeArrayRecebido(int [] numeros) {

        int [] vetorImpares = Exercicio19.retirarOsImparesDeUmArray(numeros);

        return vetorImpares;
    }
}
