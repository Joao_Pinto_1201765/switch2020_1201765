package Bloco4.com.company;

public class Exercicio5 {

    public static int somaParesDeUmArray(int [] array) {

        int somaParesDeUmArray = 0;

        int [] pares = Exercicio4.retornarArrayApenasComParesDeArrayRecebido(array);

        for (int i = 0; i < pares.length; i++) {
            somaParesDeUmArray += pares[i];

        }

        return somaParesDeUmArray;
    }

    public static int somaImparesDeUmArray(int [] array) {

        int somaImparesDeUmArray = 0;

        int [] impares = Exercicio4.retornarArrayApenasComImparesDeArrayRecebido(array);

        for (int i = 0; i < impares.length; i++) {
            somaImparesDeUmArray += impares[i];

        }

        return somaImparesDeUmArray;
    }
}
