package Bloco4.com.company;

public class Exercicio6 {

    public static int [] retornarVetorComOsPrimeirosNElementosDeUmArray(int [] array, int numeroDeElementos) {

        int [] vetorComOsPrimeirosNElementosDeUmArray = new int[numeroDeElementos];

        for (int i = 0; i < vetorComOsPrimeirosNElementosDeUmArray.length; i++) {
            vetorComOsPrimeirosNElementosDeUmArray[i] = array[i];

        }

        return vetorComOsPrimeirosNElementosDeUmArray;
    }

    public static char [] retornarVetorComOsPrimeirosNElementosDeUmArray(char [] array, int numeroDeElementos) {

        char [] vetorComOsPrimeirosNElementosDeUmArray = new char[numeroDeElementos];

        for (int i = 0; i < vetorComOsPrimeirosNElementosDeUmArray.length; i++) {
            vetorComOsPrimeirosNElementosDeUmArray[i] = array[i];

        }

        return vetorComOsPrimeirosNElementosDeUmArray;
    }
}
