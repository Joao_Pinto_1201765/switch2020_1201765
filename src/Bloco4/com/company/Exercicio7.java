package Bloco4.com.company;

public class Exercicio7 {

    public static int[] criarArrayDadoIntrevalo(int limiteInferior, int limiteSuperior) {

        int[] arrayDadoUmIntrevalo = new int[Exercicio7.calcularDimensaoDeArrayParaIntrevalo(limiteInferior, limiteSuperior)];

        int k = 0;

        for (int i = limiteInferior; i <= limiteSuperior; i++) { // inclusive limiteInferior e limiteSuperior
            arrayDadoUmIntrevalo[k] = i;
            k++;

        }

        return arrayDadoUmIntrevalo;
    }

    public static int calcularDimensaoDeArrayParaIntrevalo(int limiteInferior, int limiteSuperior) {

        int intrevalo;

        intrevalo = limiteSuperior - limiteInferior + 1; // inclusive limteInferior e limiteSuperior

        return intrevalo;
    }

    public static int[] multiplosDeNDadoUmIntrevalo(int limiteInferior, int limiteSuperior, int n) {

        int[] array = Exercicio7.criarArrayDadoIntrevalo(limiteInferior, limiteSuperior);
        int counterMultiplosDeN = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % n == 0) {
                counterMultiplosDeN++;
            }
        }

        int[] multiplosDeNDadoUmIntrevalo = new int[counterMultiplosDeN];
        int k = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % n == 0){
                multiplosDeNDadoUmIntrevalo[k] = array[i];
                k++;
            }
        }

        return multiplosDeNDadoUmIntrevalo;
    }
}
