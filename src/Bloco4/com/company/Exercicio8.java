package Bloco4.com.company;

public class Exercicio8 {

    public static int counterMultiposDeNArrayDeArray(int limiteInferior, int limiteSuperior, int[] n) {

        int[] array = Exercicio7.criarArrayDadoIntrevalo(limiteInferior, limiteSuperior);
        int counterMulti = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < n.length; j++) {
                if (array[i] % n[j] == 0)
                    counterMulti++;

            }
        }

        return counterMulti;
    }

    public static int[] criarArrayDosMultiplosdeNArray(int limiteInferior, int limiteSuperior, int[] n) {

        int[] array = Exercicio7.criarArrayDadoIntrevalo(limiteInferior, limiteSuperior);
        int[] arrayDeMultiplos = new int[Exercicio8.counterMultiposDeNArrayDeArray(limiteInferior, limiteSuperior, n)];

        int l = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < n.length; j++) {
                if (array[i] % n[j] == 0) {
                    arrayDeMultiplos[l] = array[i];
                    l++;
                }
            }
        }

        return arrayDeMultiplos;
    }

    public static int counterMultiplosComunsDeN(int limiteInferior, int limiteSuperior, int[] n) {

        int[] arrayDeMultiplos = Exercicio8.criarArrayDosMultiplosdeNArray(limiteInferior, limiteSuperior, n);

        int counter, counterfinal = 0;

        for (int i = 0; i < arrayDeMultiplos.length; i++) {

            counter = 0;

            for (int j = 1; j < arrayDeMultiplos.length; j++) {
                if (arrayDeMultiplos[i] == arrayDeMultiplos[j]) {
                    counter++;
                    if (counter == n.length)
                        counterfinal++;
                }
            }
        }

        //counterfinal = counterfinal / n.length;

        return counterfinal;

    }

    public static int[] criarArrayComOsMultiplosComunsRepetidos(int limiteInferior, int limiteSuperior, int[] n) {

        int[] arrayComMultiplosComuns = new int[counterMultiplosComunsDeN(limiteInferior, limiteSuperior, n)];
        int[] arrayDeMultiplos = Exercicio8.criarArrayDosMultiplosdeNArray(limiteInferior, limiteSuperior, n);

        int counter, k = 0;


        for (int i = 0; i < arrayDeMultiplos.length; i++) {

            counter = 0;

            for (int j = 1; j < arrayDeMultiplos.length; j++) {
                if (arrayDeMultiplos[i] == arrayDeMultiplos[j]) {
                    counter++;
                    if (counter == n.length) {
                        arrayComMultiplosComuns[k] = arrayDeMultiplos[j];
                        k++;
                    }
                }
            }
        }


        return arrayComMultiplosComuns;
    }

    public static int[] retornarSoUmaVezARepeticao(int limiteInferior, int limiteSuperior, int[] n) {

        int[] arrayComMultiplosComuns = Exercicio8.criarArrayComOsMultiplosComunsRepetidos(limiteInferior, limiteSuperior, n);

        int[] arrayDeMultiplosComSoUmaRepeticao = new int[counterMultiplosComunsDeN(limiteInferior, limiteSuperior, n) / 2];

        int counter, z = 0;

        for (int i = 0; i < arrayComMultiplosComuns.length; i++) {

            counter = 1;

            for (int j = i+1; j < arrayComMultiplosComuns.length; j++) {
                if (arrayComMultiplosComuns[i] == arrayComMultiplosComuns[j]) {
                    counter++;
                }
            }
            if (counter == n.length) {
                arrayDeMultiplosComSoUmaRepeticao[z] = arrayComMultiplosComuns[i];
                z++;
            }

        }

        return arrayDeMultiplosComSoUmaRepeticao;

    }
}