package Bloco4.com.company;

import Bloco3.com.company.Bloco3;

public class Exercicio9 {

    public static boolean verificarCapicua(int numero){
        boolean flag = false;

        if (Bloco3.isCapicua(numero))
            flag = true;

        return flag;
    }
}
