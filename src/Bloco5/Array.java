package Bloco5;

import Bloco3.com.company.Bloco3;

public class Array {

    // atributes

    private int[] array;


    // constructors
    // a)
    public Array() {
        this.array = new int[0];
    }

    // b)
    public Array(int[] initialArray) {
        isArrayEmpty(initialArray);
        this.array = new int[initialArray.length];

        for (int i = 0; i < initialArray.length; i++) {
            this.array[i] = initialArray[i];
        }
    }

    // getter and setters


    public int[] toArray() {
        return copyArray(array, array.length);
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    // methods
    // c)
    public void addToArray(int intToAdd) {
        int[] tempArray = new int[this.array.length + 1];

        for (int i = 0; i < tempArray.length - 1; i++) {
            tempArray[i] = this.array[i];
        }

        tempArray[tempArray.length - 1] = intToAdd;

        this.array = tempArray;
    }

    // d)
    public void removeFirstElementOf(int elementToRemove) {
        isArrayEmpty();
        //throw new IllegalArgumentException("Element to remove isn't in Array");

        int[] array = copyArray(this.array, this.array.length - 1);
        int k = 0;
        if (isInArray(elementToRemove)) {

            for (int i = 0; i < this.array.length; i++) {
                if (equals(this.array[i], elementToRemove) && k == i) {
                    continue;
                } else {
                    array[k] = this.array[i];
                    k++;
                }
            }
        } else {
            this.array = copyArray(this.array, this.array.length);
        }
        this.array = array;
    }

    // n)
    private boolean isEmpty() {
        return this.array.length == 0;
    }

    private boolean isEmpty(int[] array) {
        return array == null || array.length == 0;
    }

    private void isArrayEmpty() {
        if (isEmpty())
            throw new IllegalArgumentException("Empty Array");
    }

    private void isArrayEmpty(int[] array) {
        if (isEmpty(array))
            throw new IllegalArgumentException("Empty Array");
    }

    private boolean isInArray(int element) {
        for (int i = 0; i < this.array.length; i++) {
            if (equals(this.array[i], element))
                return true;
        }
        return false;
    }

    public int[] copyArray(int[] array, int size) {
        int[] copyArray = new int[size];

        for (int i = 0; i < copyArray.length; i++) {
            copyArray[i] = array[i];
        }
        return copyArray;
    }

    // e)
    public int valueInPosition(int position) {
        if (outOfBoundsInArray(position))
            throw new IllegalArgumentException("Position Out Of Bounds");

        return this.array[position];
    }

    private boolean outOfBoundsInArray(int position) {
        return position < 0 || position >= this.array.length;
    }

    //f)
    public int length() {
        return this.array.length;
    }

    // g)
    public int largerValueInArray() {
        isArrayEmpty();

        int largerValueInArray = this.array[0];

        for (int i = 1; i < this.array.length; i++) {
            if (this.array[i] > largerValueInArray)
                largerValueInArray = this.array[i];
        }
        return largerValueInArray;
    }

    // h)
    public int smallerValueInArray() {
        isArrayEmpty();

        int smallerValueInArray = this.array[0];

        for (int i = 1; i < this.array.length; i++) {
            if (this.array[i] < smallerValueInArray)
                smallerValueInArray = this.array[i];
        }
        return smallerValueInArray;
    }

    // i)
    public double meanOfElementsInArray() {
        isArrayEmpty();
        return Bloco4.com.company.Exercicio10.mediaValoresDeUmVetor(this.array);
    }

    // j)
    public int sumOfElementsInArray() {
        return Bloco4.com.company.Exercicio10.somaValoresDeUmVetor(this.array);
    }

    public double meanOfEvenElements() {
        isArrayEmpty();

        double sumOfEvenElements = 0, numberOfEvenElements = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (isEven(this.array[i])) {
                sumOfEvenElements += this.array[i];
                numberOfEvenElements++;
            }
        }
        if (isZero(numberOfEvenElements)) return 0;

        return sumOfEvenElements / numberOfEvenElements;
    }

    // k)
    public double meanOfOddElements() {
        isArrayEmpty();

        double sumOfOddElements = 0, numberOfOddElements = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (!isEven(this.array[i])) {
                sumOfOddElements += this.array[i];
                numberOfOddElements++;
            }
        }
        if (isZero(numberOfOddElements)) return 0;

        return sumOfOddElements / numberOfOddElements;
    }

    private boolean isZero(double number) {
        return number == 0;
    }

    private boolean isEven(int i) {
        return i % 2 == 0 && i != 0;
    }

    // l)
    public double meanOfMultiplesOfAGivenNumber(int givenNumber) {
        isArrayEmpty();
        if (isZero(givenNumber))
            throw new IllegalArgumentException("Given Number is Zero");

        double sumOfMutiples = 0, numberOfMultiples = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (isMultiple(givenNumber, this.array[i])) {
                sumOfMutiples += this.array[i];
                numberOfMultiples++;
            }
        }
        if (isZero(numberOfMultiples)) return 0;

        return sumOfMutiples / numberOfMultiples;
    }

    private boolean isMultiple(int i1, int p) {
        return p % i1 == 0;
    }

    // m)
    public int compareByValue(int valueOne, int valueTwo) {
        return Integer.compare(valueOne, valueTwo);
    }

    public void sortAsc() {
        isArrayEmpty();
        int temp;

        for (int i = 0; i < this.array.length; i++) {
            for (int j = i + 1; j < this.array.length; j++) {
                if (compareByValue(this.array[j], this.array[i]) < 0) {
                    temp = this.array[i];
                    this.array[i] = this.array[j];
                    this.array[j] = temp;
                }
            }
        }
    }

    public void sortDesc() {
        isArrayEmpty();
        int temp;

        for (int i = 0; i < this.array.length; i++) {
            for (int j = i + 1; j < this.array.length; j++) {
                if (compareByValue(this.array[j], this.array[i]) > 0) {
                    temp = this.array[i];
                    this.array[i] = this.array[j];
                    this.array[j] = temp;
                }
            }
        }
    }

    // o)
    public boolean haveJustOneElement() {
        return equals(this.array.length, 1);
    }

    // p)
    public boolean allElementsAreEven() {
        isArrayEmpty();
        int numberOfEvenElements = 0;

        for (int values : this.array) {
            if (isEven(values))
                numberOfEvenElements++;
        }
        return equals(numberOfEvenElements, this.array.length);
    }

    // q)
    public boolean allElementsAreOdd() {
        isArrayEmpty();
        int numberOfOddElements = 0;

        for (int values : this.array) {
            if (isOdd(values))
                numberOfOddElements++;
        }
        return equals(numberOfOddElements, this.array.length);
    }

    private boolean isOdd(int value) {
        return !isEven(value) && value != 0;
    }

    // r)
    public boolean haveDuplicates() {
        isArrayEmpty();
        for (int i = 0; i < this.array.length; i++) {
            for (int j = i + 1; j < this.array.length; j++) {
                if (Array.this.equals(this.array[i], this.array[j]))
                    return true;
            }
        }
        return false;
    }

    private boolean equals(int p, int p2) {
        return p == p2;
    }

    // s)
    protected int meanOfValueLength() {
        int sumOfValuesLengths = 0;
        String value;
        for (int i = 0; i < this.array.length; i++) {
            value = toString(Math.abs(this.array[i]));
            sumOfValuesLengths += value.length();
        }
        return sumOfValuesLengths / this.array.length;
    }

    private String toString(int value) {
        return String.valueOf(value);
    }

    private int lengthOfString(String string) {
        return string.length();
    }

    public int[] elementsBiggerThanTheMeanLength() {
        isArrayEmpty();
        int[] elementsBiggerThanTheMean = new int[this.array.length];
        int newArrayIndex = 0;
        int meanOfValueLength = meanOfValueLength();

        for (int i = 0; i < this.array.length; i++) {
            if (lengthOfString(toString(this.array[i])) > meanOfValueLength) {
                elementsBiggerThanTheMean[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(elementsBiggerThanTheMean, newArrayIndex);
    }

    // t)
    private double percentageOfEvenDigits(int number) {
        int evenCounter = 0, digitCounter = 0, iterationWithMultiplesOfTen = 10;
        int iterator = number;
        while (iterator != 0) {
            if (isEven(iterator)) {
                evenCounter++;
            }
            iterator /= 10;
            digitCounter++;
        }

        return (double) evenCounter / digitCounter;
    }

    private double percentageOfEvenDigits() {
        int evenCounter = 0, digitCounter = 0, iterationWithMultiplesOfTen = 10;

        for (int i = 0; i < this.array.length; i++) {
            int iterator = this.array[i];
            while (iterator != 0) {
                if (isEven(iterator)) {
                    evenCounter++;
                }
                iterator /= 10;
                digitCounter++;
            }
        }
        return (double) evenCounter / digitCounter;
    }

    public int[] elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements() {
        isArrayEmpty();
        int[] arrayWithElements = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (percentageOfEvenDigits(this.array[i]) > percentageOfEvenDigits()) {
                arrayWithElements[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(arrayWithElements, newArrayIndex);
    }

    // u)
    public int[] elementsWithAllEvenDigits() {
        isArrayEmpty();
        int[] arrayWithElements = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (percentageOfEvenDigits(this.array[i]) == 1) {
                arrayWithElements[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }

        }
        return copyArray(arrayWithElements, newArrayIndex);
    }

    // v)
    private boolean hasAscSequenceOfDigits(int number) {
        int aux, aux_2;
        int iterator = number;

        aux = iterator % 10;
        iterator /= 10;

        while (iterator != 0) {
            aux_2 = iterator % 10;
            if (aux <= aux_2)
                return false;
            aux = aux_2;
            iterator /= 10;
        }
        return true;
    }

    public int[] elementsWhithAscSequenceOfDigits() {
        isArrayEmpty();
        int[] arrayWithElements = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (hasAscSequenceOfDigits(this.array[i])) {
                arrayWithElements[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(arrayWithElements, newArrayIndex);
    }

    // w)
    public int[] capicuaInArray() {
        isArrayEmpty();
        int[] capicuaArray = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (Bloco3.isCapicua(this.array[i])) {
                capicuaArray[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(capicuaArray, newArrayIndex);
    }

    // x)
    private boolean hasAllSameDigits(int number) {
        int aux, aux_2;
        int iterator = number;

        aux = iterator % 10;
        iterator /= 10;

        while (iterator != 0) {
            aux_2 = iterator % 10;
            if (aux != aux_2)
                return false;
            aux = aux_2;
            iterator /= 10;
        }
        return true;
    }

    public int[] elementsWithAllSameDigits() {
        isArrayEmpty();
        int[] elements = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (hasAllSameDigits(this.array[i])) {
                elements[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(elements, newArrayIndex);
    }

    // y)
    public int[] elementsNotArmstrong() {
        isArrayEmpty();
        int[] elementsNotArmstrong = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (!Bloco3.isArmstrong(this.array[i])) {
                elementsNotArmstrong[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(elementsNotArmstrong, newArrayIndex);
    }

    // z)
    private boolean hasAscSequenceOfDigitsOfAtLeastNDigits(int number, int numberOfdigits) {
        int aux, aux_2, counterOfSequence = 1; // Initial digits is in sequence
        int iterator = number;

        aux = iterator % 10;
        iterator /= 10;

        while (iterator != 0 && counterOfSequence != numberOfdigits) {
            aux_2 = iterator % 10;
            if (aux > aux_2) {
                counterOfSequence++;
            } else
                counterOfSequence = 0;
            aux = aux_2;
            iterator /= 10;
        }
        return counterOfSequence >= numberOfdigits;
    }

    private void isNumberBiggerThanZero(int number) {
        if (isBiggerThanZero(number))
            throw new IllegalArgumentException("Number Must Be Bigger Than Zero");
    }

    private boolean isBiggerThanZero(int number) {
        return number <= 0;
    }

    public int[] elementsWithAscSequenceOfDigitsOfAtLeastNDigits(int numberOfDigits) {
        isArrayEmpty();
        isNumberBiggerThanZero(numberOfDigits);
        int[] elements = new int[this.array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.array.length; i++) {
            if (hasAscSequenceOfDigitsOfAtLeastNDigits(this.array[i], numberOfDigits)) {
                elements[newArrayIndex] = this.array[i];
                newArrayIndex++;
            }
        }
        return copyArray(elements, newArrayIndex);
    }

    // aa)
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Array)) return false;

        Array other = (Array) obj;

        if (other.array.length != this.array.length)
            return false;

        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] != other.array[i])
                return false;
        }
        return true;
    }
}
