package Bloco5;

public class ArrayOfArrays {
    // atributes

    private Array[] arrayOfArrays;

    // constructors
    // a)
    public ArrayOfArrays() {
        this.arrayOfArrays = new Array[0];
    }

    // b)
    public ArrayOfArrays(Array[] arrays) {
        isArrayEmpty(arrays);
        this.arrayOfArrays = new Array[arrays.length];

        this.arrayOfArrays = copyArray(arrays, arrays.length);
    }

    public ArrayOfArrays(int[][] arrays) {
        isArrayEmpty(arrays);
        this.arrayOfArrays = new Array[arrays.length];

        this.arrayOfArrays = copyArray(fromMatrixToArrayOfArrays(arrays));
    }

    // getters & setters

    private Array[] fromMatrixToArrayOfArrays(int[][] matrix) {
        Array[] arrays = new Array[matrix.length];

        for (int i = 0; i < matrix.length; i++) {
            arrays[i] = new Array(matrix[i]); //Não é necessario fazer valor a valor
            // Cada linha da matriz é um array, DAH!!!!!!
            /*for (int j = 0; j < matrix[i].length; j++) {
                arrays[i].addToArray(matrix[i][j]);
            }
             */
        }

        return arrays;
    }


    public Array[] toArrayOfArrays() {
        return copyArray(this.arrayOfArrays, this.arrayOfArrays.length);
    }

    public int[][] toMatrix() {
        int[][] matrix = new int[this.arrayOfArrays.length][];

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            matrix[i] = new int[this.arrayOfArrays[i].length()];
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                matrix[i][j] = this.arrayOfArrays[i].valueInPosition(j);
            }
        }
        return matrix;
    }

    public void setArrayOfArrays(Array[] arrayOfArrays) {
        this.arrayOfArrays = arrayOfArrays;
    }

    // methods
    private Array[] copyArray(Array[] arrays, int size) {
        isArrayEmpty(arrays);
        Array[] copyArray = new Array[size];
        for (int i = 0; i < size && i < arrays.length; i++) {
            copyArray[i] = arrays[i];
        }

        return copyArray;
    }

    private Array[] copyArray(Array[] arrays) {
        isArrayEmpty(arrays);
        Array[] copyArray = new Array[arrays.length];
        for (int i = 0; i < arrays.length; i++) {
            copyArray[i] = arrays[i];
        }

        return copyArray;
    }

    private boolean isEmpty() {
        return this.arrayOfArrays.length == 0;
    }

    private boolean isEmpty(Array[] arrays) {
        return arrays == null || arrays.length == 0;
    }

    private boolean isEmpty(int[][] arrays) {
        return arrays == null || arrays.length == 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ArrayOfArrays)) return false;

        ArrayOfArrays other = (ArrayOfArrays) obj;

        if (this.arrayOfArrays.length != other.arrayOfArrays.length) return false;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                if (this.arrayOfArrays[i].valueInPosition(j) != other.arrayOfArrays[i].valueInPosition(j))
                    return false;
            }
        }
        return true;
    }

    // e)
    private void isArrayEmpty() {
        if (isEmpty())
            throw new IllegalArgumentException("Empty Array");
    }

    private void isArrayEmpty(Array[] arrays) {
        if (isEmpty(arrays))
            throw new IllegalArgumentException("Empty Array");
    }

    private void isArrayEmpty(int[][] arrays) {
        if (isEmpty(arrays))
            throw new IllegalArgumentException("Empty Array");
    }

    // c)
    public void addElementInRow(int row, int element) {
        isArrayEmpty();
        this.arrayOfArrays[row].addToArray(element);
    }

    private boolean insideBounds(int row, int column) {
        return row < this.arrayOfArrays.length && column < this.arrayOfArrays[row].length();
    }

    private void isInsideBoundsPlusOne(int row, int column) {
        if (!insideBounds(row, column))
            throw new IllegalArgumentException("Row or Column out of Matrix Bound");
    }

    // d)
    public void removeFristElementOfGivenValue(int value) {
        isArrayEmpty();
        boolean hasChange = false;
        for (int i = 0; i < this.arrayOfArrays.length && !hasChange; i++) {
            int initialLength = this.arrayOfArrays[i].length();
            this.arrayOfArrays[i].removeFirstElementOf(value);
            if (this.arrayOfArrays[i].length() != initialLength)
                hasChange = true;
        }
    }

    // estava a ler o Exercicio 1...
    public int valueInPosition(int row, int column) {
        isInsideBoundsPlusOne(row, column);
        return this.arrayOfArrays[row].valueInPosition(column);
    }

    // f)
    public int largerNumberInArrays() {
        isArrayEmpty();

        int largerNumberInArrays = this.arrayOfArrays[0].valueInPosition(0);

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                if (this.arrayOfArrays[i].valueInPosition(j) > largerNumberInArrays)
                    largerNumberInArrays = this.arrayOfArrays[i].valueInPosition(j);

            }
        }
        return largerNumberInArrays;
    }

    // g)
    public int smallerNumberInArrays() {
        isArrayEmpty();
        int smallerNumberInArrays = this.arrayOfArrays[0].valueInPosition(0);

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                if (this.arrayOfArrays[i].valueInPosition(j) < smallerNumberInArrays)
                    smallerNumberInArrays = this.arrayOfArrays[i].valueInPosition(j);
            }
        }
        return smallerNumberInArrays;
    }

    // h)
    private int sumOfElementsInArrayOfArrays() {
        int sum = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                sum += this.arrayOfArrays[i].valueInPosition(j);
            }
        }
        return sum;
    }

    private int numberOfElemenstInArrayOfArrays() {
        int number = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            number += this.arrayOfArrays[i].length();
        }
        return number;
    }

    public double meanOfArrayOfArrays() {
        double meanOfArrayOfArrays = (double) sumOfElementsInArrayOfArrays() / numberOfElemenstInArrayOfArrays();

        return meanOfArrayOfArrays;
    }

    // i)
    public int[] sumOfEachRowArrayInArrayOfArrays() {
        Array sumOfEachArrayInArrayOfArrays = new Array();

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            sumOfEachArrayInArrayOfArrays.addToArray(this.arrayOfArrays[i].sumOfElementsInArray());
        }
        return sumOfEachArrayInArrayOfArrays.toArray();
    }

    // j)
    private int sumOfElementsInColumn(int column) {
        int sumOfElementsInColumn = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            sumOfElementsInColumn += this.arrayOfArrays[i].valueInPosition(column);
        }
        return sumOfElementsInColumn;
    }

    public int[] sumOfEachColumnInArrayOfArrays() {
        Array sumOfEachColumnInArrayOfArrays = new Array();

        for (int i = 0; i < this.arrayOfArrays[0].length(); i++) {
            sumOfEachColumnInArrayOfArrays.addToArray(sumOfElementsInColumn(i));
        }
        return sumOfEachColumnInArrayOfArrays.toArray();
    }

    // k)
    public int indexOfRowWithBiggerSumOfElements() {
        int indexOfRowWithBiggerSumOfElements = 0;
        int biggerSumOfElements = this.arrayOfArrays[0].sumOfElementsInArray();

        for (int i = 1; i < this.arrayOfArrays.length; i++) {
            if (this.arrayOfArrays[i].sumOfElementsInArray() > biggerSumOfElements) {
                biggerSumOfElements = this.arrayOfArrays[i].sumOfElementsInArray();
                indexOfRowWithBiggerSumOfElements = i;
            }
        }

        return indexOfRowWithBiggerSumOfElements;
    }

    // l)
    public boolean isSquare() {
        int[][] matrix = toMatrix();

        return Util.isSquare(matrix);
    }

    // m)
    public boolean isSimetric() { // Same as Transposed matrix
        int[][] matrix = toMatrix();
        int[][] transposed = Util.transposeMatrix(matrix);

        ArrayOfArrays matrixObject = new ArrayOfArrays(matrix);
        ArrayOfArrays transposedObject = new ArrayOfArrays(transposed);

        return matrixObject.equals(transposedObject);
    }

    // n)
    public int numberOfNotNullElementsInDiagonalOfArrayOfArrays() { // AKA ZERO!!
        if (!isSquare())
            return -1;

        int counterOfNotNullElements = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            if (this.arrayOfArrays[i].valueInPosition(i) != 0)
                counterOfNotNullElements++;
        }
        return counterOfNotNullElements;
    }

    // o)
    private Array arrayWithPrimaryDiagonal() {
        Array primaryDiagonal = new Array();

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            primaryDiagonal.addToArray(this.arrayOfArrays[i].valueInPosition(i));
        }
        return primaryDiagonal;
    }

    private Array arrayWithSecundaryDiagonal() {
        Array secundaryDiagonal = new Array();
        int j = arrayOfArrays.length - 1;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            secundaryDiagonal.addToArray(this.arrayOfArrays[i].valueInPosition(j));
            j--;
        }
        return secundaryDiagonal;
    }

    public boolean primaryDiagonalEqualsSecundaryDiagonal() {
        isSquare();
        return arrayWithPrimaryDiagonal().equals(arrayWithSecundaryDiagonal());
    }

    // p)
    private int countOfElementsInArrayOfArrays() {
        int counterOfElements = 0;
        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            counterOfElements += this.arrayOfArrays[i].length();
        }
        return counterOfElements;
    }

    private int meanOfDigitsInArrayOfArrays() {
        int sumOfDigits = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {

                sumOfDigits += Util.lengthOfString(Util.toString(this.arrayOfArrays[i].valueInPosition(j)));
            }
        }
        return sumOfDigits / countOfElementsInArrayOfArrays();
    }

    private boolean isBiggerThanTheMeanLength(int value) {
        return Util.lengthOfString(Util.toString(value)) > meanOfDigitsInArrayOfArrays();
    }

    public Array arrayWtihElementsWithNumberOfDigitsBiggerThanAverage() {
        Array arrayWithElements = new Array();

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                if (isBiggerThanTheMeanLength(this.arrayOfArrays[i].valueInPosition(j)))
                    arrayWithElements.addToArray(this.arrayOfArrays[i].valueInPosition(j));
            }
        }
        return arrayWithElements;
    }

    // q)
    private double percentageOfEvenDigits(int number) {
        int evenCounter = 0, digitCounter = 0, iterationWithMultiplesOfTen = 10;
        int iterator = number;
        while (iterator != 0) {
            if (Util.isEven(iterator)) {
                evenCounter++;
            }
            iterator /= 10;
            digitCounter++;
        }

        return (double) evenCounter / digitCounter;
    }

    private double percentageOfEvenDigits() {
        int evenCounter = 0, digitCounter = 0;

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                int iterator = this.arrayOfArrays[i].valueInPosition(j);
                while (iterator != 0) {
                    if (Util.isEven(iterator)) {
                        evenCounter++;
                    }
                    iterator /= 10;
                    digitCounter++;
                }
            }
        }
        return (double) evenCounter / digitCounter;
    }

    public Array arrayWithElementsWithPercentageOfEvenDigitsBiggerThanMean() {
        Array arrayWithElments = new Array();
        double percentageOfEvenDigits = percentageOfEvenDigits();

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                if (percentageOfEvenDigits(this.arrayOfArrays[i].valueInPosition(j)) > percentageOfEvenDigits)
                    arrayWithElments.addToArray(this.arrayOfArrays[i].valueInPosition(j));
            }
        }
        return arrayWithElments;
    }

    // r)
    public void invertionOfElementsInRow() {
        int[][] invertedRowMatrix = new int[this.arrayOfArrays.length][];

        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            int k = 0;
            invertedRowMatrix[i] = new int[this.arrayOfArrays[i].length()];
            for (int j = this.arrayOfArrays[i].length() - 1; j >= 0; j--) {
                invertedRowMatrix[i][k] = this.arrayOfArrays[i].valueInPosition(j);
                k++;
            }
        }
        this.arrayOfArrays = copyArray(fromMatrixToArrayOfArrays(invertedRowMatrix));
    }

    // s)
    public void invertionOfElementsInColumn() {
        isArrayEmpty();
        Array[] invertedColumnMatrix = new Array[this.arrayOfArrays.length];
        int k = this.arrayOfArrays.length - 1;
        for (int i = 0; i < this.arrayOfArrays.length; i++) {
            invertedColumnMatrix[i] = new Array();
            for (int j = 0; j < this.arrayOfArrays[i].length(); j++) {
                invertedColumnMatrix[i].addToArray(this.arrayOfArrays[k].valueInPosition(j));

            }
            k--;
        }
        this.arrayOfArrays = copyArray(invertedColumnMatrix);
    }

    // t)
    public void ninetyDegresRotation() {
        isArrayEmpty();
        isSquare();
        Array[] ninetyDegresRotation = new Array[this.arrayOfArrays[0].length()];
        int k = 0;
        for (int i = 0; i < this.arrayOfArrays[0].length(); i++) {
            ninetyDegresRotation[k] = new Array();
            for (int j = this.arrayOfArrays.length - 1; j >= 0; j--) {
                ninetyDegresRotation[k].addToArray(this.arrayOfArrays[j].valueInPosition(i));
            }
            k++;
        }
        this.arrayOfArrays = copyArray(ninetyDegresRotation);
    }

    // u)
    public void oneHundredAndEitghtyDegresRotation() {
        isArrayEmpty();
        isSquare();
        Array[] oneHundredAndEitghtyDegresRotation = new Array[this.arrayOfArrays.length];
        int k = 0;
        for (int i = this.arrayOfArrays.length - 1; i >= 0; i--) {
            oneHundredAndEitghtyDegresRotation[k] = new Array();
            for (int j = this.arrayOfArrays[i].length() - 1; j >= 0 ; j--) {
                oneHundredAndEitghtyDegresRotation[k].addToArray(this.arrayOfArrays[i].valueInPosition(j));
            }
            k++;
        }
        this.arrayOfArrays = copyArray(oneHundredAndEitghtyDegresRotation);
    }

    // v)
    public void ninetyDegresRotationNegative() {
        isArrayEmpty();
        isSquare();
        Array[] ninetyDegresRotation = new Array[this.arrayOfArrays[0].length()];
        int k = 0;
        for (int i = this.arrayOfArrays[0].length() - 1; i >= 0; i--) {
            ninetyDegresRotation[k] = new Array();
            for (int j = 0; j < this.arrayOfArrays.length; j++) {
                ninetyDegresRotation[k].addToArray(this.arrayOfArrays[j].valueInPosition(i));

            }
            k++;
        }
        this.arrayOfArrays = copyArray(ninetyDegresRotation);
    }
}
