package Bloco5;


public class BidimensionalArrayOne {

    // atributes
    private int[][] matrix;

    // constructors
    // a)
    public BidimensionalArrayOne() {
        this.matrix = new int[0][0];
    }

    // b)
    public BidimensionalArrayOne(int[][] matrix) {
        // INSERT Validaçao
        this.matrix = new int[matrix.length][matrix[0].length];

        this.matrix = copyMatrix(matrix, matrix.length);
    }

    // gets ands setters


    public int[][] getMatrix() {
        return copyMatrix(this.matrix, this.matrix.length);
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    // methods
    private int[][] copyMatrix (int[][] matrix, int rowSize) {
        int[][] copyMatrix = new int[rowSize][];
        for (int i = 0; i < copyMatrix.length; i++) {
            copyMatrix[i] = new int[matrix[i].length];
            for (int j = 0; j < copyMatrix[i].length && j < matrix[i].length; j++) {
                copyMatrix[i][j] = matrix[i][j];
            }
        }
        return copyMatrix;
    }

    //c)
    public void addElementToRow(int row, int element) {
        // Validação
        this.matrix = copyMatrix(this.matrix, this.matrix.length);

        this.matrix[row][this.matrix[row].length - 1] = element;
    }

    // d)
    /*public void removeFristElementOfGivenValue(int value) {

    }

     */

}
