package Bloco5.SopaDeLetras;

public class GameGride {

    private char[][] gameGride;

    public GameGride(char[][] gameGride) {
        isEmpty(gameGride);
        if (isSquared(gameGride)) {
            this.gameGride = gameGride;
        } else
            throw new IllegalArgumentException("Initial Game Grid Not Valid");
    }

    private void isEmpty(char[][] gameGride) {
        if (gameGride == null || gameGride.length == 0)
            throw new IllegalArgumentException("Game Grid are Empty or Null");
    }

    

    protected char[][] toMatrix (){
        return gameGride;
    }

    public int length() {
        return this.gameGride.length;
    }

    public int columnLength() {
        return this.gameGride[0].length;
    }

    public char charInPosition(int row, int column) {
        return this.gameGride[row][column];
    }

    private boolean isSquared(char[][] gameGride) {
        return gameGride.length == gameGride[0].length;
    }
}
