package Bloco5.SopaDeLetras;

public class HuntingWordsGame {

    private ValidWords validWords;
    private final GameGride gameGride;
    private int numberOfWordsLeft;

    public HuntingWordsGame(ValidWords validWords, GameGride gameGride) {
        this.validWords = validWords;
        this.gameGride = gameGride;
        setNumberOfWordsLeft();
    }

    private void setNumberOfWordsLeft() {
        this.numberOfWordsLeft = validWords.length();
    }

    public int numberOfWordsLeft() {
        return numberOfWordsLeft;
    }

    public char[][] getGameGride() {
        return gameGride.toMatrix();
    }

    public boolean sequenceIsValid(int initialRow, int initialColumn, int finalRow, int finalColumn) {
        if (Util.isSequenceAValidWord(initialRow, initialColumn, finalRow, finalColumn, this.gameGride, this.validWords)) {
                char[] sequenceWord = charSequenceFromUser(initialRow, initialColumn, finalRow, finalColumn);
                String word = String.valueOf(sequenceWord);
                validWords.removeWord(word);
                this.numberOfWordsLeft--;
                return true;
        }
        return false;
    }

    private char[] charSequenceFromUser(int initialRow, int initialColumn, int finalRow, int finalColumn) {
        String direction = Util.direcaoDadaAsCoordenadasIniciaisEFinais(initialRow, initialColumn, finalRow, finalColumn);
        char[] sequence = new char[this.gameGride.length()];
        switch (direction) {
            case "Direita":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoDireita(this.gameGride, initialRow, initialColumn, finalColumn);
                break;
            case "Esquerda":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoEsquerda(this.gameGride, initialRow, initialColumn, finalColumn);
                break;
            case "Baixo":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoBaixo(this.gameGride, initialRow, initialColumn, finalRow);
                break;
            case "CimaDireita":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoCimaDireita(this.gameGride, initialRow, initialColumn, finalRow, finalColumn);
                break;
            case "CimaEsquerda":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda(this.gameGride, initialRow, initialColumn, finalRow, finalColumn);
                break;
            case "BaixoEsquerda":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda(this.gameGride, initialRow, initialColumn, finalRow, finalColumn);
                break;
            case "BaixaDireita":
                sequence = Util.sequenciaNaMatrizParaComparacaoDirecaoBaixoDireita(this.gameGride, initialRow, initialColumn, finalRow, finalColumn);
        }
        return sequence;
    }
}
