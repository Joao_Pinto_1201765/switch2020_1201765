package Bloco5.SopaDeLetras;

import Bloco4.com.company.Exercicio12;
import Bloco4.com.company.Exercicio13;
import Bloco4.com.company.Exercicio6;

public class Util {

    public static int[][] matrizMascara(char[][] matrizLetras, char letra, int posicaoDaletraNaString) {
        //Assegurar que é Quadrada e não Nula
        if (isMatrixNull(matrizLetras) || !Exercicio13.EMatrizQuadradaChar(matrizLetras))
            return null;

        int[][] matrizMascara = new int[matrizLetras.length][matrizLetras.length];

        for (int i = 0; i < matrizLetras.length; i++) {
            for (int j = 0; j < matrizLetras[i].length; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, letra))
                    matrizMascara[i][j] = posicaoDaletraNaString;

            }
        }

        return matrizMascara;
    }

    /**
     * Verificar se char dado se encontra na posição dada na matriz
     *
     * @param matrizLetras Matriz da Sopa de Letras
     * @param i            linha
     * @param j            coluna
     * @param letra        char para verificar se está na posição
     * @return boolean true se está na posição
     */

    public static boolean letraCorrespondeAPosicao(char[][] matrizLetras, int i, int j, char letra) {
        boolean flag = false;

        if (matrizLetras[i][j] == letra)
            flag = true;

        return flag;
    }

    /**
     * Verificar se matriz é nula
     *
     * @param matriz matriz dada
     * @return boolean
     */
    public static boolean isMatrixNull(char[][] matriz) {
        boolean flag = false;

        if (matriz == null)
            flag = true;

        return flag;
    }

    public static boolean isMatrixNull(int[][] matriz) {
        boolean flag = false;

        if (matriz == null)
            flag = true;

        return flag;
    }

    /**
     * Somar duas Matrizes
     *
     * @param matrizInicial Matriz incial
     * @param matrizASomar  Matriz a Somar
     * @return Soma das duas Matrizes
     */

    public static int[][] somarMatriz(int[][] matrizInicial, int[][] matrizASomar) {
        if (naoQuadradaEDeDimensoesDiferentes(matrizInicial, matrizASomar))
            return null;

        int[][] somarMatriz = new int[matrizInicial.length][matrizInicial.length];

        for (int i = 0; i < matrizInicial.length; i++) {
            for (int j = 0; j < matrizInicial.length; j++) {
                somarMatriz[i][j] = matrizASomar[i][j] + matrizInicial[i][j];

            }

        }
        return somarMatriz;
    }

    /**
     * Verificar se as duas matrizes não são quadradas e têm as mesmas dimensoes
     *
     * @param matrizUm   Matriz Um
     * @param matrizDois Matriz Dois
     * @return boolean
     */

    public static boolean naoQuadradaEDeDimensoesDiferentes(int[][] matrizUm, int[][] matrizDois) {
        return !Exercicio13.isSquare(matrizUm) || !Exercicio13.isSquare(matrizDois) || Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matrizUm) != Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matrizDois);
    }

    // b)

    /**
     * Verificar se Palavra dada existe na matriz
     *
     * @param matrizLetras matriz de letras dada
     * @param palavra      palavra a verificar
     * @return boolean
     */

    public static Boolean palavraExisteNaMatriz(char[][] matrizLetras, String palavra) {
        boolean palavraExisteNaMatriz = false;

        if (isMatrixNull(matrizLetras) || isNullString(palavra))
            return null;

        if (palavraExisteNumaDirecao(matrizLetras, palavra)) {
            palavraExisteNaMatriz = true;
        }

        return palavraExisteNaMatriz;
    }

    /**
     * Verificar se palavra dada existe na Matriz de Letras.
     * <p>
     * Percorre as direções até encontar a palavra ou percorrer todas as direções e não encontrar.
     * A existirem duas palavras iguais só identifica a primeira que encontrar.
     *
     * @param matrizLetras Matriz de letras dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean palavraExisteNumaDirecao(char[][] matrizLetras, String palavra) {
        boolean existeNumaDirecao;

        if (direcaoDireita(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoEsquerda(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoCima(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoBaixo(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoCimaDireita(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoCimaEsquerda(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoBaixoEsquerda(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else if (direcaoBaixoDireita(matrizLetras, palavra)) {
            existeNumaDirecao = true;

        } else
            existeNumaDirecao = false;

        return existeNumaDirecao;
    }

    /**
     * Verificar se palavra se encontra na direção Direita na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoDireita(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoDireita = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoDireita; i++) { // Percorrer linhas
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoDireita; j++) { // Percorrer
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoDireita = existeDirecaoDireita(matrizLetras, existeDirecaoDireita, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoDireita;
    }

    public static boolean existeDirecaoDireita(char[][] matrizLetras, boolean existeDirecaoDireita, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && k + j < matrizLetras[i].length && !existeDirecaoDireita; k++) { //limite à direita
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i][j + k]) { // adiciona k à coluna[j] inicial, fazendo o varrimento para a Direita
                counter++; // Se char na matriz corresponder à posiçao no array de char da palavra, contador aumenta Um
                if (counter == arrayDeCharsDaPalavra.length) { // Se o contador de letras iguais à palavra for igual ao tamanho da array de chars da string palavra
                    existeDirecaoDireita = true; // existe na direção

                }
            }
        }
        return existeDirecaoDireita;
    }

    /**
     * Verificar se palavra se encontra na direção Esquerda na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoEsquerda(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoEsquerda = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoEsquerda; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoEsquerda; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoEsquerda = existeDirecaoEsquerda(matrizLetras, existeDirecaoEsquerda, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoEsquerda;
    }

    public static boolean existeDirecaoEsquerda(char[][] matrizLetras, boolean existeDirecaoEsquerda, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && j - k >= 0 && !existeDirecaoEsquerda; k++) { // limite esquerda
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i][j - k]) { // retira k à coluna[j] inicial, fazendo o varrimento para a Esquerda
                counter++; // Se char na matriz corresponder à posiçao no array de char da palavra, contador aumenta Um
                if (counter == arrayDeCharsDaPalavra.length) { // Se o contador de letras iguais à palavra for igual ao tamanho da array de chars da string palavra
                    existeDirecaoEsquerda = true; // existe na direção
                }
            }
        }
        return existeDirecaoEsquerda;
    }

    /**
     * Verificar se palavra se encontra na direção Cima na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoCima(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoCima = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoCima; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoCima; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoCima = existeDirecaoCima(matrizLetras, existeDirecaoCima, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoCima;
    }

    public static boolean existeDirecaoCima(char[][] matrizLetras, boolean existeDirecaoCima, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i - k >= 0 && !existeDirecaoCima; k++) { // limite superior
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i - k][j]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoCima = true;
                }
            }
        }
        return existeDirecaoCima;
    }

    /**
     * Verificar se palavra se encontra na direção Baixo na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoBaixo(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoBaixo = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoBaixo; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoBaixo; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoBaixo = existeDirecaoBaixo(matrizLetras, existeDirecaoBaixo, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoBaixo;
    }

    public static boolean existeDirecaoBaixo(char[][] matrizLetras, boolean existeDirecaoBaixo, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i + k < matrizLetras.length && !existeDirecaoBaixo; k++) {
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i + k][j]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoBaixo = true;
                }
            }
        }
        return existeDirecaoBaixo;
    }

    /**
     * Verificar se palavra se encontra na direção Cima/Direita na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoCimaDireita(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoCimaDireita = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoCimaDireita; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoCimaDireita; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoCimaDireita = existeDirecaoCimaDireita(matrizLetras, existeDirecaoCimaDireita, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoCimaDireita;
    }

    public static boolean existeDirecaoCimaDireita(char[][] matrizLetras, boolean existeDirecaoCimaDireita, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i - k >= 0 && j + k < matrizLetras[j].length && !existeDirecaoCimaDireita; k++) { // limite direita e superior
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i - k][j + k]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoCimaDireita = true;
                }
            }
        }
        return existeDirecaoCimaDireita;
    }

    /**
     * Verificar se palavra se encontra na direção Cima/Esquerda na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoCimaEsquerda(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoCimaEsquerda = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoCimaEsquerda; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoCimaEsquerda; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoCimaEsquerda = existeDirecaoCimaEsquerda(matrizLetras, existeDirecaoCimaEsquerda, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoCimaEsquerda;
    }

    public static boolean existeDirecaoCimaEsquerda(char[][] matrizLetras, boolean existeDirecaoCimaEsquerda, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i - k >= 0 && j - k >= 0 && !existeDirecaoCimaEsquerda; k++) { // limite esquerda e superior
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i - k][j - k]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoCimaEsquerda = true;
                }
            }
        }
        return existeDirecaoCimaEsquerda;
    }

    /**
     * Verificar se palavra se encontra na direção Baixo/Esquerda na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoBaixoEsquerda(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoBaixoEsquerda = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoBaixoEsquerda; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoBaixoEsquerda; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoBaixoEsquerda = existeDirecaoBaixoEsquerda(matrizLetras, existeDirecaoBaixoEsquerda, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoBaixoEsquerda;
    }

    public static boolean existeDirecaoBaixoEsquerda(char[][] matrizLetras, boolean existeDirecaoBaixoEsquerda, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i + k < matrizLetras.length && j - k >= 0 && !existeDirecaoBaixoEsquerda; k++) {
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i + k][j - k]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoBaixoEsquerda = true;
                }
            }
        }
        return existeDirecaoBaixoEsquerda;
    }

    /**
     * Verificar se palavra se encontra na direção Baixo/Direita na Matriz dada
     *
     * @param matrizLetras Matriz dada
     * @param palavra      Palavra a verificar
     * @return boolean
     */

    public static boolean direcaoBaixoDireita(char[][] matrizLetras, String palavra) {
        boolean existeDirecaoBaixoDireita = false;
        char[] arrayDeCharsDaPalavra = stringToCharArray(palavra);
        int indexZeroArrayChars = 0;

        for (int i = 0; i < matrizLetras.length && !existeDirecaoBaixoDireita; i++) {
            for (int j = 0; j < matrizLetras[i].length && !existeDirecaoBaixoDireita; j++) {
                if (letraCorrespondeAPosicao(matrizLetras, i, j, palavra.charAt(indexZeroArrayChars))) {
                    existeDirecaoBaixoDireita = existeDirecaoBaixoDireita(matrizLetras, existeDirecaoBaixoDireita, arrayDeCharsDaPalavra, i, j);
                }
            }
        }

        return existeDirecaoBaixoDireita;
    }

    public static boolean existeDirecaoBaixoDireita(char[][] matrizLetras, boolean existeDirecaoBaixoDireita, char[] arrayDeCharsDaPalavra, int i, int j) {
        int counter;
        counter = 0;

        for (int k = 0; k < arrayDeCharsDaPalavra.length && i + k < matrizLetras.length && j + k < matrizLetras[i].length && !existeDirecaoBaixoDireita; k++) {
            if (arrayDeCharsDaPalavra[k] == matrizLetras[i + k][j + k]) {
                counter++;
                if (counter == arrayDeCharsDaPalavra.length) {
                    existeDirecaoBaixoDireita = true;
                }
            }
        }
        return existeDirecaoBaixoDireita;
    }

    /**
     * Transformar String em Array de Chars
     *
     * @param palavra String
     * @return array de chars
     */
    public static char[] stringToCharArray(String palavra) {
        if (isNullString(palavra))
            return null;

        char[] array = new char[palavra.length()];

        for (int i = 0; i < array.length; i++) {
            // .charAt = caracter na posição (i)
            array[i] = palavra.charAt(i);

        }

        return array;
    }

    /**
     * Verificar se String não é Nula ou Vazia
     *
     * @param palavra String
     * @return boolean
     */
    public static boolean isNullString(String palavra) {
        boolean flag = false;

        if (palavra == null || palavra.equals(""))
            flag = true;

        return flag;
    }

    // c)

    /**
     * Verificar se Sequencia dada atraves de Coordenadas Contem Palavra Valida na matriz de letras
     *
     * @param i_inicial      Linha do começo da sequencia
     * @param j_inicial      Coluna do começo da sequencia
     * @param i_final        Linha final da sequencia
     * @param j_final        Coluna final da sequencia
     * @param //matrizLetras Matriz de Letras da Sopa de Letras
     * @param //palavras     Array de palavras válidas presentes na Matriz de Letras
     * @return boolean (Sequencia escolhida pelo utilizador contêm uma palavra Válida)
     */
    public static Boolean isSequenceAValidWord(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride, ValidWords validWords) {
        boolean existeNaDirecaoESequenciaDada = false;
        //char[] arrayDeCharsDaDireçao;

        if (posicaoIncialIgualAFinal(i_inicial, j_inicial, i_final, j_final) || coordenadasComValoresNegativos(i_inicial, j_inicial, i_final, j_final) || coordenadasForaDosLimites(i_inicial, j_inicial, i_final, j_final, gameGride))
            return null;

        if (eDirecaoDireita(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaDireita(i_inicial, j_inicial, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaEsquerda(i_inicial, j_inicial, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoCima(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaCima(i_inicial, j_inicial, i_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoBaixo(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaBaixo(i_inicial, j_inicial, i_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoCimaDireita(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaCimaDireita(i_inicial, j_inicial, i_final, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoCimaEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaCimaEsquerda(i_inicial, j_inicial, i_final, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else if (eDirecaoBaixoEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaBaixoEsquerda(i_inicial, j_inicial, i_final, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);


        } else {// baixo direita
            existeNaDirecaoESequenciaDada = existeNaDirecaoESequenciaDadaBaixoDireita(i_inicial, j_inicial, i_final, j_final, gameGride, validWords, existeNaDirecaoESequenciaDada);
        }


        return existeNaDirecaoESequenciaDada;
    }

    /**
     * Verificar se Existe palavra Valida na Direcao e Dentro dos Limites dados pelo Utilizador
     */

    private static boolean coordenadasForaDosLimites(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride) {
        return i_inicial >= gameGride.length() || i_final >= gameGride.length() || j_inicial >= gameGride.columnLength() || j_final >= gameGride.columnLength();
    }

    public static boolean existeNaDirecaoESequenciaDadaBaixoDireita(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoBaixoDireita(gameGride, i_inicial, j_inicial, i_final, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaBaixoEsquerda(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda(gameGride, i_inicial, j_inicial, i_final, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaCimaEsquerda(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda(gameGride, i_inicial, j_inicial, i_final, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaCimaDireita(int i_inicial, int j_inicial, int i_final, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoCimaDireita(gameGride, i_inicial, j_inicial, i_final, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaBaixo(int i_inicial, int j_inicial, int i_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoBaixo(gameGride, i_inicial, j_inicial, i_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaCima(int i_inicial, int j_inicial, int i_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoCima(gameGride, i_inicial, j_inicial, i_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaEsquerda(int i_inicial, int j_inicial, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoEsquerda(gameGride, i_inicial, j_inicial, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    public static boolean existeNaDirecaoESequenciaDadaDireita(int i_inicial, int j_inicial, int j_final, GameGride gameGride, ValidWords validWords, boolean existeNaDirecaoESequenciaDada) {
        char[] arrayDeCharsDaDirecao = sequenciaNaMatrizParaComparacaoDirecaoDireita(gameGride, i_inicial, j_inicial, j_final);
        if (verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(validWords, arrayDeCharsDaDirecao))
            existeNaDirecaoESequenciaDada = true;
        return existeNaDirecaoESequenciaDada;
    }

    /**
     * Retornar Sequencia (Array de Chars) de Letras atraves das coordenadas dadas
     */

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoDireita(GameGride gameGride, int i_inicial, int j_inicial, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i < gameGride.length() && i <= j_final; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial, i + j_inicial); // matrizLetras[i_inicial][i + j_inicial];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoEsquerda(GameGride gameGride, int i_inicial, int j_inicial, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; j_inicial - i >= 0 && j_inicial - i >= j_final; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial, j_inicial - i); //matrizLetras[i_inicial][j_inicial - i];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoCima(GameGride gameGride, int i_inicial, int j_inicial, int i_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i_inicial - i >= 0 && i_inicial - i >= i_final; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial - i, j_inicial);// matrizLetras[i_inicial - i][j_inicial];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoBaixo(GameGride gameGride, int i_inicial, int j_inicial, int i_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i_inicial + i < gameGride.length() && i_inicial + i <= i_final; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial + i, j_inicial);//matrizLetras[i_inicial + i][j_inicial];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoCimaDireita(GameGride gameGride, int i_inicial, int j_inicial, int i_final, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; j_inicial + i < gameGride.length() && i_inicial - i >= 0 && i_final <= i_inicial - i && j_final >= j_inicial + i; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial - i, j_inicial + i); //matrizLetras[i_inicial - i][j_inicial + i];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda(GameGride gameGride, int i_inicial, int j_inicial, int i_final, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i_inicial - i >= i_final && j_inicial - i >= j_final && i_inicial - i >= 0 && j_inicial - i >= 0; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial - i, j_inicial - i); //matrizLetras[i_inicial - i][j_inicial - i];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda(GameGride gameGride, int i_inicial, int j_inicial, int i_final, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i_inicial + i <= i_final && j_inicial - i >= j_final && i_inicial + i < gameGride.length() && j_inicial - i >= 0; i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial + i, j_inicial - i); //matrizLetras[i_inicial + i][j_inicial - i];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    public static char[] sequenciaNaMatrizParaComparacaoDirecaoBaixoDireita(GameGride gameGride, int i_inicial, int j_inicial, int i_final, int j_final) {
        char[] sequenciaNaMatrizParaComparacao = new char[gameGride.length()];
        int counter = 0;

        for (int i = 0; i_inicial + i <= i_final && j_inicial + i <= j_final && i_inicial + i < gameGride.length() && j_inicial + i < gameGride.columnLength(); i++) {
            sequenciaNaMatrizParaComparacao[i] = gameGride.charInPosition(i_inicial + i, j_inicial + i); //matrizLetras[i_inicial + i][j_inicial + i];
            counter++;

        }

        sequenciaNaMatrizParaComparacao = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(sequenciaNaMatrizParaComparacao, counter);

        return sequenciaNaMatrizParaComparacao;
    }

    /**
     * Verificar se Sequencia dada pelo Utilizador forma uma Palavra presente num array de Strings (Palavras Válidas)
     *
     * @param //palavras                      String Array de palavras validas
     * @param sequenciaNaMatrizParaComparacao Char Array de caracteres da sequencia dada pelo utilizador
     * @return boolean
     */
    public static boolean verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(ValidWords validWords, char[] sequenciaNaMatrizParaComparacao) {
        boolean possivelFormarPalavraComSequenciaDadaPelasCoordenadas = false;
        String sequenciaNaMatrizParaComparacaoString = new String(sequenciaNaMatrizParaComparacao);

        for (int i = 0; i < validWords.length(); i++) {
            if (validWords.position(i).equals(sequenciaNaMatrizParaComparacaoString)) {
                possivelFormarPalavraComSequenciaDadaPelasCoordenadas = true;
                i = validWords.length();
            }
        }

        return possivelFormarPalavraComSequenciaDadaPelasCoordenadas;
    }

    /**
     * Verificar Direcao
     */

    private static boolean coordenadasComValoresNegativos(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_inicial < 0 || j_inicial < 0 || i_final < 0 || j_final < 0;
    }

    public static boolean eDirecaoBaixoEsquerda(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_final > i_inicial && j_final < j_inicial;
    }

    public static boolean eDirecaoCimaEsquerda(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_final < i_inicial && j_final < j_inicial;
    }

    public static boolean eDirecaoCimaDireita(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_final < i_inicial && j_final > j_inicial;
    }

    public static boolean eDirecaoBaixo(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_final > i_inicial && j_inicial == j_final;
    }

    public static boolean eDirecaoCima(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_final < i_inicial && j_inicial == j_final;
    }

    public static boolean eDirecaoEsquerda(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_inicial == i_final && j_inicial > j_final;
    }

    public static boolean eDirecaoDireita(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_inicial == i_final && j_inicial < j_final;
    }

    public static boolean posicaoIncialIgualAFinal(int i_inicial, int j_inicial, int i_final, int j_final) {
        return i_inicial == i_final && j_inicial == j_final;
    }

    public static String direcaoDadaAsCoordenadasIniciaisEFinais(int i_inicial, int j_inicial, int i_final, int j_final) {
        String direcao;

        if (posicaoIncialIgualAFinal(i_inicial, j_inicial, i_final, j_final) || coordenadasComValoresNegativos(i_inicial, j_inicial, i_final, j_final))
            return null;

        if (eDirecaoDireita(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "Direita";

        } else if (eDirecaoEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "Esquerda";

        } else if (eDirecaoCima(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "Cima";

        } else if (eDirecaoBaixo(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "Baixo";

        } else if (eDirecaoCimaDireita(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "CimaDireita";

        } else if (eDirecaoCimaEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "CimaEsquerda";

        } else if (eDirecaoBaixoEsquerda(i_inicial, j_inicial, i_final, j_final)) {
            direcao = "BaixoEsquerda";

        } else
            direcao = "BaixoDireita";

        return direcao;

    }

    public static boolean isSquare(int[][] matriz) {
        boolean flag = false;
        int counterDeLinhasComNColunasIguaisANumerodeLinhas = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz.length){
                counterDeLinhasComNColunasIguaisANumerodeLinhas++;
            }
        }
        if (counterDeLinhasComNColunasIguaisANumerodeLinhas == matriz.length && Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz) > 0){
            flag = true;
        }

        return flag;
    }
}
