package Bloco5.SopaDeLetras;

public class ValidWords {

    private String[] validWords;

    public ValidWords(String[] validWords) {
        isEmpty(validWords);
        this.validWords = copyStringArray(validWords);
    }

    private boolean isEmpty(String[] validWords) {
        if (validWords == null || validWords.length == 0)
            throw new IllegalArgumentException("Valid Words are Empty or Null");
        return true;
    }

    private String[] copyStringArray(String[] stringArray) {
        String[] copyArray = new String[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            copyArray[i] = stringArray[i];
        }
        return copyArray;
    }

    public String[] toArray() {
        String[] toArray = new String[this.validWords.length];

        for (int i = 0; i < this.validWords.length; i++) {
            toArray[i] = this.validWords[i];
        }

        return toArray;
    }

    protected boolean existInStringArray(String word){
        for (int i = 0; i < this.validWords.length; i++) {
            if (word.equals(this.validWords[i]))
                return true;
        }
        return false;
    }

    protected void removeWord(String word) {
        if(existInStringArray(word));

        String[] newArray = new String[this.validWords.length - 1];
        int j = 0;

        for (int i = 0; i < this.validWords.length; i++) {
            if (this.validWords[i].equals(word))
                continue;
            newArray[j] = this.validWords[i];
            j++;
        }
        this.validWords = copyStringArray(newArray);
    }

    public int length () {
        return validWords.length;
    }

    protected String position(int position) {
        return validWords[position];
    }
}
