package Bloco5.Sudoku;

public class SudokuGame {

    private final int SUM_MATRIX_VALID_SUDOKU = 405;
    private int[][] actualGameGrid;
    private int[][] initialGameGrid;
    private int[][] initialMask;
    private boolean finishedWithSucess = false;

    public SudokuGame(int[][] initialGameGrid) {
        isAValidSudokuMatrix(initialGameGrid);
        this.actualGameGrid = copyMatrix(initialGameGrid);
        this.initialGameGrid = copyMatrix(initialGameGrid);
        setInitialMask(initialGameGrid);
    }

    private void isAValidSudokuMatrix(int[][] matrix) {
        if (!isValidMatrix(matrix))
            throw new IllegalArgumentException("Matrix not Valid, Must Be 9X9");
    }

    private boolean isValidMatrix(int[][] matrix) {
        return matrix.length == 9 && Util.areAllRowOfEqualLength(matrix);
    }

    private void setInitialMask(int[][] initialMask) {
        this.initialMask = copyMatrix(Util.toMask(initialMask));
    }

    private int[][] copyMatrix(int[][] matrix) {
        int[][] copyMatrix = new int[matrix.length][matrix.length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                copyMatrix[i][j] = matrix[i][j];
            }
        }
        return copyMatrix;
    }

    public boolean isFinishedWithSucess() {
        return finishedWithSucess;
    }

    public int[][] getActualGameGrid() {
        return copyMatrix(actualGameGrid);
    }

    private boolean isGameGridFull() {
        boolean isGameGridFull = true;

        for (int i = 0; i < this.actualGameGrid.length && isGameGridFull; i++) {
            for (int j = 0; j < this.actualGameGrid[i].length && isGameGridFull; j++) {
                if (Util.isZero(this.actualGameGrid[i][j])) {
                    isGameGridFull = false;
                }
            }
        }
        return isGameGridFull;
    }

    public void doMove(int row, int column, int numberToPlay) {

        isValidNumberInSudokuGame(numberToPlay);
        isInlimits(row, column);
        isNotInMask(row, column);

        if (isAValidMove(row, column, numberToPlay) && !isGameGridFull()) {
            row--;
            column--;
            this.actualGameGrid[row][column] = numberToPlay;
        }
    }

    public void resetGame(){
        this.actualGameGrid = copyMatrix(this.initialGameGrid);
    }

    private void isNotInMask(int row, int column) {
        if (isInInitialGameGrid(row, column))
            throw new IllegalArgumentException("Position is fill in Initial Game Grid");
    }

    private boolean isInInitialGameGrid(int row, int column) {
        return this.initialMask[row-1][column-1] == 1;
    }

    private void isInlimits(int row, int column) {
        if (!isInBounds(row, column) || isRowOrColumnZeroOrLess(row, column))
            throw new IndexOutOfBoundsException("Invalid Play, Index Out Of Bounds");
    }

    private boolean isRowOrColumnZeroOrLess(int row, int column) {
        return Util.zeroOrLessThanZero(row) || Util.zeroOrLessThanZero(column);
    }

    private boolean isInBounds(int row, int column) {
        return row <= this.initialGameGrid.length && column <= this.initialGameGrid.length;
    }

    private void isValidNumberInSudokuGame(int number) {
        if (!isValidNumberInSudoku(number))
            throw new IllegalArgumentException("Number to insert isn't valid");
    }

    private boolean isValidNumberInSudoku(int number) {
        return number > 0 && number <= 9;
    }

    private boolean isAValidMove(int row, int column, int numberToPlay) {
        return isMoveValidColumn(row, numberToPlay) && isMoveValidRow(column, numberToPlay) && isAValidPlayForBlock(row, column, numberToPlay);
    }

    private boolean isAValidPlayForBlock(int row, int column, int numberToPlay) {
        boolean isAValidPlay = true;
        int block = moveIsInBlock(row, column);

        switch (block) {
            case 1:
                isAValidPlay = isAValidMoveBlockOne(numberToPlay);
                break;
            case 2:
                isAValidPlay = isAValidMoveBlockTwo(numberToPlay);
                break;
            case 3:
                isAValidPlay = isAValidMoveBlockThree(numberToPlay);
                break;
            case 4:
                isAValidPlay = isAValidMoveBlockFour(numberToPlay);
                break;
            case 5:
                isAValidPlay = isAValidMoveBlockFive(numberToPlay);
                break;
            case 6:
                isAValidPlay = isAValidMoveBlckSix(numberToPlay);
                break;
            case 7:
                isAValidPlay = isAValidMoveBlckSeven(numberToPlay);
                break;
            case 8:
                isAValidPlay = isAValidMoveBlockEigth(numberToPlay);
                break;
            case 9:
                isAValidPlay = isAValidMoveBlockNine(numberToPlay);
                break;
        }


        return isAValidPlay;
    }

    private boolean isAValidMoveBlockNine(int intToPlay) {
        for (int i = 6; i < 9; i++) {
            for (int j = 6; j < 9; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockEigth(int intToPlay) {
        for (int i = 6; i < 9; i++) {
            for (int j = 3; j < 6; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return false;
    }

    private boolean isAValidMoveBlckSeven(int intToPlay) {
        for (int i = 6; i < 9; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlckSix(int intToPlay) {
        for (int i = 3; i < 6; i++) {
            for (int j = 6; j < 9; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockFive(int intToPlay) {
        for (int i = 3; i < 6; i++) {
            for (int j = 3; j < 6; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockFour(int intToPlay) {
        for (int i = 3; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockThree(int intToPlay) {
        for (int i = 0; i < 3; i++) {
            for (int j = 6; j < 9; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockTwo(int intToPlay) {
        for (int i = 0; i < 3; i++) {
            for (int j = 3; j < 6; j++) {
                if (this.actualGameGrid[i][j] == intToPlay)
                    return false;
            }
        }
        return true;
    }

    private boolean isAValidMoveBlockOne(int intToPkay) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.actualGameGrid[i][j] == intToPkay)
                    return false;
            }
        }
        return true;
    }

    private boolean isMoveValidColumn(int row, int intToPlay) {
        for (int i = 0; i < this.actualGameGrid[row - 1].length; i++) {
            if (this.actualGameGrid[row - 1][i] == intToPlay)
                return false;

        }
        return true;
    }

    private boolean isMoveValidRow(int column, int intToPlay) {

        for (int i = 0; i < this.actualGameGrid.length; i++) {
            if (this.actualGameGrid[i][column - 1] == intToPlay)
                return false;

        }
        return true;
    }

    private int moveIsInBlock(int row, int column) {
        int block;

        if (blockOne(row, column)) {
            block = 1;

        } else if (blockTwo(row, column)) {
            block = 2;

        } else if (blockThree(row, column)) {
            block = 3;

        } else if (blockFour(row, column)) {
            block = 4;

        } else if (blockFive(row, column)) {
            block = 5;

        } else if (blockSix(row, column)) {
            block = 6;

        } else if (blockSeven(row, column)) {
            block = 7;

        } else if (blockEigth(row, column)) {
            block = 8;

        } else if (blockNine(row, column)) {
            block = 9;

        } else
            block = -1; // retorna -1 quando coordenadas excedem os limites da matriz de jogo

        return block;
    }

    private boolean blockNine(int linha, int coluna) {
        return linha < 10 && coluna < 10;
    }

    private boolean blockEigth(int linha, int coluna) {
        return linha < 10 && coluna < 7;
    }

    private boolean blockSeven(int linha, int coluna) {
        return linha < 10 && coluna < 4;
    }

    private boolean blockSix(int linha, int coluna) {
        return linha < 7 && coluna < 10;
    }

    private boolean blockFive(int linha, int coluna) {
        return linha < 7 && coluna < 7;
    }

    private boolean blockFour(int linha, int coluna) {
        return linha < 7 && coluna < 4;
    }

    private boolean blockThree(int linha, int coluna) {
        return linha < 4 && coluna < 10;
    }

    private boolean blockTwo(int linha, int coluna) {
        return linha < 4 && coluna < 7;
    }

    private boolean blockOne(int linha, int coluna) {
        return linha < 4 && coluna < 4;
    }

    public boolean isSudokuFinishedAndValid(){
        if(isGameGridFull() && isGameGridValid()) {
            this.finishedWithSucess = true;
            return true;
        }
        return false;
    }

    private boolean isGameGridValid(){
        int sum = 0;

        for (int i = 0; i < this.actualGameGrid.length; i++) {
            for (int j = 0; j < this.actualGameGrid[0].length; j++) {
                sum += this.actualGameGrid[i][j];
            }
        }
        return sum == this.SUM_MATRIX_VALID_SUDOKU;
    }





}
