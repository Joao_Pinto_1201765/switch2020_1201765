package Bloco5.Sudoku;

import java.util.Random;

/**
 * Reference: https://www.101computing.net/sudoku-generator-algorithm/
 * In Python
 * https://stackoverflow.com/questions/43529967/java-generating-an-unique-sudoku
 */

public class SudokuGenerator {

    public int[][] sudokuGenerationGrid = new int[9][9];

    private int[] numberSelection = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    public SudokuGenerator() {
        solveGrid();

    }

    public int[][] getSudokuGenerationGrid() {
        return copyMatrix(sudokuGenerationGrid);
    }

    private int[][] copyMatrix(int[][] matrix) {
        int[][] copyMatrix = new int[matrix.length][matrix.length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                copyMatrix[i][j] = matrix[i][j];
            }
        }
        return copyMatrix;
    }

    private boolean isFull() {
        for (int i = 0; i < this.sudokuGenerationGrid.length; i++) {
            for (int j = 0; j < this.sudokuGenerationGrid.length; j++) {
                if (this.sudokuGenerationGrid[i][j] == 0)
                    return false;
            }
        }
        return true;
    }

    private void solveGrid() {
        int row, col;

        for (int i = 0; i < 81; i++) {
            row = i / 9;
            col = i % 9;

            if (this.sudokuGenerationGrid[row][col] == 0) {

                for (int j = 1; j < 10; j++) {
                    Random random = new Random();
                    int k = random.nextInt(9) +1;

                    if (Util.verificarValidadeDaJogada(this.sudokuGenerationGrid, row, col, k)) {
                        this.sudokuGenerationGrid[row][col] = k;
                        if (isFull()) {
                            break;
                        } else
                            solveGrid();
                    }
                }
            }
        }
    }
}
