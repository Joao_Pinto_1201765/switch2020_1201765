package Bloco5.Sudoku;

import Bloco4.com.company.Exercicio13;
import Bloco4.com.company.Exercicio18;

public class Util {

    public static boolean areAllRowOfEqualLength(int[][] matriz) {
        int counterLinhasComColunasIguais = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz[0].length)
                counterLinhasComColunasIguais++;
        }
        if (counterLinhasComColunasIguais == matriz.length) {
            return true;
        }
        return false;
    }

    public static int[][] toMask(int[][] matrizBase) {
        if (Exercicio18.isMatrixNull(matrizBase) || !Exercicio13.isSquare(matrizBase) || !eMatrizNovePorNove(matrizBase))
            return null;

        int identificadorDePresencaDeNumeroNaMatrizBase = 1;
        int[][] matrizMascaraInicial = new int[matrizBase.length][matrizBase.length]; // MatrizQuadrada

        for (int i = 0; i < matrizBase.length; i++) {
            for (int j = 0; j < matrizBase[i].length; j++) {
                if (isNotZero(matrizBase[i][j])) // Se !ZERO é numero presente na matrizBase
                    matrizMascaraInicial[i][j] = identificadorDePresencaDeNumeroNaMatrizBase;

            }
        }

        return matrizMascaraInicial;
    }

    public static boolean eMatrizNovePorNove(int[][] matrizBase) {
        return matrizBase.length == 9 && matrizBase[0].length == 9;
    }

    public static boolean isZero(int i) {
        return i == 0;
    }

    public static boolean isNotZero(int i1) {
        return i1 != 0;
    }

    public static boolean zeroOrLessThanZero(int i) {
        return i <= 0;
    }

    public static boolean verificarValidadeDaJogada(int[][] matrizJogo, int linha, int coluna, int numeroAJogar) {
        boolean jogadaValida = false;


        if (eJogadaValidaLinha(matrizJogo, coluna, numeroAJogar) && eJogadaValidaColuna(matrizJogo, linha, numeroAJogar) && eJogadaValidaBloco(matrizJogo, linha, coluna, numeroAJogar))
            jogadaValida = true;


        return jogadaValida;
    }

    /**
     * Verificar Se Jogada é Validada por Bloco
     * Divisão da matriz 9X9 por blocos de 3X3, iniciando a contagem no bloco Superior Esquerdo = 1, até bloco Inferior Direito = 9
     *
     * @param matrizJogo   Matriz de Jogo
     * @param linha        Linha a Jogar
     * @param coluna       Coluna a Jogar
     * @param numeroAJogar Numero a Colocar
     * @return boolean
     */

    public static boolean eJogadaValidaBloco(int[][] matrizJogo, int linha, int coluna, int numeroAJogar) {
        boolean jogadaValida = true;
        int bloco = linhaEColunaPertenceAoBloco(linha, coluna);

        switch (bloco) {
            case 1:
                jogadaValida = eJogadaValidaBlocoUm(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 2:
                jogadaValida = eJogadaValidaBlocoDois(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 3:
                jogadaValida = eJogadaValidaBlocoTres(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 4:
                jogadaValida = eJogadaValidaBlocoQuatro(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 5:
                jogadaValida = eJogadaValidaBlocoCinco(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 6:
                jogadaValida = eJogadaValidaBlocoSeis(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 7:
                jogadaValida = eJogadaValidaBlocoSete(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 8:
                jogadaValida = eJogadaValidaBlocoOito(matrizJogo, numeroAJogar, jogadaValida);
                break;
            case 9:
                jogadaValida = eJogadaValidaBlocoNove(matrizJogo, numeroAJogar, jogadaValida);
                break;
        }


        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoNove(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoOito(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoSete(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 6; i < 9; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoSeis(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoCinco(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoQuatro(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 3; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoTres(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 6; j < 9; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoDois(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 3; j < 6; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaBlocoUm(int[][] matrizJogo, int numeroAJogar, boolean jogadaValida) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrizJogo[i][j] == numeroAJogar)
                    jogadaValida = false;
            }
        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaColuna(int[][] matrizJogo, int linha, int numeroAJogar) {
        boolean jogadaValida = true;
        for (int i = 0; i < matrizJogo[linha].length; i++) {
            if (matrizJogo[linha][i] == numeroAJogar)
                jogadaValida = false;

        }
        return jogadaValida;
    }

    public static boolean eJogadaValidaLinha(int[][] matrizJogo, int coluna, int numeroAJogar) {
        boolean jogadaValida = true;
        for (int i = 0; i < matrizJogo.length; i++) {
            if (matrizJogo[i][coluna] == numeroAJogar)
                jogadaValida = false;

        }
        return jogadaValida;
    }

    /**
     * Classificar atraves da linha e coluna o bloco pertencente
     *
     * @param linha  Linha a Jogar
     * @param coluna Coluna a Jogar
     * @return int = numero do bloco
     */

    public static int linhaEColunaPertenceAoBloco(int linha, int coluna) {
        int bloco;

        if (blocoUm(linha, coluna)) {
            bloco = 1;

        } else if (blocoDois(linha, coluna)) {
            bloco = 2;

        } else if (blocoTres(linha, coluna)) {
            bloco = 3;

        } else if (blocoQuatro(linha, coluna)) {
            bloco = 4;

        } else if (blocoCinco(linha, coluna)) {
            bloco = 5;

        } else if (blocoSeis(linha, coluna)) {
            bloco = 6;

        } else if (blocoSete(linha, coluna)) {
            bloco = 7;

        } else if (blocoOito(linha, coluna)) {
            bloco = 8;

        } else if (blocoNove(linha, coluna)) {
            bloco = 9;

        } else
            bloco = -1; // retorna -1 quando coordenadas excedem os limites da matriz de jogo

        return bloco;
    }

    public static boolean blocoNove(int linha, int coluna) {
        return linha < 9 && coluna < 9;
    }

    public static boolean blocoOito(int linha, int coluna) {
        return linha < 9 && coluna < 6;
    }

    public static boolean blocoSete(int linha, int coluna) { return linha < 9 && coluna < 3; }

    public static boolean blocoSeis(int linha, int coluna) {
        return linha < 6 && coluna < 9;
    }

    public static boolean blocoCinco(int linha, int coluna) {
        return linha < 6 && coluna < 6;
    }

    public static boolean blocoQuatro(int linha, int coluna) {
        return linha < 6 && coluna < 3;
    }

    public static boolean blocoTres(int linha, int coluna) {
        return linha < 3 && coluna < 9;
    }

    public static boolean blocoDois(int linha, int coluna) {
        return linha < 3 && coluna < 6;
    }

    public static boolean blocoUm(int linha, int coluna) {
        return linha < 3 && coluna < 3;
    }

}
