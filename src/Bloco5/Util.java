package Bloco5;

import Bloco4.com.company.Exercicio12;

public class Util {

    public static int[][] transposeMatrix(int[][] matriz) {
        int[][] matrizTransposta = new int[matriz[0].length][matriz.length]; //inversao das linhas com colunas

        for (int i = 0; i < matrizTransposta[0].length; i++) {
            for (int j = 0; j < matrizTransposta.length; j++) {
                matrizTransposta[j][i] = matriz[i][j];

            }

        }

        return matrizTransposta;
    }

    public static boolean isSquare(int[][] matriz) {
        boolean flag = false;
        int counterDeLinhasComNColunasIguaisANumerodeLinhas = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length == matriz.length){
                counterDeLinhasComNColunasIguaisANumerodeLinhas++;
            }
        }
        if (counterDeLinhasComNColunasIguaisANumerodeLinhas == matriz.length && Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz) > 0){
            flag = true;
        }

        return flag;
    }

    public static String toString(int value) {
        return String.valueOf(value);
    }

    public static int lengthOfString(String string) {
        return string.length();
    }

    public static boolean isEven(int i) {
        return i % 2 == 0 && i != 0;
    }

}
