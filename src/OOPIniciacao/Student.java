package OOPIniciacao;

public class Student {
    // atributos

    private int number;
    private String name;
    private int grade = -1; // default, quem não tem avaliacao tem -1

    // metodos construtores

    public Student(int number, String name) {
        setNumber(number);
        setName(name);

    }

    private Student(int number, String name, int grade) {
        this.number = number;
        this.name = name;
        this.grade = grade;
    }

    // private Student() { // para nao permitir a criacao de Student sem parametros que são obrigatorios, neste caso, nome e numero, afinal não é necessário
    // }

    // metodos getter e setters

    public int getNumber() {
        return number;
    }

    private void setNumber(int number) {
        if (!studentNumberInvalid(number)) // numero aluno com 5 digitos
            throw new IllegalArgumentException("Invalid Number");
        this.number = number;
    }


    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (!isNameValid(name)) // nome de aluno com 5 ou mais caracteres
            throw new IllegalArgumentException("Invalid Name");
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        if (!isValidGrade(grade))
            throw new IllegalArgumentException("Nota Invalida");
        this.grade = grade;
    }

    // metodos negocio && Validacao
    private boolean isNameValid(String name) {
        return name.length() >= 5;
    }

    private boolean studentNumberInvalid(int number) {
        return number > 10000 && number < 99999;
    }

    private boolean isValidGrade(int grade) {
        return grade >= 0 && grade <= 20;
    }

    private boolean isEvaluated() {
        return this.grade != -1;
    }

    public int compareToByNumber(Student other) {
        return Integer.compare(this.number, other.number);
    }

    public int compareToByGrade(Student other) {
        return Integer.compare(this.grade, other.grade);
    }

    /* Tinha os dois (setGrade) para o setGrade ser private public void doEvaluation(int grade) {
        setGrade(grade);
    }*/

    // Herdou metodos da classe Objetct
    @Override
    // Override ao metodo public boolean equals da classe Objeto, nome igual, funcionalidade adaptada à presente classe
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return this.number == student.number;
    }
}
