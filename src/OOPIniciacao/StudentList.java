package OOPIniciacao;

import java.security.InvalidParameterException;
import java.util.ArrayList;

public class StudentList {
    // atributos
    private ArrayList<Student> students;

    // metodos construtores

    public StudentList(Student[] students) {
        // adicionar Exepção
        this.students = new ArrayList<>();
        for (Student st : students) {
            this.addStudent(st);
        }
    }

    public StudentList() {
        this.students = new ArrayList<>();
    }

    // metodos getters e setters


    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        if (!studentArrayValid(students))
            throw new InvalidParameterException("StudentList is null or empty");

        this.students = students;
    }

    // metodos de negocio && Validacao

    private boolean studentArrayValid(ArrayList<Student> students) {
        return students != null || students.size() > 0;
    }

    public void sortByNumberAsc() {
        Student temporaryStudent = null;

        for (int i = 0; i < this.students.size(); i++) { // para cada numero percorre o resto da StudentList
            for (int j = i + 1; j < this.students.size(); j++) {

                if (this.students.get(i).compareToByNumber(this.students.get(j)) > 0) { // Ascendente (Integer.compare > 0)
                    temporaryStudent = this.students.get(i);
                    this.students.set(i, this.students.get(j));
                    this.students.set(j, temporaryStudent);
                }

            }

        }
    }

    public void sortByGradeDesc(ArrayList<Student> students) {
        Student temporaryStudent = null;

        for (int i = 0; i < this.students.size(); i++) {
            for (int j = i + 1; j < this.students.size(); j++) {

                if (this.students.get(i).compareToByGrade(this.students.get(j)) < 0) { // Descendente (Integer.compare < 0)
                    temporaryStudent = this.students.get(i);
                    this.students.set(i, this.students.get(j));
                    this.students.set(j, temporaryStudent);
                }
            }
        }
    }

    private ArrayList<Student> copyStudentsFromArray(ArrayList<Student> students, int size) { // "enviar sempre uma copia, para não mexer nas referencias"
        // utilização do size para truncar futuramente, quando não necessario usar como parametro students.length
        ArrayList<Student> copyArray = new ArrayList<Student>();

        for (int i = 0; i < size && i < students.size(); i++) {
            copyArray.set(i, students.get(i));

        }
        return copyArray;
    }

    public Student[] toArray() {
        Student[] array = new Student[this.students.size()];
        return this.students.toArray(array);
    }

    private boolean exist(Student student) {
        for (int i = 0; i < this.students.size(); i++) {
            if (student.equals(this.students.get(i))) ;
            return true;

        }
        return false;
    }

    public boolean addStudent(Student student) { // boolean addicionado ou não adicionado
        if (student == null)
            return false;
        if (this.students.contains(student))
            return false;

        this.students.add(student);
        return true;
    }

    public boolean removeStudent(Student student) {
        if (student == null)
            return false;
        if (!this.students.contains(student))
            return false;
        return this.students.remove(student);
    }


}
