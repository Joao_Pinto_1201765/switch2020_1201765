package Bloco1.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Bloco1Test {

    //Testes Exercício 1
    @Test
    public void percentagemDeRapazes_50() {
        int rapazes = 5;
        int raparigas = 15;
        double expected = 25.0;

        double result = Bloco1.percentagemDeRapazes(rapazes, raparigas);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void percentagemDeRaparigas() {
        int rapazes = 10;
        int raparigas = 10;
        double expected = 50.0;

        double result = Bloco1.percentagemDeRaparigas(rapazes, raparigas);

        assertEquals(expected, result, 0.0001);
    }

    //Testes Exercício 2
    @Test
    public void precoTotal_1() {
        int rosas = 1;
        int tulipas = 1;
        double precoRosa = 1;
        double precoTulipa = 1;
        double expected = 2;

        double result = Bloco1.precoTotal(rosas, tulipas, precoRosa, precoTulipa);

        assertEquals(expected, result, 0.0001);
    }

    //Testes Exercício 3

    @Test
    public void volumeCilindroEmLitros_1() {
        double raio = 1;
        double altura = 1;
        double expected = 3141.592654;

        double result = Bloco1.volumeCilindro(raio, altura);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void volumeCilindroEmLitros_2() {
        double raio = 2;
        double altura = 2;
        double expected = 25132.74123;

        double result = Bloco1.volumeCilindro(raio, altura);

        assertEquals(expected, result, 0.0001);
    }

    //Testes Exercício 4

    @Test
    public void kmDeDistancia_1() {
        double tempoSeg = 10;
        double expected = 3.4;

        double result = Bloco1.kmDeDistancia(tempoSeg);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void kmDeDistancia_2() {
        double tempoSeg = 14.7;
        double expected = 5;

        double result = Bloco1.kmDeDistancia(tempoSeg);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercício 5

    @Test
    public void alturaEdificio_1() {
        int tempoSeg = 2;
        double expected = 19.6;

        double result = Bloco1.alturaEdificio(tempoSeg);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void alturaEdificio_2() {
        int tempoSeg = 3;
        double expected = 44.1;

        double result = Bloco1.alturaEdificio(tempoSeg);

        assertEquals(expected, result, 0.001);
    }

    //Testes exercício 6

    @Test
    public void alturaEdificioSombra_1() {
        double sombraEdificio = 40;
        double alturaPessoa = 2;
        double sombraPessoa = 4;
        double expected = 20;

        double result = Bloco1.alturaEdificioSombra(sombraEdificio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void alturaEdificioSombra_2() {
        double sombraEdificio = 80;
        double alturaPessoa = 2;
        double sombraPessoa = 8;
        double expected = 20;

        double result = Bloco1.alturaEdificioSombra(sombraEdificio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

    //Testes Exercício 7

    @Test
    public void corrida_1() {
        double distanciaCompleta = 42195;
        double tempoTotal = 4.03611;
        double tempoParcial = 1.09333;
        double expected = 11430.07;

        double result = Bloco1.corrida(distanciaCompleta, tempoTotal, tempoParcial);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void corrida_2() {
        double distanciaCompleta = 10000;
        double tempoTotal = 0.9;
        double tempoParcial = 0.5;
        double expected = 5555.5;

        double result = Bloco1.corrida(distanciaCompleta, tempoTotal, tempoParcial);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercício 8

    @Test
    public void leiCossenos_1() {
        double compAC = 7;
        double compBC = 13;
        double angulo = 140;
        double expected = 18.9;

        double result = Bloco1.leiCossenosCompDeUmLado(compAC, compBC, angulo);

        assertEquals(expected, result, 0.01);
    }
    @Test
    public void leiCossenos_2() {
        double compAC = 10;
        double compBC = 5;
        double angulo = 30;
        double expected = 6.16;

        double result = Bloco1.leiCossenosCompDeUmLado(compAC, compBC, angulo);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void leiCossenos_3() {
        double compAC = 5;
        double compBC = 16;
        double angulo = 61;
        double expected = 14.3; //17.95 ??!!

        double result = Bloco1.leiCossenosCompDeUmLado(compAC, compBC, angulo);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercício 9

    @Test
    public void perimetro_1() {
        double a = 1;
        double b = 1;
        double expected = 4;

        double result = Bloco1.perimetroRectangulo(a, b);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void perimetro_2() {
        double a = 10;
        double b = 20;
        double expected = 60;

        double result = Bloco1.perimetroRectangulo(a, b);

        assertEquals(expected, result, 0.1);
    }


    //Testes Exercício 10

    @Test
    public void hipotenusa_1() {
        double cateto1 = 1;
        double cateto2 = 1;
        double expected = 1.42;

        double result = Bloco1.hipotenusaTrianguloRectangulo(cateto1,cateto2);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void hipotenusa_2() {
        double cateto1 = 458;
        double cateto2 = 589;
        double expected = 746.11;

        double result = Bloco1.hipotenusaTrianguloRectangulo(cateto1,cateto2);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercício 11

    @Test
    public void formulaRes1_1() {
        double b = 4;
        double c = -21;
        double expexted = 3;

        double result = Bloco1.formulaResolvente1(b,c);

        assertEquals(expexted, result, 0.1);
    }
    @Test
    public void formulaRes1_2() {
        double b = 4;
        double c = -21;
        double expexted = -7;

        double result = Bloco1.formulaResolvente2(b,c);

        assertEquals(expexted, result, 0.1);
    }
    @Test
    public void formulaRes2_1() {
        double b = 2;
        double c = -15;
        double expexted = 3;

        double result = Bloco1.formulaResolvente1(b, c);

        assertEquals(expexted, result, 0.1);
    }
    @Test
    public void formulaRes2_2_1() {
        double b = 2;
        double c = -15;
        double expexted = -5;

        double result = Bloco1.formulaResolvente2(b, c);

        assertEquals(expexted, result, 0.1);
    }
    @Test
    public void formulaRes3_1() {
        double b = -14;
        double c = 20;
        double expexted = 12.3852;

        double result = Bloco1.formulaResolvente1(b, c);

        assertEquals(expexted, result, 0.1);
    }
    @Test
    public void formulaRes3_2() {
        double b = -14;
        double c = 20;
        double expexted = 1.6114;

        double result = Bloco1.formulaResolvente2(b, c);

        assertEquals(expexted, result, 0.1);
    }

    //Testes Exercício 12

    @Test
    public void celsiusToFah_1() {
        double celsius =30 ;
        double expected = 86;

        double result = Bloco1.celsiusToFahrenheit(celsius);

        assertEquals(expected,result, 0.1);
    }
    @Test
    public void celsiusToFah_2() {
        double celsius =-12 ;
        double expected = 10.4;

        double result = Bloco1.celsiusToFahrenheit(celsius);

        assertEquals(expected,result, 0.1);
    }
    //Testes Exercício 13

    @Test
    public void horasMinutosToMinutos_1() {
        double horas = 2;
        double minutos = 30;
        double expected = 150;

        double result = Bloco1.horasMinutosToMinutos(horas, minutos);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void horasMinutosToMinutos_2() {
        double horas = 0;
        double minutos = 30;
        double expected = 30;

        double result = Bloco1.horasMinutosToMinutos(horas, minutos);

        assertEquals(expected, result, 0.1);
    }
    @Test
    public void horasMinutosToMinutos_3() {
        double horas = 1;
        double minutos = 0;
        double expected = 60;

        double result = Bloco1.horasMinutosToMinutos(horas, minutos);

        assertEquals(expected, result, 0.1);
    }

}