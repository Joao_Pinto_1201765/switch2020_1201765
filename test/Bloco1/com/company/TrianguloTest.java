package Bloco1.com.company;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class TrianguloTest {

    @Test
    public void testeHipotenusa() {
        Triangulo triangulo = new Triangulo(1,1);
        double expected = 1.41;

        assertEquals(expected, triangulo.hipotenusa(), 0.01);


    }

    @Test (expected = InvalidParameterException.class)
    public void testeHipotenusaErro() {
        Triangulo triangulo = new Triangulo(-1,1);


        //assertThrows(InvalidParameterException, triangulo.hipotenusa(), 0.01);


    }
}