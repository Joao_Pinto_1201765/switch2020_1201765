package Bloco2.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Bloco2Test {

    //Testes Exercício 1
    @Test
    public void mediaPonderada_1() {
        //arrange
        double nota1 = 10;
        double nota2 = 10;
        double nota3 = 10;
        double peso1 = 10;
        double peso2 = 10;
        double peso3 = 10;
        double expected = 10;
        //act
        double result = Bloco2.mediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);
        //assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void mediaPonderada_2() {
        //arrange
        double nota1 = 7;
        double nota2 = 13;
        double nota3 = 0;
        double peso1 = 10;
        double peso2 = 10;
        double peso3 = 10;
        double expected = 6.67;
        //act
        double result = Bloco2.mediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);
        //assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void mediaPonderada_3() {
        //arrange
        double nota1 = 20;
        double nota2 = 16;
        double nota3 = 13;
        double peso1 = 6;
        double peso2 = 4;
        double peso3 = 10;
        double expected = 15.7;

        double result = Bloco2.mediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercício 2

    @Test
    public void tresDigitos1_1() {
        //arrange
        int numero = 100;
        double expected = 1;

        double result = Bloco2.tresDigitos1(numero);

        assertEquals(expected, result, 1);
    }

    @Test
    public void tresDigitos2_1() {
        //arrange
        int numero = 100;
        double expected = 0;
        //act
        double result = Bloco2.tresDigitos2(numero);
        //assert
        assertEquals(expected, result, 1);
    }

    @Test
    public void tresDigitos3_1() {
        //arrange
        int numero = 100;
        double expected = 0;

        double result = Bloco2.tresDigitos3(numero);

        assertEquals(expected, result, 1);
    }

    @Test
    public void parOuImpoar_1() {
        //arrange
        int numero = 100;
        String expected = "Número é par";

        String result = Bloco2.parOuImpar(numero);

        assertEquals(expected, result);
    }

    @Test
    public void parOuImpoar_2() {
        //arrange
        int numero = 101;
        String expected = "Número é impar";

        String result = Bloco2.parOuImpar(numero);

        assertEquals(expected, result);
    }

    //Testes Exercicio 3

    @Test
    public void distancia_1() {
        double x1 = 2;
        double x2 = 7;
        double y1 = 2;
        double y2 = 5;
        double expected = 5.83;

        double result = Bloco2.distanciaDoisPontos(x1, x2, y1, y2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void distancia_2() {
        double x1 = 2;
        double x2 = 8;
        double y1 = 2;
        double y2 = 2;
        double expected = 6;

        double result = Bloco2.distanciaDoisPontos(x1, x2, y1, y2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void distancia_3() {
        double x1 = -1;
        double x2 = -1;
        double y1 = -1;
        double y2 = 5;
        double expected = 6;

        double result = Bloco2.distanciaDoisPontos(x1, x2, y1, y2);

        assertEquals(expected, result, 0.01);
    }

    //Testes Exercicio 4

    @Test
    public void funcaoDeX_1() {
        int x = 4;
        double expected = 0;

        double result = Bloco2.funcaoDeX(x);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void funcaoDeX_2() {
        int x = 0;
        double expected = 0;

        double result = Bloco2.funcaoDeX(x);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void funcaoDeX_3() {
        int x = -1;
        double expected = -1;

        double result = Bloco2.funcaoDeX(x);

        assertEquals(expected, result, 0.1);
    }

    //Testes Exercicio 5

    @Test
    public void volumeCubo_1() {
        double area = 1000;
        double expected = 2.1517;

        double result = Bloco2.volumeCubo(area);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void volumeCubo_2() {
        double area = 10000;
        double expected = 68.041;

        double result = Bloco2.volumeCubo(area);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void classCubo2_1() {
        double volumeDm3 = 1;
        String expected = "Pequeno";

        String result = Bloco2.classificacaoVolumeCubo(volumeDm3);

        assertEquals(expected, result);
    }

    @Test
    public void classCubo2_2() {
        double volumeDm3 = 2;
        String expected = "Médio";

        String result = Bloco2.classificacaoVolumeCubo(volumeDm3);

        assertEquals(expected, result);
    }

    @Test
    public void classCubo2_3() {
        double volumeDm3 = 3;
        String expected = "Grande";

        String result = Bloco2.classificacaoVolumeCubo(volumeDm3);

        assertEquals(expected, result);
    }


    //Testes Exercicio 6

    @Test
    public void segundoToHMS_1() {
        int segundos = 1;
        String expected = "00:00:01";

        String result = Bloco2.segundosToHMS(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void segundoToHMS_2() {
        int segundos = 21663;
        String expected = "06:01:03";

        String result = Bloco2.segundosToHMS(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void segundoToHMS_3() {
        int segundos = 21600;
        String expected = "06:00:00";

        String result = Bloco2.segundosToHMS(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void segundoToHMS_4() {
        int segundos = 43261;
        String expected = "12:01:01";

        String result = Bloco2.segundosToHMS(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void saudacao_1() {
        int segundos = 1;
        String expected = "Boa noite";

        String result = Bloco2.saudacao(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void saudacao_2() {
        int segundos = 21600;
        String expected = "Bom dia";

        String result = Bloco2.saudacao(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void saudacao_3() {
        int segundos = 72001;
        String expected = "Boa tarde";

        String result = Bloco2.saudacao(segundos);

        assertEquals(expected, result);
    }

    // Testes exercicio 7

    @Test
    public void custoMaoObra_1() {
        double area = 120;
        double salarioHora = 1;
        double expected = 30;

        double result = Bloco2.custoMaoObra(area, salarioHora);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void custoMaoObra_2() {
        double area = 2;
        double salarioHora = 1;
        double expected = 1;

        double result = Bloco2.custoMaoObra(area, salarioHora);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void custoTinta_1() {
        double area = 2;
        double custoLitro = 1;
        double redimento = 1;
        double expected = 2;

        double result = Bloco2.custoDaTinta(area, custoLitro, redimento);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoTinta_2() {
        double area = 3;
        double custoLitro = 10;
        double redimento = 2;
        double expected = 15;

        double result = Bloco2.custoDaTinta(area, custoLitro, redimento);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoTotal_1() {
        double salarioMaoObra = 3;
        double custoDaTinta = 10;
        double expected = 13;

        double result = Bloco2.custoTotal(salarioMaoObra, custoDaTinta);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoTotal_2() {
        double salarioMaoObra = 14;
        double custoDaTinta = 16;
        double expected = 30;

        double result = Bloco2.custoTotal(salarioMaoObra, custoDaTinta);

        assertEquals(expected, result, 0.01);
    }

    // Testes Exercicio 8

    @Test
    public void multiploDivisor_1() {
        double x = 40;
        double y = 5;
        String expected = "X é multiplo de Y";

        String result = Bloco2.multiploDivisor(x, y);

        assertEquals(expected, result);
    }

    @Test
    public void multiploDivisor_2() {
        double x = 5;
        double y = 40;
        String expected = "Y é multiplo de X";

        String result = Bloco2.multiploDivisor(x, y);

        assertEquals(expected, result);
    }

    @Test
    public void multiploDivisor_3() {
        double x = 7;
        double y = 10;
        String expected = "X não é multiplo nem divisor de Y";

        String result = Bloco2.multiploDivisor(x, y);

        assertEquals(expected, result);
    }

    //Teste Exercicio 9

    @Test
    public void sequenciaCrescente_1() {
        int numero = 123;
        String expected = "Sequência crescente";

        String result = Bloco2.sequenciaCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void sequenciaCrescente_2() {
        int numero = 321;
        String expected = "Sequência não crescente";

        String result = Bloco2.sequenciaCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void sequenciaCrescente_3() {
        int numero = 100;
        String expected = "Sequência não crescente";

        String result = Bloco2.sequenciaCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void sequenciaCrescente_4() {
        int numero = 103;
        String expected = "Sequência não crescente";

        String result = Bloco2.sequenciaCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void sequenciaCrescente_5() {
        int numero = 112;
        String expected = "Sequência não crescente";

        String result = Bloco2.sequenciaCrescente(numero);

        assertEquals(expected, result);
    }

    // Testes Exercicio 10

    @Test
    public void artigoSemSaldoMaiorQueDuzentos() {
        double valorSemSaldo = 201;
        double expected = 80.4;

        double result = Bloco2.artigoComSaldo(valorSemSaldo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void artigoSemSaldoMenorOuIgualDuzentosMaiorQueCem() {
        double valorSemSaldo = 200;
        double expected = 120;

        double result = Bloco2.artigoComSaldo(valorSemSaldo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void artigoSemSaldoMenorOuIgualCemMaiorQueCinquenta() {
        double valorSemSaldo = 100;
        double expected = 70;

        double result = Bloco2.artigoComSaldo(valorSemSaldo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void artigoSemSaldoMenorOuIgualQueCinquenta() {
        double valorSemSaldo = 50;
        double expected = 40;

        double result = Bloco2.artigoComSaldo(valorSemSaldo);

        assertEquals(expected, result, 0.01);
    }

    // Testes Exercicio 11

    @Test
    public void percentagemAprovados1() {
        double numAprovados = 5;
        double numTotalTurma = 10;
        double expected = 0.5;

        double result = Bloco2.percentagemAprovados(numAprovados, numTotalTurma);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void percentagemAprovados2() {
        double numAprovados = 10;
        double numTotalTurma = 5;
        double expected = 2;

        double result = Bloco2.percentagemAprovados(numAprovados, numTotalTurma);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void classValorInvalido() {
        double percentagemAprovados = 2;
        double percentagemTurmaMa = 0.1;
        double percentagemTurmaFraca = 0.2;
        double percentagemTurmaRazoavel = 0.3;
        double percentagemTurmaBoa = 0.4;
        String expected = "Valor inválido";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    public void classTurmaMaMenorZeroPontoCinco() {
        double percentagemAprovados = 0.5;
        double percentagemTurmaMa = 0.5;
        double percentagemTurmaFraca = 0.6;
        double percentagemTurmaRazoavel = 0.7;
        double percentagemTurmaBoa = 0.8;
        String expected = "Turma Má";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    public void classTurmaExcelenteMaiorZeroPontoQuatroUm() {
        double percentagemAprovados = 0.41;
        double percentagemTurmaMa = 0.1;
        double percentagemTurmaFraca = 0.2;
        double percentagemTurmaRazoavel = 0.3;
        double percentagemTurmaBoa = 0.4;
        String expected = "Turma Excelente";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    public void classTurmaFraca() {
        double percentagemAprovados = 0.45;
        double percentagemTurmaMa = 0.2;
        double percentagemTurmaFraca = 0.5;
        double percentagemTurmaRazoavel = 0.7;
        double percentagemTurmaBoa = 0.9;
        String expected = "Turma Fraca";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    public void classTurmaRazoavel() {
        double percentagemAprovados = 0.65;
        double percentagemTurmaMa = 0.2;
        double percentagemTurmaFraca = 0.5;
        double percentagemTurmaRazoavel = 0.7;
        double percentagemTurmaBoa = 0.9;
        String expected = "Turma Razoável";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    @Test
    public void classTurmaBoal() {
        double percentagemAprovados = 0.89;
        double percentagemTurmaMa = 0.2;
        double percentagemTurmaFraca = 0.5;
        double percentagemTurmaRazoavel = 0.7;
        double percentagemTurmaBoa = 0.9;
        String expected = "Turma Boa";

        String result = Bloco2.classificacaoAprovadoNaoAprovado(percentagemAprovados, percentagemTurmaMa, percentagemTurmaFraca, percentagemTurmaRazoavel, percentagemTurmaBoa);

        assertEquals(expected, result);
    }

    // Testes Exercio 12


    @Test
    public void poluicaoGrupoIndustrialUmIndicePoluicaoZeroPontoTrintaUm() {
        int grupoIndustrial = 1;
        double indicePoluicao = 0.31;
        String expected = "Suspender atividade";

        String result = Bloco2.poluicaoGrupos(grupoIndustrial, indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void poluicaoGrupoIndustrialUmIndicePoluicaoZeroPontoVinteNove() {
        int grupoIndustrial = 1;
        double indicePoluicao = 0.29;
        String expected = "Continuar atividade";

        String result = Bloco2.poluicaoGrupos(grupoIndustrial, indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void poluicaoGrupoIndustrialInexistente() {
        int grupoIndustrial = 4;
        double indicePoluicao = 0.29;
        String expected = "Grupo Industrial não existente";

        String result = Bloco2.poluicaoGrupos(grupoIndustrial, indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void poluicaoGrupoIndustrialDoisIndicePoluicaoZeroPontoQuarentaUm() {
        int grupoIndustrial = 2;
        double indicePoluicao = 0.41;
        String expected = "Suspender atividade";

        String result = Bloco2.poluicaoGrupos(grupoIndustrial, indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void poluicaoGrupoIndustrialTresIndicePoluicaoZeroPontoCinquentaUm() {
        int grupoIndustrial = 3;
        double indicePoluicao = 0.51;
        String expected = "Suspender atividade";

        String result = Bloco2.poluicaoGrupos(grupoIndustrial, indicePoluicao);

        assertEquals(expected, result);
    }

    // Testes Exercicio 13

    @Test
    public void custoJardinagem1() {
        double ervaMQuadrado = 1;
        double arvores = 1;
        double arbustos = 1;
        double expected = 45;

        double result = Bloco2.custoJardinagem(ervaMQuadrado, arvores, arbustos);

        assertEquals(expected, result, 0.1);

    }

    @Test
    public void custoJardinagem2() {
        double ervaMQuadrado = 2;
        double arvores = 3;
        double arbustos = 4;
        double expected = 140;

        double result = Bloco2.custoJardinagem(ervaMQuadrado, arvores, arbustos);

        assertEquals(expected, result, 0.1);

    }

    @Test
    public void tempoJardinagemHoras1() {
        double ervaMQuadrado = 1;
        double arvores = 1;
        double arbustos = 1;
        double expected = 0.36111;

        double result = Bloco2.tempoJardinagemHoras(ervaMQuadrado, arvores, arbustos);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void tempoJardinagemHoras2() {
        double ervaMQuadrado = 2;
        double arvores = 2;
        double arbustos = 2;
        double expected = 0.72222;

        double result = Bloco2.tempoJardinagemHoras(ervaMQuadrado, arvores, arbustos);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void custoTotalMaisHoras1() {
        double custoJardinagem = 1;
        double tempoJardinagem = 1;
        double expected = 11;

        double result = Bloco2.custoTotalMaisHoras(custoJardinagem, tempoJardinagem);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void custoTotalMaisHoras2() {
        double custoJardinagem = 2;
        double tempoJardinagem = 2;
        double expected = 22;

        double result = Bloco2.custoTotalMaisHoras(custoJardinagem, tempoJardinagem);

        assertEquals(expected, result, 0.1);
    }

    // Exercicio 14

    @Test
    public void mediaCincoDias1() {
        double distanciaUm = 1;
        double distanciaDois = 1;
        double distanciaTres = 1;
        double distanciaQuatro = 1;
        double distanciaCinco = 1;
        double expected = 1.609;

        double result = Bloco2.mediaCincoDiasDistanciaEstafeta(distanciaUm, distanciaDois, distanciaTres, distanciaQuatro, distanciaCinco);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void mediaCincoDias2() {
        double distanciaUm = 5;
        double distanciaDois = 15;
        double distanciaTres = 11;
        double distanciaQuatro = 9;
        double distanciaCinco = 10;
        double expected = 16.09;

        double result = Bloco2.mediaCincoDiasDistanciaEstafeta(distanciaUm, distanciaDois, distanciaTres, distanciaQuatro, distanciaCinco);

        assertEquals(expected, result, 0.1);
    }

    // Testes Exercicio 15

    @Test
    public void trianguloComAMaiorQueBMaisC() {
        double a = 15;
        double b = 5;
        double c = 5;
        String expected = "Triangulo impossivel";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void trianguloComLadoNegativo() {
        double a = -1;
        double b = 5;
        double c = 5;
        String expected = "Triangulo impossivel";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void trianguloEquilatero() {
        double a = 5;
        double b = 5;
        double c = 5;
        String expected = "Equilátero";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void trianguloEscaleno() {
        double a = 5;
        double b = 6;
        double c = 7;
        String expected = "Escaleno";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void trianguloIsoceles1() {
        double a = 5;
        double b = 5;
        double c = 6;
        String expected = "Isóceles";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void trianguloIsoceles2() {
        double a = 10;
        double b = 10;
        double c = 9;
        String expected = "Isóceles";

        String result = Bloco2.classificacaoTrianguloLados(a, b, c);

        assertEquals(expected, result);
    }

    // Testes Exercicio 16

    @Test
    public void somaAngulosDiferenteCentoOitenta() {
        double a = 90;
        double b = 45;
        double c = 46;
        String expected = "Triangulo Impossivel";

        String result = Bloco2.classificacaoTrianguloAngulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void anguloNegativo() {
        double a = -90;
        double b = 45;
        double c = 45;
        String expected = "Triangulo Impossivel";

        String result = Bloco2.classificacaoTrianguloAngulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void retangulo() {
        double a = 90;
        double b = 45;
        double c = 45;
        String expected = "Retângulo";

        String result = Bloco2.classificacaoTrianguloAngulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void obtusangulo() {
        double a = 91;
        double b = 44;
        double c = 45;
        String expected = "Obtusangulo";

        String result = Bloco2.classificacaoTrianguloAngulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void acutangulo() {
        double a = 89;
        double b = 45;
        double c = 46;
        String expected = "Acutangulo";

        String result = Bloco2.classificacaoTrianguloAngulos(a, b, c);

        assertEquals(expected, result);
    }

    // Testes Exercicio 17

    @Test
    public void horasComboioHoje() {
        int horasPartida = 0;
        int minPartida = 0;
        int horasViagem = 1;
        int minViagem = 0;
        String expected = "1:00hoje";

        String result = Bloco2.horaComboio(horasPartida, minPartida, horasViagem, minViagem);

        assertEquals(expected, result);
    }

    @Test
    public void horasComboioAmanha() {
        int horasPartida = 22;
        int minPartida = 30;
        int horasViagem = 4;
        int minViagem = 0;
        String expected = "2:30amanhã";

        String result = Bloco2.horaComboio(horasPartida, minPartida, horasViagem, minViagem);

        assertEquals(expected, result);
    }

    //Testes Exercicio 18

    @Test
    public void tempoProcessamento1() {
        int horasInicioProcessamento = 0;
        int minInicioProcessamento = 0;
        int segInicioProcessamento = 0;
        int tempoProcessamento = 61;
        String expeted = "00:01:01";

        String result = Bloco2.tempoProcessamento(horasInicioProcessamento, minInicioProcessamento, segInicioProcessamento, tempoProcessamento);

        assertEquals(expeted, result);
    }

    @Test
    public void tempoProcessamento2() {
        int horasInicioProcessamento = 2;
        int minInicioProcessamento = 59;
        int segInicioProcessamento = 59;
        int tempoProcessamento = 1;
        String expeted = "03:00:00";

        String result = Bloco2.tempoProcessamento(horasInicioProcessamento, minInicioProcessamento, segInicioProcessamento, tempoProcessamento);

        assertEquals(expeted, result);
    }

    // Testes Exercicio 19

    @Test
    public void salarioHorasMenorOuIgualTrinca() {
        int horasTrabalho = 36;
        double expected = 270;

        double result = Bloco2.salario(horasTrabalho);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void salarioHorasMenorOuIgualQuarentaUm() {
        int horasTrabalho = 41;
        double expected = 320;

        double result = Bloco2.salario(horasTrabalho);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void salarioHorasSuperiorAQuarentaUm() {
        int horasTrabalho = 42;
        double expected = 335;

        double result = Bloco2.salario(horasTrabalho);

        assertEquals(expected, result, 0.01);
    }

    //Testes Exercicio 20


    @Test
    public void tipoADiaUtilDoisKm() {
        String tipoDia = "Util";
        String tipoKit = "A";
        double kmDistancia = 2;
        double expected = 34;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void tipoADiaNaoUtilTresKm() {
        String tipoDia = "Não Util";
        String tipoKit = "A";
        double kmDistancia = 3;
        double expected = 46;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void tipoBDiaUtilDoisKm() {
        String tipoDia = "Util";
        String tipoKit = "B";
        double kmDistancia = 2;
        double expected = 54;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void tipoBDiaNaoUtilTreKm() {
        String tipoDia = "Não Util";
        String tipoKit = "B";
        double kmDistancia = 3;
        double expected = 76;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void tipoCDiaUtilDoisKm() {
        String tipoDia = "Util";
        String tipoKit = "C";
        double kmDistancia = 2;
        double expected = 104;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void tipoCDiaNaoUtilTreKm() {
        String tipoDia = "Não Util";
        String tipoKit = "C";
        double kmDistancia = 3;
        double expected = 146;

        double result = Bloco2.precoJardinagemDomicilio(tipoDia, tipoKit, kmDistancia);

        assertEquals(expected, result, 0.01);
    }

}