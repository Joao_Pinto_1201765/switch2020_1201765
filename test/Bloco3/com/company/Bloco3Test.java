package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Bloco3Test {

    @Test
    public void testeExercicioUmZero() {
        int num = 1;
        String expected = "O resultado é:" + 0;

        String result = Bloco3.exercicioUm(num);

        assertEquals(expected, result);
    }

    @Test
    public void testeExercicioUmUm() {
        int num = 2;
        String expected = "O resultado é:" + 1;

        String result = Bloco3.exercicioUm(num);

        assertEquals(expected, result);
    }

    @Test
    public void testeExercicioUmUmV2() {
        int num = 3;
        String expected = "O resultado é:" + 1;

        String result = Bloco3.exercicioUm(num);

        assertEquals(expected, result);
    }

    @Test
    public void testeExercicioUmZeroNegativo() {
        int num = -1;
        String expected = "O resultado é:" + 0;

        String result = Bloco3.exercicioUm(num);

        assertEquals(expected, result);
    }

    @Test
    public void testeExercicioUmUmNegativo() {
        int num = -5;
        String expected = "O resultado é:" + 0;

        String result = Bloco3.exercicioUm(num);

        assertEquals(expected, result);
    }

    // Testes Exercicio 2

    @Test
    public void testeLerNotasUm() {
        int nAlunos = 3;
        int[] notas = {10, 10, 10};
        double expeted = 10.0;

        double result = Bloco3.lerNotasMediaTotal(nAlunos, notas);

        assertEquals(expeted, result, 0.1);
    }

    @Test
    public void testeLerNotasDois() {
        int nAlunos = 5;
        int[] notas = {10, 12, 14, 16, 18};
        double expeted = 14.0;

        double result = Bloco3.lerNotasMediaTotal(nAlunos, notas);

        assertEquals(expeted, result, 0.1);
    }

    @Test
    public void testeNotasMediaNegativasPercentagemNegativasUm() {
        int nAlunos = 5;
        int[] notas = {10, 12, 14, 16, 18};
        String expeted = "0.0 1.0";

        String result = Bloco3.lerNotasMediaNegativasPercentagemNegativas(nAlunos, notas);

        assertEquals(expeted, result);
    }

    @Test
    public void testeNotasMediaNegativasPercentagemNegativasDois() {
        int nAlunos = 10;
        int[] notas = {10, 12, 14, 16, 18, 8, 6, 4, 2, 0};
        String expeted = "4.0 0.5";

        String result = Bloco3.lerNotasMediaNegativasPercentagemNegativas(nAlunos, notas);

        assertEquals(expeted, result);
    }

    // Teste Exercicio 3

    @Test
    public void testePercentagemParesMediaImparesUm() {

        int[] seqNumeros = {1, 2, 3, 4, -5};
        String expected = "0.5 2.0";

        String result = Bloco3.percentagemParesMediaImpares(seqNumeros);

        assertEquals(expected, result);
    }

    @Test
    public void testePercentagemParesMediaImparesDois() {

        int[] seqNumeros = {4, 2, 3, 4, -1};
        String expected = "0.75 3.0";

        String result = Bloco3.percentagemParesMediaImpares(seqNumeros);

        assertEquals(expected, result);
    }

    // Teste Exercicio 4 a)

    @Test
    public void testeNumMultiplosTresUm() {
        int numInicial = 4;
        int numFinal = 10;
        int expected = 2;

        int result = Bloco3.numMultriplosTres(numInicial, numFinal);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumMultiplosTresDois() {
        int numInicial = 2;
        int numFinal = 20;
        int expected = 6;

        int result = Bloco3.numMultriplosTres(numInicial, numFinal);

        assertEquals(expected, result, 0.1);
    }

    // Teste Exercicio 4 b)

    @Test
    public void testeNumMultriplosNumDadoUm() {
        int numInicial = 1;
        int numFinal = 20;
        int numDado = 5;
        int expected = 3;

        int result = Bloco3.numMultriplosNumDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumMultriplosNumDadoDois() {
        int numInicial = 5;
        int numFinal = 25;
        int numDado = 5;
        int expected = 4;

        int result = Bloco3.numMultriplosNumDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumMultriplosNumDadoTres() {
        int numInicial = 6;
        int numFinal = 26;
        int numDado = 4;
        int expected = 5;

        int result = Bloco3.numMultriplosNumDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    // Testes c)

    @Test
    public void testeNumMultiplosTresECincoUm() {
        int numInicial = 2;
        int numFinal = 10;
        String expected = "3 2";

        String result = Bloco3.numMultriplosTresECicno(numInicial, numFinal);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumMultiplosTresECincoDois() {
        int numInicial = 5;
        int numFinal = 25;
        String expected = "7 5";

        String result = Bloco3.numMultriplosTresECicno(numInicial, numFinal);

        assertEquals(expected, result);
    }

    // d)

    @Test
    public void testeNumMultriplosDoisNumDadoUm() {
        int numInicial = 1;
        int numFinal = 10;
        int numDadoUm = 1;
        int numDadoDois = 2;
        String expected = "10 5";

        String result = Bloco3.numMultriplosDoisNumDado(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumMultriplosDoisNumDadoDois() {
        int numInicial = 50;
        int numFinal = 60;
        int numDadoUm = 10;
        int numDadoDois = 4;
        String expected = "2 3";

        String result = Bloco3.numMultriplosDoisNumDado(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumMultriplosDoisNumDadoTres() {
        int numInicial = 60;
        int numFinal = 50;
        int numDadoUm = 10;
        int numDadoDois = 4;
        String expected = "Número inicial superior ao numero final";

        String result = Bloco3.numMultriplosDoisNumDado(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumMultriplosDoisNumDadoQuatro() {
        int numInicial = -5;
        int numFinal = 5;
        int numDadoUm = 10;
        int numDadoDois = 4;
        String expected = "1 3";

        String result = Bloco3.numMultriplosDoisNumDado(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    // Testes e)

    @Test
    public void testeSomaMultiplosDoisNumDadoIntervaloUm() {
        int numInicial = 1;
        int numFinal = 10;
        int numDadoUm = 1;
        int numDadoDois = 2;
        int expected = 85;

        int result = Bloco3.somaMultiplosDoisNumDadoIntervalo(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaMultiplosDoisNumDadoIntervaloDois() {
        int numInicial = 2;
        int numFinal = 20;
        int numDadoUm = 5;
        int numDadoDois = 7;
        int expected = 71;

        int result = Bloco3.somaMultiplosDoisNumDadoIntervalo(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaMultiplosDoisNumDadoIntervaloMenosUm() {
        int numInicial = 20;
        int numFinal = 2;
        int numDadoUm = 5;
        int numDadoDois = 7;
        int expected = -1;

        int result = Bloco3.somaMultiplosDoisNumDadoIntervalo(numInicial, numFinal, numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);
    }

    // Testes Exercicio 5 a)

    @Test
    public void testeSomaNumerosParesIntervaloDadoUm() {
        int numDadoUm = 1;
        int numDadoDois = 6;
        int expected = 12;

        int result = Bloco3.somaNumerosParesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaNumerosParesIntervaloDadoDois() {
        int numDadoUm = 8;
        int numDadoDois = 2;
        int expected = 20;

        int result = Bloco3.somaNumerosParesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaNumerosParesIntervaloDadoTres() {
        int numDadoUm = 8;
        int numDadoDois = -2;
        int expected = 18;

        int result = Bloco3.somaNumerosParesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    // Testes 5.b)

    @Test
    public void testeQuantidadeNumerosParesIntervaloDadoUm() {
        int numDadoUm = 1;
        int numDadoDois = 10;
        int expected = 5;

        int result = Bloco3.quantidadeNumerosParesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeQuantidadeNumerosParesIntervaloDadoDois() {
        int numDadoUm = 10;
        int numDadoDois = -10;
        int expected = 11;

        int result = Bloco3.quantidadeNumerosParesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result);
    }

    // Testes 5.c)

    @Test
    public void testeSomaNumerosImaresIntervaloDadoUm() {
        int numDadoUm = 3;
        int numDadoDois = 15;
        int expected = 63;

        int result = Bloco3.somaNumerosIpmaresIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaNumerosImaresIntervaloDadoDois() {
        int numDadoUm = 3;
        int numDadoDois = -9;
        int expected = -21;

        int result = Bloco3.somaNumerosIpmaresIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);
    }

    // Testes 5.d)

    @Test
    public void testeQuantidadeNumerosImparesIntervaloDadoUm() {
        int numDadoUm = 2;
        int numDadoDois = 15;
        int expected = 7;

        int result = Bloco3.quantidadeNumerosImparesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);

    }

    @Test
    public void testeQuantidadeNumerosImparesIntervaloDadoDois() {
        int numDadoUm = 1;
        int numDadoDois = 17;
        int expected = 9;

        int result = Bloco3.quantidadeNumerosImparesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);

    }

    @Test
    public void testeQuantidadeNumerosImparesIntervaloDadoTres() {
        int numDadoUm = 17;
        int numDadoDois = 1;
        int expected = 9;

        int result = Bloco3.quantidadeNumerosImparesIntervaloDado(numDadoUm, numDadoDois);

        assertEquals(expected, result, 0.1);

    }

    // Testes 5.e)

    @Test
    public void testeSomaMultiplosNumeroDadoIntervaloDadoUm() {
        int numeroInicial = 2;
        int numeroFinal = 10;
        int numDado = 2;
        int expected = 20;

        int result = Bloco3.somaMultiplosNumeroDadoIntervaloDado(numeroInicial, numeroFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaMultiplosNumeroDadoIntervaloDadoDois() {
        int numeroInicial = 15;
        int numeroFinal = 1;
        int numDado = 3;
        int expected = 30;

        int result = Bloco3.somaMultiplosNumeroDadoIntervaloDado(numeroInicial, numeroFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaMultiplosNumeroDadoIntervaloDadoTres() {
        int numeroInicial = -15;
        int numeroFinal = 1;
        int numDado = 3;
        int expected = -1;

        int result = Bloco3.somaMultiplosNumeroDadoIntervaloDado(numeroInicial, numeroFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    // Testes 5.f)

    @Test
    public void testeProdutoNumerosMultiplosNumeroDadoUm() {
        int numeroInicial = 10;
        int numeroFinal = 20;
        int numeroDado = 5;
        int expected = 150;

        int result = Bloco3.produtoNumerosMultiplosNumeroDado(numeroInicial, numeroFinal, numeroDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeProdutoNumerosMultiplosNumeroDadoDois() {
        int numeroInicial = 20;
        int numeroFinal = 10;
        int numeroDado = 5;
        int expected = 150;

        int result = Bloco3.produtoNumerosMultiplosNumeroDado(numeroInicial, numeroFinal, numeroDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeProdutoNumerosMultiplosNumeroDadoTres() {
        int numeroInicial = 10;
        int numeroFinal = 10;
        int numeroDado = 5;
        int expected = -1;

        int result = Bloco3.produtoNumerosMultiplosNumeroDado(numeroInicial, numeroFinal, numeroDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeProdutoNumerosMultiplosNumeroDadoQuatro() {
        int numeroInicial = -10;
        int numeroFinal = 10;
        int numeroDado = 5;
        int expected = -1;

        int result = Bloco3.produtoNumerosMultiplosNumeroDado(numeroInicial, numeroFinal, numeroDado);

        assertEquals(expected, result, 0.1);
    }

    // Testes 5.g)

    @Test
    public void testeMediaMultiplosNumeroDadoIntervaloDadoUm() {
        int numInicial = 4;
        int numFinal = 12;
        int numDado = 4;
        double expected = 6;

        double result = Bloco3.mediaMultiplosNumeroDadoIntervaloDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaMultiplosNumeroDadoIntervaloDadoDois() {
        int numInicial = 12;
        int numFinal = 4;
        int numDado = 2;
        double expected = 7;

        double result = Bloco3.mediaMultiplosNumeroDadoIntervaloDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaMultiplosNumeroDadoIntervaloDadoTres() {
        int numInicial = 12;
        int numFinal = 12;
        int numDado = 2;
        double expected = -1;

        double result = Bloco3.mediaMultiplosNumeroDadoIntervaloDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaMultiplosNumeroDadoIntervaloDadoQuatro() {
        int numInicial = -12;
        int numFinal = 12;
        int numDado = 2;
        double expected = -1;

        double result = Bloco3.mediaMultiplosNumeroDadoIntervaloDado(numInicial, numFinal, numDado);

        assertEquals(expected, result, 0.1);
    }

    // Testes 5.h)

    @Test
    public void testediaMultiplosXeYUm() {
        int x = 1;
        int y = 2;
        int numInicial = 1;
        int numFinal = 10;
        String expected = "1.0 1.5";

        String result = Bloco3.mediaMultiplosXeY(x, y, numInicial, numFinal);

        assertEquals(expected, result);
    }

    @Test
    public void testediaMultiplosXeYDois() {
        int x = 20;
        int y = 22;
        int numInicial = 20;
        int numFinal = 23;
        String expected = "20.0 22.0";

        String result = Bloco3.mediaMultiplosXeY(x, y, numInicial, numFinal);

        assertEquals(expected, result);
    }

    @Test
    public void testediaMultiplosXeYTres() {
        int x = 20;
        int y = 22;
        int numInicial = 23;
        int numFinal = 20;
        String expected = "20.0 22.0";

        String result = Bloco3.mediaMultiplosXeY(x, y, numInicial, numFinal);

        assertEquals(expected, result);
    }

    // Teste 6.a)

    @Test
    public void testeNumeroAlgarismosDeNumeroInteiroLongoUm() {
        long numero = 100;
        int expected = 3;

        int result = Bloco3.numeroAlgarismosDeNumeroInteiroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumeroAlgarismosDeNumeroInteiroLongoDois() {
        long numero = 1846527186;
        int expected = 10;

        int result = Bloco3.numeroAlgarismosDeNumeroInteiroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    // Teste 6.b)

    @Test
    public void testeNumeroAlgarismosParesDeUmNumeroLongo() {
        long numero = 212121212;
        int expected = 5;

        int result = Bloco3.numeroAlgarismosParesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumeroAlgarismosParesDeUmNumeroLongoDois() {
        long numero = 1234567892;
        int expected = 5;

        int result = Bloco3.numeroAlgarismosParesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumeroAlgarismosParesDeUmNumeroLongoTres() {
        long numero = 123456789;
        int expected = 4;

        int result = Bloco3.numeroAlgarismosParesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    // Testes 6.c)

    @Test
    public void testeNumeroAlgarismosImparesDeUmNumeroLongoUm() {
        long numero = 212121212;
        int expected = 4;

        int result = Bloco3.numeroAlgarismosImparesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeNumeroAlgarismosImparesDeUmNumeroLongoDois() {
        long numero = 777223331;
        int expected = 7;

        int result = Bloco3.numeroAlgarismosImparesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    // Testes 6.d)

    @Test
    public void testeSomaAlgarismosDeUmNumeroUm() {
        long numero = 123456789;
        int expected = 45;

        int result = Bloco3.somaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeSomaAlgarismosDeUmNumeroDois() {
        long numero = 88878887;
        int expected = 62;

        int result = Bloco3.somaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.1);
    }

    // Testes 6.e)

    @Test
    public void testeSomaAlgarismosParesDeUmNumeroUm() {
        long numero = 123456789;
        int expected = 20;

        int result = Bloco3.somaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaAlgarismosParesDeUmNumeroDois() {
        long numero = 847351947;
        int expected = 16;

        int result = Bloco3.somaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result);
    }

    // Testes 6.f)

    @Test
    public void testeSomaAlgarismosImparesDeUmNumeroUm() {
        long numero = 123456789;
        int expected = 25;

        int result = Bloco3.somaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaAlgarismosImparesDeUmNumeroDois() {
        long numero = 997862912;
        int expected = 35;

        int result = Bloco3.somaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result);
    }

    // Testes 6.g)

    @Test
    public void testeMediaAlgarismosDeUmNumeroLongoUm() {
        long numero = 123456789;
        double expected = 5.0;

        double result = Bloco3.mediaAlgarismosDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaAlgarismosDeUmNumeroLongoDois() {
        long numero = 94959692;
        double expected = 6.625;

        double result = Bloco3.mediaAlgarismosDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.0001);
    }

    // Testes 6.h

    @Test
    public void testeMediaAlgarismosParesDeUmNumeroLongoUm() {
        long numero = 1234567898;
        double expected = 5.6;

        double result = Bloco3.mediaAlgarismosParesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeMediaAlgarismosParesDeUmNumeroLongoDois() {
        long numero = 864286428;
        double expected = 5.33333;

        double result = Bloco3.mediaAlgarismosParesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.0001);
    }

    // Testes 6.i)

    @Test
    public void testeMediaAlgarismosImparesDeUmNumeroLongoUm() {
        long numero = 123456789;
        double expected = 5.0;

        double result = Bloco3.mediaAlgarismosImparesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.00001);
    }

    @Test
    public void testeMediaAlgarismosImparesDeUmNumeroLongoDois() {
        long numero = 1357924913;
        double expected = 4.75;

        double result = Bloco3.mediaAlgarismosImparesDeUmNumeroLongo(numero);

        assertEquals(expected, result, 0.00001);
    }

    // Testes 6.j)

    @Test
    public void testeConverterNumeroParaOrdemInversaAlgarismos() {
        long numero = 123456789;
        long expected = 987654321;

        long result = Bloco3.converterNumeroParaOrdemInversaAlgarismos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeConverterNumeroParaOrdemInversaAlgarismoDois() {
        long numero = 192837465;
        long expected = 564738291;

        long result = Bloco3.converterNumeroParaOrdemInversaAlgarismos(numero);

        assertEquals(expected, result);
    }

    // Testes 7.a)

    @Test
    public void testeVerificarNumeroECapicuaTrue() {
        long numero = 848;
        boolean expected = true;

        boolean result = Bloco3.isCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroECapicuaFalse() {
        long numero = 841;
        boolean expected = false;

        boolean result = Bloco3.isCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroECapicuaLongTrue() {
        long numero = 98766789;
        boolean expected = true;

        boolean result = Bloco3.isCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroECapicuaLongFalse() {
        long numero = 987667123;
        boolean expected = false;

        boolean result = Bloco3.isCapicua(numero);

        assertEquals(expected, result);
    }

    // Testes 7.b)

    @Test
    public void testeVerificarNumeroEArmstrongTrueUm() {
        int numero = 153;
        boolean expected = true;

        boolean result = Bloco3.isArmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroEArmstrongTrueDois() {
        int numero = 407;
        boolean expected = true;

        boolean result = Bloco3.isArmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroEArmstrongTrueComQuatroAlgarismos() {
        int numero = 1634;
        boolean expected = true;

        boolean result = Bloco3.isArmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroEArmstrongFalseUm() {
        int numero = 143;
        boolean expected = false;

        boolean result = Bloco3.isArmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarNumeroEArmstrongFalseDois() {
        int numero = 1592;
        boolean expected = false;

        boolean result = Bloco3.isArmstrong(numero);

        assertEquals(expected, result);
    }

    // Testes 7.c)

    @Test
    public void testeRetornarPrimeiraCapicuaIntrevaloDadoUm() {
        int numInicial = 10;
        int numFinal = 999;
        int expected = 10;

        int result = Bloco3.retornarPrimeiraCapicuaIntrevaloDado(numInicial, numFinal);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetornarPrimeiraCapicuaIntrevaloDadoDois() {
        int numInicial = 100;
        int numFinal = 999;
        int expected = 101;

        int result = Bloco3.retornarPrimeiraCapicuaIntrevaloDado(numInicial, numFinal);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetornarPrimeiraCapicuaIntrevaloDadoTres() {
        int numInicial = 9000;
        int numFinal = 10000;
        int expected = 9009;

        int result = Bloco3.retornarPrimeiraCapicuaIntrevaloDado(numInicial, numFinal);

        assertEquals(expected, result);
    }

    // Testes 7.e)

    @Test
    public void testeNumeroCapicuasIntervaloDadoUm() {
        int limiteInferior = 100;
        int limiteSupeior = 120;
        int expected = 2;

        int result = Bloco3.numeroCapicuasIntervaloDado(limiteInferior, limiteSupeior);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroCapicuasIntervaloDadoDois() {
        int limiteInferior = 8000;
        int limiteSupeior = 9000;
        int expected = 10;

        int result = Bloco3.numeroCapicuasIntervaloDado(limiteInferior, limiteSupeior);

        assertEquals(expected, result);
    }

    // Testes 7.f)

    @Test
    public void testeRetornarPrimeiroNumeroArmstrongNumIntrevaloDadoUm() {
        int limiteOnferior = 100;
        int limiteSuperior = 400;
        int expected = 153;

        int result = Bloco3.retornarPrimeiroNumeroArmstrongNumIntrevaloDado(limiteOnferior, limiteSuperior);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetornarPrimeiroNumeroArmstrongNumIntrevaloDadoDois() {
        int limiteOnferior = 1000;
        int limiteSuperior = 10000;
        int expected = 1634;

        int result = Bloco3.retornarPrimeiroNumeroArmstrongNumIntrevaloDado(limiteOnferior, limiteSuperior);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetornarPrimeiroNumeroArmstrongNumIntrevaloDadoTres() {
        int limiteOnferior = 100000;
        int limiteSuperior = 10000000;
        int expected = 548834;

        int result = Bloco3.retornarPrimeiroNumeroArmstrongNumIntrevaloDado(limiteOnferior, limiteSuperior);

        assertEquals(expected, result);
    }

    // Testes 7.g)

    @Test
    public void testeRetornarQuantidadeNumerosArmstrongNumDadoIntervalo() {
        int limiteInferior = 100;
        int limiteSuperior = 1000;
        int expected = 4;

        int result = Bloco3.retornarQuantidadeNumerosArmstrongNumDadoIntervalo(limiteInferior, limiteSuperior);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetornarQuantidadeNumerosArmstrongNumDadoIntervaloDois() {
        int limiteInferior = 10000;
        int limiteSuperior = 1000000;
        int expected = 4;

        int result = Bloco3.retornarQuantidadeNumerosArmstrongNumDadoIntervalo(limiteInferior, limiteSuperior);

        assertEquals(expected, result);
    }

    // Testes Exercicio 9

    @Test
    public void testeCclacularSalarioMensal() {
        int horasExtra = 1;
        int salarioBase = 500;
        double expected = 510;

        double result = Bloco3.calcularSalarioMensal(horasExtra, salarioBase);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeCclacularSalarioMensalDois() {
        int horasExtra = 9;
        int salarioBase = 792;
        double expected = 934.56;

        double result = Bloco3.calcularSalarioMensal(horasExtra, salarioBase);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeMediaSalariosMensais() {
        double[] salariosMensais = {500, 600, 300, 400, 1000};
        double expected = 560;

        double result = Bloco3.mediaSalariosMensais(salariosMensais);

        assertEquals(expected, result, 0.001);
    }

    @Test
    public void testeMediaSalariosMensaisDois() {
        double[] salariosMensais = {551, 630.23, 300.99, 4120.09, 1000.01};
        double expected = 1320.464;

        double result = Bloco3.mediaSalariosMensais(salariosMensais);

        assertEquals(expected, result, 0.001);
    }

    // Testes Exercicio 11

    @Test
    public void testeNumeroDeManeirasParaObterNumeroDadoDeUmAVinteComNMaiorQueDoisDigitosUm() {
        int numeroDado = 20;
        int expected = 1;

        int result = Bloco3.numeroDeManeirasParaObterNumeroDadoDeUmAVinte(numeroDado);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeManeirasParaObterNumeroDadoDeUmAVinteComNMaiorQueDoisDigitosDois() {
        int numeroDado = 18;
        int expected = 2;

        int result = Bloco3.numeroDeManeirasParaObterNumeroDadoDeUmAVinte(numeroDado);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeManeirasParaObterNumeroDadoDeUmAVinteComNDeUmDigitoUm() {
        int numeroDado = 5;
        int expected = 3;

        int result = Bloco3.numeroDeManeirasParaObterNumeroDadoDeUmAVinte(numeroDado);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeManeirasParaObterNumeroDadoDeUmAVinteComNDeUmDigitoDois() {
        int numeroDado = 7;
        int expected = 4;

        int result = Bloco3.numeroDeManeirasParaObterNumeroDadoDeUmAVinte(numeroDado);

        assertEquals(expected, result);
    }

    // Testes Exercicio 12

    @Test
    public void testeFformulaResolventeCompletaComAIgualAZero() {
        double a = 0;
        double b = 1;
        double c = 2;
        String expected = "Não é equação do segundo grau";

        String result = Bloco3.formulaResolventeCompleta(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void testeFformulaResolventeCompletaUm() {
        double a = 1;
        double b = -3;
        double c = -10;
        String expected = "A equação tem duas raízes reais x = 5.0 e x = -2.0";

        String result = Bloco3.formulaResolventeCompleta(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void testeFformulaResolventeCompletaDois() {
        double a = 9;
        double b = -12;
        double c = 4;
        String expected = "A equação tem uma raiz dupla x = 0.6666666666666666";

        String result = Bloco3.formulaResolventeCompleta(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void testeFformulaResolventeCompletaTres() {
        double a = 5;
        double b = 3;
        double c = 5;
        String expected = "A equação tem raizes imaginárias";

        String result = Bloco3.formulaResolventeCompleta(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoLeituraTerminada() {
        int codigoProduto = 0;
        String expected = "Leitura dos códigos terminada";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoAlimenntoNaoPerecivel() {
        int codigoProduto = 1;
        String expected = "Alimento não perecível";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoAlimenntoPerecivel() {
        int codigoProduto = 3;
        String expected = "Alimento perecível";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoVestuario() {
        int codigoProduto = 6;
        String expected = "Vestuário";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoHigienePessoal() {
        int codigoProduto = 7;
        String expected = "Higiene pessoal";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoLimpeza() {
        int codigoProduto = 12;
        String expected = "Limpeza e utensílios domésticos";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    @Test
    public void testeCclassificarProdutoCodigoInvalido() {
        int codigoProduto = 16;
        String expected = "Codigo Inválido";

        String result = Bloco3.classificarProduto(codigoProduto);

        assertEquals(expected, result);
    }

    // Testes Exercicio 14

    @Test
    public void testeSelecionarMoedaCambioDolar() {
        String moeda = "D";
        double expected = 1.534;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioLibra() {
        String moeda = "L";
        double expected = 0.774;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioIene() {
        String moeda = "I";
        double expected = 161.480;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioCoroa() {
        String moeda = "C";
        double expected = 9.593;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioFrancoSuico() {
        String moeda = "F";
        double expected = 1.601;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioFrancoDefaut() {
        String moeda = "";
        double expected = 1;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeSelecionarMoedaCambioFrancoDefautDois() {
        String moeda = "G";
        double expected = 1;

        double result = Bloco3.selecionarMoedaCambio(moeda);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testeCambioDaQuantiaDadaDolarDezEuros() {
        double quantia = 10;
        String moeda = "D";
        double expected = 15.34;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }

    @Test
    public void testeCambioDaQuantiaDadaLibraDezEuros() {
        double quantia = 10;
        String moeda = "L";
        double expected = 7.74;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }

    @Test
    public void testeCambioDaQuantiaDadaIeneDezEuros() {
        double quantia = 10;
        String moeda = "I";
        double expected = 1614.80;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }

    @Test
    public void testeCambioDaQuantiaDadaCoroaDezEuros() {
        double quantia = 10;
        String moeda = "C";
        double expected = 95.93;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }

    @Test
    public void testeCambioDaQuantiaDadaFrancoSuicoDezEuros() {
        double quantia = 10;
        String moeda = "F";
        double expected = 16.01;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }

    @Test
    public void testeCambioDaQuantiaDadaDefautDezEuros() {
        double quantia = 10;
        String moeda = "E";
        double expected = 10.00;

        double result = Bloco3.cambioDaQuantiaDada(quantia, moeda);

        assertEquals(expected, result, 0.000001);
    }
}