package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio15Test {

    @Test
    public void notasInteirasParaQualitativasMau() {
        int nota = 3;
        String expected = "Mau";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasMediocreComFlutuante() {
        int nota = (int) 5.1;
        String expected = "Medíocre";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasSuficiente() {
        int nota = 13;
        String expected = "Suficiente";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasBom() {
        int nota = 16;
        String expected = "Bom";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasMuitoBom() {
        int nota = 19;
        String expected = "Muito Bom";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasNotaInvalidaNegativa() {
        int nota = -1;
        String expected = "Nota inválida";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }

    @Test
    public void notasInteirasParaQualitativasNotaInvalidaPositiva() {
        int nota = 21;
        String expected = "Nota inválida";

        String result = Exercicio15.notasInteirasParaQualitativas(nota);

        assertEquals(expected, result);
    }
}