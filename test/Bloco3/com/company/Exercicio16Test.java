package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio16Test {

    @Test
    public void calcularSalarioLiquidoQuinhentos() {
        double salario = 500;
        double expected = 425;

        double result = Exercicio16.calcularSalarioLiquido(salario);

        assertEquals(expected, result, 0.00001);

    }

    @Test
    public void calcularSalarioLiquidoQuatrocentos() {
        double salario = 400;
        double expected = 360;

        double result = Exercicio16.calcularSalarioLiquido(salario);

        assertEquals(expected, result, 0.00001);

    }

    @Test
    public void calcularSalarioLiquidoMil() {
        double salario = 1000;
        double expected = 850;

        double result = Exercicio16.calcularSalarioLiquido(salario);

        assertEquals(expected, result, 0.00001);

    }

    @Test
    public void calcularSalarioLiquidoMilEUm() {
        double salario = 1001;
        double expected = 800.8;

        double result = Exercicio16.calcularSalarioLiquido(salario);

        assertEquals(expected, result, 0.00001);

    }

    @Test
    public void calcularSalarioLiquidoNegativo() {
        double salario = -1001;
        double expected = -1;

        double result = Exercicio16.calcularSalarioLiquido(salario);

        assertEquals(expected, result, 0.00001);

    }
}