package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio17Test {

    @Test
    public void quantidadeDeRacaoEmGramasRacaPequena() {
        double pesoCao = 10;
        double expected = 100;

        double result = Exercicio17.quantidadeDeRacaoEmGramas(pesoCao);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void quantidadeDeRacaoEmGramasRacaMedia() {
        double pesoCao = 11;
        double expected = 250;

        double result = Exercicio17.quantidadeDeRacaoEmGramas(pesoCao);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void quantidadeDeRacaoEmGramasRacaGrande() {
        double pesoCao = 26;
        double expected = 300;

        double result = Exercicio17.quantidadeDeRacaoEmGramas(pesoCao);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void quantidadeDeRacaoEmGramasRacaGigante() {
        double pesoCao = 46;
        double expected = 500;

        double result = Exercicio17.quantidadeDeRacaoEmGramas(pesoCao);

        assertEquals(expected, result, 0.1);
    }
}