package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio18Test {

    @Test
    public void verificacaoIDUm() {
        int numeroID = 142143820;
        boolean expected = true;

        boolean result = Exercicio18.verificacaoID(numeroID);

        assertEquals(expected, result);
    }

    @Test
    public void verificacaoIDDois() {
        int numeroID = 235711136;
        boolean expected = true;

        boolean result = Exercicio18.verificacaoID(numeroID);

        assertEquals(expected, result);
    }

    @Test
    public void verificacaoIDTres() {
        int numeroID = 151157324;
        boolean expected = true;

        boolean result = Exercicio18.verificacaoID(numeroID);

        assertEquals(expected, result);
    }

    @Test
    public void verificacaoIDInvalido() {
        int numeroID = 736152438;
        boolean expected = false;

        boolean result = Exercicio18.verificacaoID(numeroID);

        assertEquals(expected, result);
    }
}