package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio19Test {

    @Test
    public void testeRetirarImparesDeUmArray() {

        int[] nNumerosInteiros = {1,2,3,4,5,6,7,8,9};
        int[] expected = {1,3,5,7,9};

        int[] result = Exercicio19.retirarOsImparesDeUmArray(nNumerosInteiros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarParesDeUmArray() {
        int[] nNumerosInteiros = {1,2,3,4,5,6,7,8,9};
        int[] expected = {2,4,6,8};

        int[] result = Exercicio19.retirarOsParesDeUmArray(nNumerosInteiros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarImparesDeUmArrayDois() {
        int[] nNumerosInteiros = {2,2,3,4,7,6,7,9,9};
        int[] expected = {3,7,7,9,9};

        int[] result = Exercicio19.retirarOsImparesDeUmArray(nNumerosInteiros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarParesDeUmArrayDois() {
        int[] nNumerosInteiros = {2,1,2,3,4,5,6,7,8,9,8,2};
        int[] expected = {2,2,4,6,8,8,2};

        int[] result = Exercicio19.retirarOsParesDeUmArray(nNumerosInteiros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeCounterPares() {
        int[] nNumerosInteiros = {2,1,2,3,4,5,6,7,8,9,8,2};
        int expected = 7;

        int result = Exercicio19.countEvenElements(nNumerosInteiros);

        assertEquals(expected, result);
    }

    @Test
    public void testeParesDireitaImparesEsquerda() {
        int[] numerosInteiros = {1,2,3,4,5,6};
        int[] impares = {1,3,5};
        int[] pares = {2,4,6};
        int[] expected = {1,3,5,2,4,6};

        int[] result = Exercicio19.paresDireitaImparesEsquerda(impares, pares, numerosInteiros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParesDireitaImparesEsquerdaDois() {
        int[] numerosInteiros = {2,2,3,3,4,7,1,2,3,4};
        int[] impares = {3,3,7,1,3};
        int[] pares = {2,2,4,2,4};
        int[] expected = {3,3,7,1,3,2,2,4,2,4};

        int[] result = Exercicio19.paresDireitaImparesEsquerda(impares, pares, numerosInteiros);

        assertArrayEquals(expected, result);
    }
}