package Bloco3.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio20Test {

    @Test
    public void testeXlassificacaoNumeroInteCroEmPerfeito() {
        int numero = 6;
        String expected = "Perfeito";

        String result = Exercicio20.classificacaoNumeroInteiroEmPerfeitoAbundanteOuReduzido(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeXlassificacaoNumeroInteCroEmAbundante() {
        int numero = 12;
        String expected = "Abundante";

        String result = Exercicio20.classificacaoNumeroInteiroEmPerfeitoAbundanteOuReduzido(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeXlassificacaoNumeroInteCroEmReduzido() {
        int numero = 9;
        String expected = "Reduzido";

        String result = Exercicio20.classificacaoNumeroInteiroEmPerfeitoAbundanteOuReduzido(numero);

        assertEquals(expected, result);
    }
}