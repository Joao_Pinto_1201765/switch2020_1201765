package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Exercicio10Test {

    // Testes a) NumeroDeMenorValorDeUmVetor

    @Test
    public void testeNumeroDeMenorValorDeUmVetor() {
        int[] vetor = {1, 2, 3, 4, 5};
        int expected = 1;

        int result = Exercicio10.numeroDeMenorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeMenorValorDeUmVetorDois() {
        int[] vetor = {5, 4, 3, 2, 1};
        int expected = 1;

        int result = Exercicio10.numeroDeMenorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeMenorValorDeUmVetorTres() {
        int[] vetor = {6, 3, 6, 2, 8, 5, 10};
        int expected = 2;

        int result = Exercicio10.numeroDeMenorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    // Testes b) NumeroDeMaiorValorDeUmVetor

    @Test
    public void testeNumeroDeMaiorValorDeUmVetor() {
        int[] vetor = {1, 2, 3, 4, 5};
        int expected = 5;

        int result = Exercicio10.numeroDeMaiorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeMaiorValorDeUmVetorDois() {
        int[] vetor = {5, 6, 3, 4, 1, 2};
        int expected = 6;

        int result = Exercicio10.numeroDeMaiorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeMaiorValorDeUmVetorTres() {
        int[] vetor = {1, 1, 1, 1, 1, 1};
        int expected = 1;

        int result = Exercicio10.numeroDeMaiorValorDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    // Testes c)
    // Testes SomaValoresDeUmVetor

    @Test
    public void testeSomaValoresDeUmVetor() {
        int[] vetor = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int expected = 45;

        int result = Exercicio10.somaValoresDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaValoresDeUmVetorDois() {
        int[] vetor = {9, 9, 9, 9, 9, 9};
        int expected = 54;

        int result = Exercicio10.somaValoresDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    // Testes MediaValoresDeUmVetor

    @Test
    public void testeMediaValoresDeUmVetor() {
        int[] vetor = {9, 9, 9, 9, 9, 9};
        double expected = 9;

        double result = Exercicio10.mediaValoresDeUmVetor(vetor);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaValoresDeUmVetorDois() {
        int[] vetor = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        double expected = 5;

        double result = Exercicio10.mediaValoresDeUmVetor(vetor);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaValoresDeUmVetorTres() {
        int[] vetor = {5, 4, 2, 7, 8, 9};
        double expected = 5.8;

        double result = Exercicio10.mediaValoresDeUmVetor(vetor);

        assertEquals(expected, result, 0.1);
    }

    // Testes d) SomaProdutosValoresDeUmVetor

    @Test
    public void testeSomaProdutosValoresDeUmVetor() {
        int[] vetor = {1, 2, 3, 4, 5};
        int expected = 120;

        int result = Exercicio10.somaProdutosValoresDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaProdutosValoresDeUmVetorDois() {
        int[] vetor = {10, 5, 10, 2};
        int expected = 1000;

        int result = Exercicio10.somaProdutosValoresDeUmVetor(vetor);

        assertEquals(expected, result);
    }

    // Testes e) RetornarConjuntoDeElementosNaoRepetidosDeUmVetor e ContarValoresNaoZeroNumArray //
    // e RetirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidos e RetirarValoresZeroNumArray

    @Test
    public void testeRetornarConjuntoDeElementosNaoRepetidosDeUmVetor() {
        int[] vetor = {1, 2, 3, 3, 4};
        int[] expected = {1, 2, 0, 3, 4};

        int[] result = Exercicio10.retornarConjuntoDeElementosNaoRepetidosDeUmVetorIncluindoZerosDeSubstituicao(vetor);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeContarValoresNaoZeroNumArray() {
        int[] array = {1, 2, 0, 3, 4};
        int expected = 4;

        int result = Exercicio10.contarValoresNaoZeroNumArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeContarValoresNaoZeroNumArrayDois() {
        int[] array = {1, 2, 0, 3, 4, 0, 3};
        int expected = 5;

        int result = Exercicio10.contarValoresNaoZeroNumArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeRetirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidos() {
        int[] array = {1, 2, 3, 3, 4};
        int[] expected = {1, 2, 3, 4};

        int[] result = Exercicio10.retirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidos(array);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidosDois() {
        int[] array = {1, 2, 3, 3, 4, 4, 5, 5, 6};
        int[] expected = {1, 2, 3, 4, 5, 6};

        int[] result = Exercicio10.retirarValoresZeroNumArrayDadoUmArrayComNumerosRepetidos(array);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarValoresZeroNumArray() {
        int[] array = {1, 2, 0, 3, 4, 0, 3};
        int[] expected = {1, 2, 3, 4, 3};

        int[] result = Exercicio10.retirarValoresZeroNumArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarValoresZeroNumArrayDois() {
        int[] array = {1, 2, 0, 3, 4, 0, 3, 0, 5};
        int[] expected = {1, 2, 3, 4, 3, 5};

        int[] result = Exercicio10.retirarValoresZeroNumArray(array);

        assertArrayEquals(expected, result);
    }

    // Teste f) InverterVetor

    @Test
    public void testeInverterVetor() {
        int[] array = {5, 6, 7};
        int[] expected = {7, 6, 5};

        int[] result = Exercicio10.inverterVetor(array);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeInverterVetorDois() {
        int[] array = {1, 2, 3};
        int[] expected = {3, 2, 1};

        int[] result = Exercicio10.inverterVetor(array);

        assertArrayEquals(expected, result);
    }

    // Testes 10 g)

    @Test
    public void testeVerificarSeEPrimo() {
        int numero = 13;
        boolean expected = true;

        boolean result = Exercicio10.verificarSeEPrimoNumeroNatural(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarSeEPrimoDois() {
        int numero = 11;
        boolean expected = true;

        boolean result = Exercicio10.verificarSeEPrimoNumeroNatural(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarSeEPrimoTres() {
        int numero = 4;
        boolean expected = false;

        boolean result = Exercicio10.verificarSeEPrimoNumeroNatural(numero);

        assertEquals(expected, result);
    }

    // Teste RetornarSoElementosPrimosDeUmVetor

    @Test
    public void testeRetornarSoElementosPrimosComZerosDeUmVetor() {
        int[] array = {11, 12, 13};
        int[] expected = {11, 0, 13};

        int[] result = Exercicio10.retornarSoElementosPrimosComZerosDeUmVetor(array);

        assertArrayEquals(expected, result);

    }

    @Test
    public void testeRetornarSoElementosPrimosComZerosDeUmVetorDois() {
        int[] array = {11, 12, 13, 14, 15, 16, 17};
        int[] expected = {11, 0, 13, 0, 0, 0, 17};

        int[] result = Exercicio10.retornarSoElementosPrimosComZerosDeUmVetor(array);

        assertArrayEquals(expected, result);

    }

    @Test
    public void testeRetornarSoPrimosDeUmVetor() {
        int[] array = {11, 12, 13, 14, 15, 16, 17};
        int[] expected = {11, 13, 17};

        int[] result = Exercicio10.retornarSoPrimosDeUmVetor(array);

        assertArrayEquals(expected, result);
    }
}