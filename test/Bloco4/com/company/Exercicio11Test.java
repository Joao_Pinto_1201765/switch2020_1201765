package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Exercicio11Test {

    @Test
    public void testeVerificarSeDoisVetoresMesmoTamanho() {
        int[] vetorUm = {};
        int[] vetorDois = {};
        boolean expected = true;

        boolean result = Exercicio11.verificarSeDoisVetoresMesmoTamanho(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarSeDoisVetoresMesmoTamanhoDois() {
        int[] vetorUm = {1, 2, 3};
        int[] vetorDois = {6, 7, 4};
        boolean expected = true;

        boolean result = Exercicio11.verificarSeDoisVetoresMesmoTamanho(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarSeDoisVetoresMesmoTamanhoTres() {
        int[] vetorUm = {9};
        int[] vetorDois = {6, 7, 4};
        boolean expected = false;

        boolean result = Exercicio11.verificarSeDoisVetoresMesmoTamanho(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeVerificarSeDoisVetoresMesmoTamanhoQuatro() {
        int[] vetorUm = {8};
        int[] vetorDois = {};
        boolean expected = false;

        boolean result = Exercicio11.verificarSeDoisVetoresMesmoTamanho(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoEscalarDeDoisVetores() {
        int[] vetorUm = {2, 1};
        int[] vetorDois = {4, 3};
        Integer expected = 11;

        Integer result = Exercicio11.produtoEscalarDeDoisVetores(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoEscalarDeDoisVetoresDois() {
        int[] vetorUm = {3, 3};
        int[] vetorDois = {2, -2};
        Integer expected = 0;

        Integer result = Exercicio11.produtoEscalarDeDoisVetores(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoEscalarDeDoisVetoresTres() {

        //Arrange
        int[] vetorUm = {3};
        int[] vetorDois = {2, -2};
        Integer result;

        //Act
        result = Exercicio11.produtoEscalarDeDoisVetores(vetorUm, vetorDois);

        //Assert
        assertNull(result);
    }

    @Test
    public void testeProdutoEscalarDeDoisVetoresQuatro() {
        int[] vetorUm = {2, 1, 1};
        int[] vetorDois = {4, 3, 1};
        Integer expected = 12;

        Integer result = Exercicio11.produtoEscalarDeDoisVetores(vetorUm, vetorDois);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoEscalarDeDoisVetoresCinco() {
        int[] vetorUm = {2, 1, 1};
        int[] vetorDois = {4, 3, 2};
        Integer expected = 13;

        Integer result = Exercicio11.produtoEscalarDeDoisVetores(vetorUm, vetorDois);

        assertEquals(expected, result);
    }
}