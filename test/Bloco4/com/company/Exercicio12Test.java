package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio12Test {

    @Test
    public void verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthPositivo() {
        int[][] matriz = {
                {1,2,3},
                {1,2,3}
        };

        int expected = 3;

        int result = Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthNegativo() {
        int[][] matriz = {
                {1,2,3},
                {1,2}
        };

        int expected = -3;

        int result = Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthNegativoDois() {
        int[][] matriz = {
                {1,2,3,4},
                {1,2}
        };

        int expected = -4;

        int result = Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthPositivoDois() {
        int[][] matriz = {
                {1,2,3,4,5,6},
                {1,2,3,4,1,2}
        };

        int expected = 6;

        int result = Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void verificarSeUmaMatrizTemTodasAsLinhasDeIgualLengthPositivoTres() {
        int[][] matriz = {
                {1,2,3,4,5,6},
                {1,2,3,4,1,2},
                {1,2,3,4,5,6}
        };

        int expected = 6;

        int result = Exercicio12.verificarSeUmaMatrizTemTodasAsLinhasDeIgualLength(matriz);

        assertEquals(expected, result);
    }
}