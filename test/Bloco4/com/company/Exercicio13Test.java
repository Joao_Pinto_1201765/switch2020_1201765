package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio13Test {

    @Test
    public void testeEMatrizQuadradaTrueUm() {
        int[][] matriz = {
                {1,2,3},
                {2,3,4},
                {3,4,5}
        };

        boolean result = Exercicio13.isSquare(matriz);

        assertTrue(result);
    }

    @Test
    public void testeEMatrizQuadradaTrueDois() {
        int[][] matriz = {
                {1,2,3,6},
                {2,3,4,7},
                {3,4,5,3},
                {1,2,3,4}
        };

        boolean result = Exercicio13.isSquare(matriz);

        assertTrue(result);
    }

    @Test
    public void testeEMatrizQuadradaFalseUm() {
        int[][] matriz = {
                {1,2,3},
                {2,3},
                {3,4,5}
        };


        boolean result = Exercicio13.isSquare(matriz);

        assertFalse(result);
    }

    @Test
    public void testeEMatrizQuadradaFalseDois() {
        int[][] matriz = {
                {1,2,3},
                {2,3,4,7},
                {3,4,5,3},
                {1,2,3,4}
        };

        boolean result = Exercicio13.isSquare(matriz);

        assertFalse(result);
    }
}