package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio14Test {

    @Test
    public void testeEMAtrizRectangularTrue() {
        int[][] matriz = {
                {1,2,3},
                {1,2,3},
        };
        boolean result;

        result = Exercicio14.eMAtrizRectangular(matriz);

        assertTrue(result);
    }

    @Test
    public void testeEMAtrizRectangularTrueDois() {
        int[][] matriz = {
                {1,2,3,8},
                {1,2,3,5},
                {1,2,3,4}
        };
        boolean result;

        result = Exercicio14.eMAtrizRectangular(matriz);

        assertTrue(result);
    }

    @Test
    public void testeEMAtrizRectangularFalse() {
        int[][] matriz = {
                {1,2,3},
                {1,2,3},
                {1,2,3}
        };
        boolean result;

        result = Exercicio14.eMAtrizRectangular(matriz);

        assertFalse(result);
    }

    @Test
    public void testeEMAtrizRectangularFalseDois() {
        int[][] matriz = {
                {1,2,3,4},
                {1,2,3},
                {1,2,3,4}
        };
        boolean result;

        result = Exercicio14.eMAtrizRectangular(matriz);

        assertFalse(result);
    }
}