package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio15Test {

    // Testes 15

    // Testes a)

    @Test
    public void testeElementoDeMenorValorNumaMatriz() {
        int[][] matriz = {
                {2, 1},
                {4, 3}
        };
        int expected = 1;

        int result = Exercicio15.elementoDeMenorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeElementoDeMenorValorNumaMatrizDois() {
        int[][] matriz = {
                {2, 1},
                {4, 3, 0}
        };
        int expected = 0;

        int result = Exercicio15.elementoDeMenorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeElementoDeMenorValorNumaMatrizTres() {
        int[][] matriz = {
                {2, 2},
                {4, 3, 1}
        };
        int expected = 1;

        int result = Exercicio15.elementoDeMenorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeElementoDeMenorValorNumaMatrizQuatro() {
        int[][] matriz = {
                {0, 2},
                {4, 3, 1}
        };
        int expected = 0;

        int result = Exercicio15.elementoDeMenorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    // Testes b)

    @Test
    public void testeEelementoDeMaiorValorNumaMatriz() {
        int[][] matriz = {
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 7, 9, 8}
        };
        int expected = 9;

        int result = Exercicio15.elementoDeMaiorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeEelementoDeMaiorValorNumaMatrizDois() {
        int[][] matriz = {
                {1, 2, 3, 4},
                {4, 5, 6, 7},
                {8}
        };
        int expected = 8;

        int result = Exercicio15.elementoDeMaiorValorNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    // Testes c) Contar Elementos da Matriz e Media de Todos os Elementos

    @Test
    public void testeCountTotalElementosNumaMatriz() {
        int[][] matriz = {
                {1},
                {1},
                {1}
        };
        int expected = 3;

        int result = Exercicio15.countTotalElementosNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeCountTotalElementosNumaMatrizDois() {
        int[][] matriz = {
                {1},
                {},
                {1}
        };
        int expected = 2;

        int result = Exercicio15.countTotalElementosNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeCountTotalElementosNumaMatrizTres() {
        int[][] matriz = {
                {1, 2, 3},
                {1, 2, 3, 4},
                {}
        };
        int expected = 7;

        int result = Exercicio15.countTotalElementosNumaMatriz(matriz);

        assertEquals(expected, result);
    }

    // Testes Media Matriz

    @Test
    public void testeMediaMatriz() {
        int[][] matriz = {
                {1, 2, 3, 4},
                {5, 6, 7}
        };

        double expected = 4;

        double result = Exercicio15.mediaMatriz(matriz);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeMediaMatrizDois() {
        int[][] matriz = {
                {9},
                {5, 6, 7},
                {123},
                {}
        };

        double expected = 30;

        double result = Exercicio15.mediaMatriz(matriz);

        assertEquals(expected, result, 0.01);
    }

    // Testes d) Produto da Matriz

    @Test
    public void testeProdutoMatriz() {
        int[][] matriz = {
                {0},
                {1, 2, 3, 4},
                {5, 6, 7, 8}
        };

        int expected = 0;

        int result = Exercicio15.produtoMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizDois() {
        int[][] matriz = {
                {1, 2, 3, 4},
                {},
                {5, 6, 7, 8}
        };

        int expected = 40320;

        int result = Exercicio15.produtoMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizTres() {
        int[][] matriz = {
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4}
        };

        int expected = 13824;

        int result = Exercicio15.produtoMatriz(matriz);

        assertEquals(expected, result);
    }

    // e)
    // Copiar Matriz

    @Test
    public void testeCopiarMatriz() {
        int[][] matriz = {
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] expected = {
                {1, 2, 3},
                {1, 2, 3}
        };

        int[][] result = Exercicio15.copiarMatriz(matriz);

        assertArrayEquals(expected, result);
        assertNotSame(matriz, result);
    }

    @Test
    public void testeCopiarMatrizDois() {
        int[][] matriz = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] expected = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };

        int[][] result = Exercicio15.copiarMatriz(matriz);

        assertArrayEquals(expected, result);
        assertNotSame(matriz, result);
    }


    // Teste Matriz com Linhas e Colunas iguais a Matriz dada

    @Test
    public void testeMatrizComLinhasEColunasIguais() {
        int[][] matriz = {
                {1, 2},
                {1, 2, 3, 4},
                {1, 2, 3}
        };

        int[][] expected = {
                {0, 0},
                {0, 0, 0, 0},
                {0, 0, 0}
        };

        int[][] result = Exercicio15.matrizComLinhasEColunasIguaisComElementosAZero(matriz);

        assertArrayEquals(expected, result);
    }


    // Teste Retirar Elementos Repetidos

    @Test
    public void testeRetirarOsNumerosRepetidosPorZerosNumaMatriz() {
        int[][] matriz = {
                {1, 1, 3},
                {2, 2, 3}
        };

        int[][] expected = {
                {0, 1, 3},
                {0, 2, 3}
        };

        int[][] result = Exercicio15.retirarOsNumerosRepetidosPorZerosNumaMatriz(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetirarOsNumerosRepetidosPorZerosNumaMatrizDois() {
        int[][] matriz = {
                {1, 1, 3, 3},
                {2, 2, 3}
        };

        int[][] expected = {
                {0, 1, 0, 3},
                {0, 2, 3}
        };

        int[][] result = Exercicio15.retirarOsNumerosRepetidosPorZerosNumaMatriz(matriz);

        assertArrayEquals(expected, result);
    }

    // Testes f) Primos

    @Test
    public void testeElementosPrimosDeUmaMatriz() {
        int[][] matriz = {
                {1, 13, 3, 4},
                {6, 6, 11}
        };
        int[][] expected = {
                {0, 13, 3, 0},
                {0, 0, 11}
        };

        int[][] result = Exercicio15.elementosPrimosDeUmaMatriz(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeElementosPrimosDeUmaMatrizDois() {
        int[][] matriz = {
                {1, 13, 3, 4, 4, 2, 2, 2},
                {6, 6, 11, 13, 7}
        };
        int[][] expected = {
                {0, 13, 3, 0, 0, 2, 2, 2},
                {0, 0, 11, 13, 7}
        };

        int[][] result = Exercicio15.elementosPrimosDeUmaMatriz(matriz);

        assertArrayEquals(expected, result);
    }

    // Testes f) diagonal princial de uma matriz

    @Test
    public void testeDiagonalPrincipalMatrizQuadrada() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        int[][] expected = {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        };

        int[][] result = Exercicio15.diagonalPrincipal(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalPrincipalMatrizQuadradaDois() {
        int[][] matriz = {
                {1, 1, 1, 2},
                {1, 1, 1, 2},
                {1, 1, 1, 2},
                {1, 1, 1, 2}
        };
        int[][] expected = {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 2}
        };

        int[][] result = Exercicio15.diagonalPrincipal(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalPrincipalMatrizRetangularUm() {
        int[][] matriz = {
                {1, 1, 1, 2},
                {1, 1, 1, 2},
                {1, 1, 1, 2}
        };
        int[][] expected = {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0}
        };

        int[][] result = Exercicio15.diagonalPrincipal(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalPrincipalMatrizRetangularDois() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        int[][] expected = {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1},
                {0, 0, 0}
        };

        int[][] result = Exercicio15.diagonalPrincipal(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalPrincipalMatrizNull() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1, 2},
                {1, 1, 1},
                {1, 1, 1}
        };


        int[][] result = Exercicio15.diagonalPrincipal(matriz);

        assertNull(result);
    }

    // Teste h) Diagonal Secundária de Uma Matriz

    @Test
    public void testeDiagonalSecundaria() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        int[][] expected = {
                {0, 0, 1},
                {0, 1, 0},
                {1, 0, 0}
        };

        int[][] result = Exercicio15.diagonalSecundaria(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalSecundariaDois() {
        int[][] matriz = {
                {1, 1, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 1, 1}
        };
        int[][] expected = {
                {0, 0, 0, 1},
                {0, 0, 1, 0},
                {0, 1, 0, 0}
        };

        int[][] result = Exercicio15.diagonalSecundaria(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalSecundariaTres() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        int[][] expected = {
                {0, 0, 1},
                {0, 1, 0},
                {1, 0, 0},
                {0, 0, 0}
        };

        int[][] result = Exercicio15.diagonalSecundaria(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeDiagonalSecundariaMatrizNull() {
        int[][] matriz = {
                {1, 1, 1},
                {1, 1, 1, 2},
                {1, 1, 1},
                {1, 1, 1}
        };


        int[][] result = Exercicio15.diagonalSecundaria(matriz);

        assertNull(result);
    }

    // i) Testes e CriarMatrizIdentidadeParaComparacao

    @Test
    public void testeCriarMatrizIdentidadeParaComparacao() {
        int[][] matriz = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };

        int[][] expected = {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        };

        int[][] result = Exercicio15.criarMatrizIdentidadeParaComparacao(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeCriarMatrizIdentidadeParaComparacaoDois() {
        int[][] matriz = {
                {1, 2, 3, 5},
                {1, 2, 3, 5},
                {1, 2, 3, 4},
                {1, 2, 3, 4}
        };

        int[][] expected = {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };

        int[][] result = Exercicio15.criarMatrizIdentidadeParaComparacao(matriz);

        assertArrayEquals(expected, result);
    }

    // Teste Verificar Se Matriz de Identidade

    @Test
    public void testeMatrizIdentidadeTrueUm() {
        int[][] matriz = {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };

        boolean result;

        result = Exercicio15.eMatrizIdentidade(matriz);

        assertTrue(result);
    }

    @Test
    public void testeMatrizIdentidadeTrueDois() {
        int[][] matriz = {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1},
        };

        boolean result;

        result = Exercicio15.eMatrizIdentidade(matriz);

        assertTrue(result);
    }

    @Test
    public void testeMatrizIdentidadeFalseUm() {
        int[][] matriz = {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 1, 1}
        };

        boolean result;

        result = Exercicio15.eMatrizIdentidade(matriz);

        assertFalse(result);
    }

    @Test
    public void testeMatrizIdentidadeFalseDois() {
        int[][] matriz = {
                {1, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };

        boolean result;

        result = Exercicio15.eMatrizIdentidade(matriz);

        assertFalse(result);
    }

    @Test
    public void testeMatrizIdentidadeFalseTres() {
        int[][] matriz = {
                {1, 1, 1, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };

        boolean result;

        result = Exercicio15.eMatrizIdentidade(matriz);

        assertFalse(result);
    }

    // Testes j) Matriz Inversa


    @Test
    public void testeMatrizInversaNull() {
        int[][] matriz = {
                {1, 2, 3},
                {1, 2}
        };

        double[][] result = Exercicio15.matrizInversa(matriz);

        assertNull(result);
    }

    @Test
    public void testeMatrizInversa() {
        int[][] matriz = {
                {2, 1},
                {5, 3}
        };
        double[][] expected = {
                {3, -1},
                {-5, 2}
        };

        double[][] result = Exercicio15.matrizInversa(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizInversaDois() {
        int[][] matriz = {
                {2, 1},
                {0, 1}
        };
        double[][] expected = {
                {0.5, -0.5},
                {0, 1}
        };

        double[][] result = Exercicio15.matrizInversa(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizInversaTres() {
        int[][] matriz = {
                {4, -1},
                {2, 0}
        };
        double[][] expected = {
                {0, 0.5},
                {-1, 2}
        };

        double[][] result = Exercicio15.matrizInversa(matriz);

        assertArrayEquals(expected, result);
    }

    //Testes Menor Complementar


    @Test
    public void testeMenorComplementar() {
        int[][] matriz = {
                {5, 0, 1},
                {-2, 3, 4},
                {0, 2, -1}
        };
        int i = 1;
        int j = 0;
        int[][] expected = {
                {0, 1},
                {2, -1}
        };

        int[][] result = Exercicio15.menorComplementar(matriz, i, j);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMenorComplementarDois() {
        int[][] matriz = {
                {5, 0, 1},
                {-2, 3, 4},
                {0, 2, -1}
        };
        int i = 1;
        int j = 1;
        int[][] expected = {
                {5, 1},
                {0, -1}
        };

        int[][] result = Exercicio15.menorComplementar(matriz, i, j);

        assertArrayEquals(expected, result);
    }

    // Teste Matriz Adjunta

    @Test
    public void testeMatrizAdjunta() {
        int[][] matriz = {
                {3, 1, 1},
                {1, 3, -1},
                {2, 4, 1}
        };

        double[][] expected = {
                {7, 3, -4},
                {-3, 1, 4},
                {-2, -10, 8}
        };

        double[][] result = Exercicio15.matrizAdjunta(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizAdjuntaDois() {
        int[][] matriz = {
                {1, -4},
                {4, -7}
        };

        double[][] expected = {
                {-7, 4},
                {-4, 1}
        };

        double[][] result = Exercicio15.matrizAdjunta(matriz);

        assertArrayEquals(expected, result);
    }


    //Teste Inversa Ordem Tres e superior

    @Test
    public void testeMatrizInversaOrdemTresUm() {
        int[][] matriz = {
                {1, 2, -1},
                {3, 4, -1},
                {0, 2, 0}
        };
        double[][] expected = {
                {-0.5, 0.5, -0.5},
                {0, 0, 0.5},
                {-1.5, 0.5, 0.5}
        };

        double[][] result = Exercicio15.matrizInversa(matriz);

        assertArrayEquals(expected, result);
    }

    // Teste k) Matriz Transposta

    @Test
    public void testeMatrizTransposta() {
        double[][] matriz = {
                {1, 2, 3},
                {0, 1, 4},
                {5, 6, 0}
        };

        double[][] expected = {
                {1, 0, 5},
                {2, 1, 6},
                {3, 4, 0}
        };

        double[][] result = Exercicio15.matrizTransposta(matriz);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizTranspostaDois() {
        double[][] matriz = {
                {1, 2},
                {3, 4},
                {5, 6}

        };

        double[][] expected = {
                {1, 3, 5},
                {2, 4, 6}

        };

        double[][] result = Exercicio15.matrizTransposta(matriz);

        assertArrayEquals(expected, result);
    }
}
