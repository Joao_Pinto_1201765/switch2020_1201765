package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Exercicio16Test {

    @Test
    public void testeDeterminanteMatrizNaoQuadrada() {
        int[][] matriz = {
                {1, 2}
        };

        Integer result = Exercicio16.determinanteMatriz(matriz);

        assertNull(result);
    }

    @Test
    public void testeDeterminanteMatrizComLinhasDeDiferentesTamanhos() {
        int[][] matriz = {
                {1, 2, 1},
                {1, 2},
                {1, 2, 3}
        };

        Integer result = Exercicio16.determinanteMatriz(matriz);

        assertNull(result);
    }

    @Test
    public void testeOrdemUm() {
        int[][] matriz = {{1}};
        int expected = 1;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemDois() {
        int[][] matriz = {
                {1, 2},
                {1, 2}
        };
        int expected = 0;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemDoisDois() {
        int[][] matriz = {
                {3, 2},
                {2, 2}
        };
        int expected = 2;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemTres() {
        int[][] matriz = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int expected = 0;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemTresDois() {
        int[][] matriz = {
                {1, 3, 0},
                {2, 5, 1},
                {2, 1, 3}
        };
        int expected = 2;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemQuadroUm() {
        int[][] matriz = {
                {1, 2, 4, 5},
                {6, 7, 8, 0},
                {4, 5, 6, 8},
                {5, 6, 7, 8}
        };
        int expected = 8;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }

    @Test
    public void testeOrdemQuadroUmDois() {
        int[][] matriz = {
                {4, 5, -3, 0},
                {2, -1, 3, 1},
                {1, -3, 2, 1},
                {0, 2, -2, 5}
        };
        int expected = 210;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }


    @Test
    public void testeOrdemQuadroUmTres() {
        int[][] matriz = {
                {1, 2, 4, 5, 1},
                {6, 7, 8, 0, 0},
                {4, 5, 6, 8, 3},
                {5, 6, 7, 4, 8},
                {6, 4, 2, 2, 2}
        };
        int expected = -962;

        int result = Exercicio16.determinanteMatriz(matriz);

        assertEquals(expected, result);
    }
}
