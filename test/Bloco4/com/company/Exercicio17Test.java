package Bloco4.com.company;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio17Test {

    // Testes Exercicio 17
    // Testes a) Produto de uma Matriz por uma Constante

    @Test
    public void testeProdutoMatrizPorConstante() {
        int[][] matriz = {
                {10, 6},
                {4, 3}
        };

        int constante = 2;
        int[][] expected = {
                {20, 12},
                {8, 6}
        };

        int[][] result = Exercicio17.produtoMatrizPorConstante(matriz, constante);

        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizPorConstanteDois() {
        int[][] matriz = {
                {10, 6},
                {4, 3}
        };

        int constante = 0;
        int[][] expected = {
                {0, 0},
                {0, 0}
        };

        int[][] result = Exercicio17.produtoMatrizPorConstante(matriz, constante);

        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizPorConstanteNull() {
        int[][] matriz = {
                {}
        };
        int constante = 1;

        int[][] result = Exercicio17.produtoMatrizPorConstante(matriz, constante);

        Assert.assertNull(result);

    }

    @Test
    public void testeProdutoMatrizPorConstanteNullDois() {
        int[][] matriz = {
                {},
                {}
        };
        int constante = 1;

        int[][] result = Exercicio17.produtoMatrizPorConstante(matriz, constante);

        Assert.assertNull(result);

    }

    // Teste b) Soma de Duas Matrizes

    @Test
    public void testeSomaDuasMatrizes() {
        int[][] matrizUm = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] matrizDois = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] expected = {
                {2, 4, 6},
                {2, 4, 6},
                {2, 4, 6}
        };

        int[][] result = Exercicio17.somaDuasMatrizes(matrizUm, matrizDois);

        Assert.assertArrayEquals(expected, result);
        Assert.assertNotNull(result);
    }

    @Test
    public void testeSomaDuasMatrizesDois() {
        int[][] matrizUm = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] matrizDois = {
                {3, 2, 1},
                {3, 2, 1},
                {3, 2, 1}
        };
        int[][] expected = {
                {4, 4, 4},
                {4, 4, 4},
                {4, 4, 4}
        };

        int[][] result = Exercicio17.somaDuasMatrizes(matrizUm, matrizDois);

        Assert.assertArrayEquals(expected, result);
        Assert.assertNotNull(result);
    }

    @Test
    public void testeSomaDuasMatrizesNull() {
        int[][] matrizUm = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3, 4}
        };
        int[][] matrizDois = {
                {3, 2, 1},
                {3, 2, 1},
                {3, 2, 1}
        };

        int[][] result = Exercicio17.somaDuasMatrizes(matrizUm, matrizDois);

        Assert.assertNull(result);

    }

    @Test
    public void testeSomaDuasMatrizesNullDois() {
        int[][] matrizUm = {
                {1, 2, 3},
                {1, 2, 3}
        };
        int[][] matrizDois = {
                {3, 2, 1},
                {3, 2, 1},
                {3, 2, 1}
        };

        int[][] result = Exercicio17.somaDuasMatrizes(matrizUm, matrizDois);

        Assert.assertNull(result);

    }

    // Testes c)

    @Test
    public void testeProdutoDuasMatrizesNull() {
        int[][] matrizUm = {{1}};
        int[][] matrizDois = {
                {2, 2},
                {0, 1}
        };

        int[][] result = Exercicio17.produtoDuasMatrizes(matrizUm, matrizDois);

        Assert.assertNull(result);
    }

    @Test
    public void testeProdutoDuasMatrizesNullDois() {
        int[][] matrizUm = {{}};
        int[][] matrizDois = {
                {2, 2},
                {0, 1}
        };

        int[][] result = Exercicio17.produtoDuasMatrizes(matrizUm, matrizDois);

        Assert.assertNull(result);
    }

    @Test
    public void testeProdutoDuasMatrizesNullTres() {
        int[][] matrizUm = null;
        int[][] matrizDois = {
                {2, 2},
                {0, 1}
        };

        int[][] result = Exercicio17.produtoDuasMatrizes(matrizUm, matrizDois);

        Assert.assertNull(result);
    }


    @Test
    public void testeProdutoDuasMatrizes() {
        int[][] matrizUm = {
                {1, 3},
                {2, 5}
        };
        int[][] matrizDois = {
                {2, 2},
                {0, 1}
        };

        int[][] expected = {
                {2, 5},
                {4, 9}
        };

        int[][] result = Exercicio17.produtoDuasMatrizes(matrizUm, matrizDois);

        Assert.assertArrayEquals(expected, result);
        Assert.assertNotNull(result);
    }

    @Test
    public void testeProdutoDuasMatrizesDois() {
        int[][] matrizUm = {
                {1, 0},
                {0, 2}
        };
        int[][] matrizDois = {
                {0, 1},
                {1, 0}
        };

        int[][] expected = {
                {0, 1},
                {2, 0}
        };

        int[][] result = Exercicio17.produtoDuasMatrizes(matrizUm, matrizDois);

        Assert.assertArrayEquals(expected, result);
        Assert.assertNotNull(result);
    }
}