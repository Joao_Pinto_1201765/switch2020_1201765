package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Testes Sopa de Letras - Exercício 18
 */
public class Exercicio18Test {

    // a)

    /**
     * Testes Matriz Mascara
     */

    @Test
    public void testeMatrizMascaraNull() {
        char[][] matrizMascara = {
                {'A', 'B'},
                {'B'}
        };

        int[][] result = Exercicio18.matrizMascara(matrizMascara, 'A', 1);

        assertNull(result);
    }

    @Test
    public void testeMatrizMascaraNullDois() {
        char[][] matrizLetras = null;

        int[][] result = Exercicio18.matrizMascara(matrizLetras, 'A', 1);

        assertNull(result);
    }

    @Test
    public void testeMatrizMascara() {
        char[][] matrizLetras = {
                {'A', 'B', 'C'},
                {'D', 'F', 'G'},
                {'H', 'I', 'J'}
        };
        char letra = 'I';
        int[][] expected = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 2, 0}
        };

        int[][] result = Exercicio18.matrizMascara(matrizLetras, letra, 2);

        assertNotNull(result);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizMascaraDois() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };
        char letra = 'Q';
        int[][] expected = {
                {0, 0, 0, 1},
                {0, 0, 0, 1},
                {0, 0, 0, 1},
                {0, 0, 1, 0}
        };

        int[][] result = Exercicio18.matrizMascara(matrizLetras, letra, 1);

        assertNotNull(result);
        assertArrayEquals(expected, result);
    }

    /**
     * Testes Letra Corresponde A Posição
     */

    @Test
    public void testeLetraCorrespondeAPosicaoTrue() {
        char[][] matrizLetras = {
                {'A', 'B'},
                {'C', 'D'}
        };
        int i = 0;
        int j = 0;

        char letra = 'A';

        boolean result = Exercicio18.letraCorrespondeAPosicao(matrizLetras, i, j, letra);

        assertTrue(result);
    }

    @Test
    public void testeLetraCorrespondeAPosicaoTrueDois() {
        char[][] matrizLetras = {
                {'A', 'B'},
                {'C', 'D'}
        };
        int i = 1;
        int j = 0;

        char letra = 'C';

        boolean result = Exercicio18.letraCorrespondeAPosicao(matrizLetras, i, j, letra);

        assertTrue(result);
    }

    @Test
    public void testeLetraCorrespondeAPosicaoFalse() {
        char[][] matrizLetras = {
                {'A', 'B'},
                {'C', 'D'}
        };
        int i = 0;
        int j = 0;

        char letra = 'B';

        boolean result = Exercicio18.letraCorrespondeAPosicao(matrizLetras, i, j, letra);

        assertFalse(result);
    }

    /**
     * Testes Verificar se Matriz do tipo Char é Nula
     */

    @Test
    public void testeIsNull() {
        char[][] matriz = null;
        boolean result = Exercicio18.isMatrixNull(matriz);

        assertTrue(result);
    }

    @Test
    public void testeIsNullFalse() {
        char[][] matriz = {};

        boolean result = Exercicio18.isMatrixNull(matriz);

        assertFalse(result);
    }

    @Test
    public void testeIsNullDois() {
        int[][] matriz = null;
        boolean result = Exercicio18.isMatrixNull(matriz);

        assertTrue(result);
    }

    /**
     * Teste Soma Duas Matrizes
     */

    @Test
    public void testeSomarMatrizNull() {
        int[][] matrizUm = {
                {2, 2, 2},
                {2, 2, 2}
        };
        int[][] matrizDois = {
                {2, 2},
                {2, 2}
        };

        int[][] result = Exercicio18.somarMatriz(matrizUm, matrizDois);

        assertNull(result);

    }

    @Test
    public void testeSomarMatriz() {
        int[][] matrizUm = {
                {2, 2, 2},
                {2, 2, 2},
                {2, 2, 2}
        };
        int[][] matrizDois = {
                {2, 2, 2},
                {2, 2, 2},
                {2, 2, 2,}
        };
        int[][] expected = {
                {4, 4, 4},
                {4, 4, 4},
                {4, 4, 4}
        };

        int[][] result = Exercicio18.somarMatriz(matrizUm, matrizDois);

        assertNotNull(result);
        assertArrayEquals(expected, result);

    }

    // b)

    /**
     * Testes String to Char Array
     */

    @Test
    public void testeStringToCharNull() {
        String palavra = null;

        char[] result = Exercicio18.stringToCharArray(palavra);

        assertNull(result);
    }

    @Test
    public void testeStringToCharNullDois() {
        String palavra = "";

        char[] result = Exercicio18.stringToCharArray(palavra);

        assertNull(result);
    }

    @Test
    public void testeStringToCharArrayAndNotNull() {
        String palavra = "ola";
        char[] expected = {'o', 'l', 'a'};

        char[] result = Exercicio18.stringToCharArray(palavra);

        assertArrayEquals(expected, result);
        assertNotNull(result);
    }

    @Test
    public void testeStringToCharArrayAndNotNullDois() {
        String palavra = "switch";
        char[] expected = {'s', 'w', 'i', 't', 'c', 'h'};

        char[] result = Exercicio18.stringToCharArray(palavra);

        assertArrayEquals(expected, result);
        assertNotNull(result);
    }

    /**
     * Testes Direçao Direita
     */

    @Test
    public void testeDireçaoDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "ABC";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçaoDireitaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AQA";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçaoDireitaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QAA";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoDireitaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QPS";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoDireitaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "QPS";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoDireitaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "ABS";

        boolean result = Exercicio18.direcaoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direção Esquerda
     */

    @Test
    public void testeDireçaoEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "JIH";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçaoEsquerdaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AQA";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçaoEsquerdaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QBA";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoEsquerdaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QPS";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoEsquerdaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "QPS";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçaoEsquerdaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "ABS";

        boolean result = Exercicio18.direcaoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direção Cima
     */

    @Test
    public void testeDireçaoCima() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "IFB";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AQQ";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "FBA";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AAQQQ";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "DAQ";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "BF";

        boolean result = Exercicio18.direcaoCima(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direçao Baixo
     */

    @Test
    public void testeDireçãoBaixo() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "CGJ";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QQA";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QAA";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AAQQQ";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "ADS";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "FB";

        boolean result = Exercicio18.direcaoBaixo(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direçao Cima/Direita
     */

    @Test
    public void testeDireçãoCimaDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "IGQ";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaDireitaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AJQ";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaDireitaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AJD";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaDireitaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AIGD";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaDireitaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "DBA";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaDireitaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "FB";

        boolean result = Exercicio18.direcaoCimaDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direçao Cima/Esquerda
     */

    @Test
    public void testeDireçãoCimaEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "JFA";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaEsquerdaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QGB";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoCimaEsquerdaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QGA";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaEsquerdaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AJFAB";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaEsquerdaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "DA";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoCimaEsquerdaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "FAS";

        boolean result = Exercicio18.direcaoCimaEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direçao Baixo/Esquerda
     */

    @Test
    public void testeDireçãoBaixoEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QJA";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoEsquerdaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QGIA";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoEsquerdaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QGA";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoEsquerdaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "QGIAG";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoEsquerdaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "BDF";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoEsquerdaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "BA";

        boolean result = Exercicio18.direcaoBaixoEsquerda(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Direçao Baixo/Direita
     */

    @Test
    public void testeDireçãoBaixoDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "DIQ";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoDireitaDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AFJA";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testeDireçãoBaixoDireitaFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "DIA";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoDireitaFalseDois() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q'},
                {'D', 'F', 'G', 'Q'},
                {'H', 'I', 'J', 'Q'},
                {'A', 'A', 'Q', 'A'}
        };

        String palavra = "AFJAQ";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoDireitaFalseTres() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "AFS";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testeDireçãoBaixoDireitaFalseQuatro() {

        char[][] matrizLetras = {
                {'A', 'B'},
                {'D', 'F'},
        };

        String palavra = "AB";

        boolean result = Exercicio18.direcaoBaixoDireita(matrizLetras, palavra);

        assertFalse(result);
    }

    /**
     * Testes Palavra Existe Na Matriz
     */

    @Test
    public void testePalavraExisteNaMatrizDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "QBZ";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "RWQJI";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizCima() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "WLB";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizBaixo() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "HZR";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizCimaDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "AWC";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizCimaEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "WQC";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizBaixoEsquerda() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "ACI";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizBaixoDireita() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "QQW";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertTrue(result);
    }

    @Test
    public void testePalavraExisteNaMatrizFalse() {

        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        String palavra = "OLA";

        boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertFalse(result);
    }

    @Test
    public void testePalavraExisteNaMatrizNull() {

        char[][] matrizLetras = null;

        String palavra = null;

        Boolean result = Exercicio18.palavraExisteNaMatriz(matrizLetras, palavra);

        assertNull(result);
    }

    // c)

    /**
     * Testes VerificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras
     */

    @Test
    public void testeVerificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras() {
        String[] palavras = {"OLA", "PLANETA", "SWITCH"};
        char[] sequenciaNaMatrizParaComparacao = {'S', 'W', 'I', 'T', 'C', 'H'};

        boolean result = Exercicio18.verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(palavras, sequenciaNaMatrizParaComparacao);

        assertTrue(result);
    }

    @Test
    public void testeVerificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetrasDois() {
        String[] palavras = {"OLA", "PLANETA", "SWITCH"};
        char[] sequenciaNaMatrizParaComparacao = {'O', 'L', 'A'};

        boolean result = Exercicio18.verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(palavras, sequenciaNaMatrizParaComparacao);

        assertTrue(result);
    }

    @Test
    public void testeVerificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetrasFalse() {
        String[] palavras = {"OLA", "PLANETA", "SWITCH"};
        char[] sequenciaNaMatrizParaComparacao = {'O', 'L', 'H', 'O'};

        boolean result = Exercicio18.verificarSeSequenciaDeCharsDadaPelasCoordenadasDoUtilizadorEUmaPalavraQueSeEncontraNaSopaDeLetras(palavras, sequenciaNaMatrizParaComparacao);

        assertFalse(result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoDireita
     */

    @Test
    public void testeSequenciaNaMatrizParaComparacaoDirecaoDireita() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 0;
        int j_inicial = 0;
        int j_final = 3;
        char[] expected = {'A', 'B', 'C', 'Q'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoDireita(matrizLetras, i_inicial, j_inicial, j_final);

        assertArrayEquals(expected, result);

    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoDireita
     */

    @Test
    public void testeSequenciaNaMatrizParaComparacaoDirecaoEsquerda() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 4;
        int j_inicial = 4;
        int j_final = 1;
        char[] expected = {'B', 'Q', 'C', 'B'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoEsquerda(matrizLetras, i_inicial, j_inicial, j_final);

        assertArrayEquals(expected, result);

    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoEsquerda
     */

    @Test
    public void testeSequenciaNaMatrizParaComparacaoDirecaoEsquerdaDois() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 1;
        int j_inicial = 4;
        int j_final = 1;
        char[] expected = {'L', 'Q', 'G', 'F'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoEsquerda(matrizLetras, i_inicial, j_inicial, j_final);

        assertArrayEquals(expected, result);

    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoCima
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoCima() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 3;
        int j_inicial = 1;
        int i_final = 1;
        char[] expected = {'A', 'I', 'F'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoCima(matrizLetras, i_inicial, j_inicial, i_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoBaixo
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoBaixo() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 2;
        int j_inicial = 1;
        int i_final = 4;
        char[] expected = {'I', 'A', 'B'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoBaixo(matrizLetras, i_inicial, j_inicial, i_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoCimaDireito
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoCimaDireita() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 3;
        int j_inicial = 2;
        int i_final = 1;
        int j_final = 4;
        char[] expected = {'Q', 'Q', 'L'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoCimaDireita(matrizLetras, i_inicial, j_inicial, i_final, j_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 2;
        int j_inicial = 2;
        int i_final = 0;
        int j_final = 0;
        char[] expected = {'J', 'F', 'A'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoCimaEsquerda(matrizLetras, i_inicial, j_inicial, i_final, j_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 2;
        int j_inicial = 2;
        int i_final = 4;
        int j_final = 0;
        char[] expected = {'J', 'A', 'A'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda(matrizLetras, i_inicial, j_inicial, i_final, j_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes SequenciaNaMatrizParaComparacaoDirecaoBaixoEsquerda
     */

    @Test
    public void testesequenciaNaMatrizParaComparacaoDirecaoBaixoDireita() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'Q', 'L', 'C'},
                {'H', 'I', 'J', 'Q', 'W', 'R'},
                {'A', 'A', 'Q', 'A', 'Y', 'H'},
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'H', 'I', 'J', 'Q', 'W', 'R'}
        };

        int i_inicial = 2;
        int j_inicial = 2;
        int i_final = 5;
        int j_final = 5;
        char[] expected = {'J', 'A', 'B', 'R'};

        char[] result = Exercicio18.sequenciaNaMatrizParaComparacaoDirecaoBaixoDireita(matrizLetras, i_inicial, j_inicial, i_final, j_final);

        assertArrayEquals(expected, result);
    }

    /**
     * Testes Verificar se Sequencia dada atraves de Coordenadas Contem Palavra Valida na matriz de letras
     */

    /**
     * Teste NULL
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasNull() {
        int i_inicial = 5;
        int j_inicial = 0;
        int i_final = 5;
        int j_final = 0;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        Boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasNullDois() {
        int i_inicial = 5;
        int j_inicial = 0;
        int i_final = -5;
        int j_final = 0;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        Boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasNullTres() {
        int i_inicial = 0;
        int j_inicial = 0;
        int i_final = 6;
        int j_final = 6;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        Boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertNull(result);
    }

    /**
     * Sequecia Direcao Direita
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasDireita() {
        int i_inicial = 5;
        int j_inicial = 0;
        int i_final = 5;
        int j_final = 3;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasFalse() {
        int i_inicial = 5;
        int j_inicial = 0;
        int i_final = 5;
        int j_final = 2;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Esquerda
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasEsquerda() {
        int i_inicial = 1;
        int j_inicial = 4;
        int i_final = 1;
        int j_final = 1;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'O', 'B', 'O', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasEsquerdaFalse() {
        int i_inicial = 2;
        int j_inicial = 4;
        int i_final = 2;
        int j_final = 1;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Cima
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCima() {
        int i_inicial = 4;
        int j_inicial = 1;
        int i_final = 0;
        int j_final = 1;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'O', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'O', 'B', 'L', 'H'},
                {'A', 'C', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCimaFalse() {
        int i_inicial = 2;
        int j_inicial = 4;
        int i_final = 0;
        int j_final = 4;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Baixo
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixo() {
        int i_inicial = 1;
        int j_inicial = 3;
        int i_final = 5;
        int j_final = 3;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'O', 'B', 'L', 'H'},
                {'A', 'C', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixoFalse() {
        int i_inicial = 2;
        int j_inicial = 3;
        int i_final = 5;
        int j_final = 3;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Cima Direita
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCimaDireita() {
        int i_inicial = 4;
        int j_inicial = 1;
        int i_final = 1;
        int j_final = 4;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'O', 'B', 'L', 'H'},
                {'A', 'C', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO", "COOL"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCimaDireitaFalse() {
        int i_inicial = 3;
        int j_inicial = 2;
        int i_final = 2;
        int j_final = 3;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Cima Esquerda
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCimaEsquerda() {
        int i_inicial = 3;
        int j_inicial = 4;
        int i_final = 0;
        int j_final = 1;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'O', 'B', 'L', 'H'},
                {'A', 'C', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO", "COOL"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasCimaEsquerdaFalse() {
        int i_inicial = 5;
        int j_inicial = 2;
        int i_final = 3;
        int j_final = 0;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Baixo Esquerda
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixoEsquerda() {
        int i_inicial = 1;
        int j_inicial = 4;
        int i_final = 4;
        int j_final = 1;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'C', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO", "COOL", "LOCO"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixoEsquerdaFalse() {
        int i_inicial = 2;
        int j_inicial = 5;
        int i_final = 5;
        int j_final = 2;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }

    /**
     * Sequecia Direcao Baixo Direita
     */

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixoDireita() {
        int i_inicial = 2;
        int j_inicial = 3;
        int i_final = 4;
        int j_final = 5;
        char[][] matrizLetras = {
                {'A', 'O', 'C', 'Q', 'B', 'Z'},
                {'D', 'Ç', 'B', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'O', 'C', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"OLA", "COBRA", "BUJA", "LOBO", "COIÇO", "COOL", "LOCO"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetrasBaixoDireitaFalse() {
        int i_inicial = 0;
        int j_inicial = 0;
        int i_final = 5;
        int j_final = 5;
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        boolean result = Exercicio18.verificarSequenciaDadaAtravesDeCoordenadasContemPalavraValidaNaMatrizDeLetras(i_inicial, j_inicial, i_final, j_final, matrizLetras, palavras);

        assertFalse(result);
        assertNotNull(result);
    }
}