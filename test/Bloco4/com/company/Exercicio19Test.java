package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio19Test {

    /**
     * Testes Matriz Mascara Inicial
     */

    @Test
    public void testeMatrizMascaraInicial() {
        int[][] matrizBase = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] expected = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };

        int[][] result = Exercicio19.matrizMascaraInicial(matrizBase);

        assertArrayEquals(expected, result);
        assertNotNull(expected);
    }

    @Test
    public void testeMatrizMascaraInicialDois() {
        int[][] matrizBase = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 9, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {3, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] expected = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 1, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 1, 1}
        };

        int[][] result = Exercicio19.matrizMascaraInicial(matrizBase);

        assertArrayEquals(expected, result);
        assertNotNull(expected);
    }

    @Test
    public void testeMatrizMascaraInicialNull() {
        int[][] matrizBase = null;

        int[][] result = Exercicio19.matrizMascaraInicial(matrizBase);

        assertNull(result);
    }

    @Test
    public void testeMatrizMascaraInicialNullDois() {
        int[][] matrizBase = {
                {0, 0, 2},
                {3, 0},
                {0, 0, 5}
        };

        int[][] result = Exercicio19.matrizMascaraInicial(matrizBase);

        assertNull(result);
    }

    /**
     * Testes Matriz AtualizadaComJogada
     */

    @Test
    public void testeMatrizAtualizdaComJogadaNull() {
        int[][] matrizJogo = {
                {7, 0, 2},
                {3, 1, 0},
                {0, 2, 5}
        };
        int[][] matrizMascaraInicial = {
                {0, 0, 1},
                {1, 0, 0},
                {0, 0, 1}

        };
        int linha = -1;
        int coluna = 10;
        int numeroAJogar = 9;

        int[][] result = Exercicio19.matrizAtualizdaComJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertNull(result);
    }

    @Test
    public void testeMatrizAtualizdaComJogadaNullDois() {
        int[][] matrizJogo = {
                {7, 0, 2},
                {3, 1, 0},
                {0, 2, 5}
        };
        int[][] matrizMascaraInicial = {
                {0, 0, 1},
                {1, 0, 0},
                {0, 0, 1}

        };
        int linha = 1;
        int coluna = 2;
        int numeroAJogar = 0;

        int[][] result = Exercicio19.matrizAtualizdaComJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertNull(result);
    }

    @Test
    public void testeMatrizAtualizdaComJogada() {
        int[][] matrizJogo = {
                {7, 0, 2},
                {3, 1, 0},
                {0, 2, 5}
        };
        int[][] matrizMascaraInicial = {
                {0, 0, 1},
                {1, 0, 0},
                {0, 0, 1}

        };
        int linha = 2;
        int coluna = 2;
        int numeroAJogar = 9;
        int[][] expected = {
                {7, 0, 2},
                {3, 9, 0},
                {0, 2, 5}
        };

        int[][] result = Exercicio19.matrizAtualizdaComJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertArrayEquals(expected, result);
        assertNotNull(result);
    }

    @Test
    public void testeMatrizAtualizdaComJogadaDois() {
        int[][] matrizJogo = {
                {7, 0, 2},
                {3, 1, 0},
                {0, 2, 5}
        };
        int[][] matrizMascaraInicial = {
                {0, 0, 1},
                {1, 0, 0},
                {0, 0, 1}

        };
        int linha = 3;
        int coluna = 1;
        int numeroAJogar = 2;
        int[][] expected = {
                {7, 0, 2},
                {3, 1, 0},
                {2, 2, 5}
        };

        int[][] result = Exercicio19.matrizAtualizdaComJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertArrayEquals(expected, result);
        assertNotNull(result);
    }

    /**
     * Testes ExisteCelulasPorPreencherNaMatriz
     */

    @Test
    public void testeExisteCelulasPorPreencherNaMatrizTrue() {
        int[][] matrizJogo = {
                {7, 0, 2},
                {3, 1, 0},
                {0, 2, 5}
        };

        boolean result = Exercicio19.existeCelulasPorPreencherNaMatriz(matrizJogo);

        assertTrue(result);

    }

    @Test
    public void testeExisteCelulasPorPreencherNaMatrizFalse() {
        int[][] matrizJogo = {
                {7, 4, 2},
                {3, 1, 1},
                {8, 2, 5}
        };

        boolean result = Exercicio19.existeCelulasPorPreencherNaMatriz(matrizJogo);

        assertFalse(result);

    }

    /**
     * Teste Verificar Validade Da Jogada
     */

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNaMatrizDeJogoInicial() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 1;
        int coluna = 1;
        int numeroAJogar = 2;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNaLinha() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 1;
        int coluna = 3;
        int numeroAJogar = 8;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNaColuna() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 9;
        int coluna = 3;
        int numeroAJogar = 7;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoUm() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 1;
        int coluna = 3;
        int numeroAJogar = 9;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoUm() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 1;
        int coluna = 3;
        int numeroAJogar = 1;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoDois() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 1;
        int coluna = 4;
        int numeroAJogar = 5;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoDois() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 3;
        int coluna = 4;
        int numeroAJogar = 2;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoTres() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 2;
        int coluna = 8;
        int numeroAJogar = 6;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoTres() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 2;
        int coluna = 8;
        int numeroAJogar = 2;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoQuatro() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 5;
        int coluna = 2;
        int numeroAJogar = 7;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoQuatro() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 4;
        int coluna = 2;
        int numeroAJogar = 1;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoCinco() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 5;
        int coluna = 5;
        int numeroAJogar = 2;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoCinco() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 5;
        int coluna = 5;
        int numeroAJogar = 5;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoSeis() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 5;
        int coluna = 8;
        int numeroAJogar = 1;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoSeis() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 6;
        int coluna = 8;
        int numeroAJogar = 4;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoSete() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 9;
        int coluna = 2;
        int numeroAJogar = 6;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoSete() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 9;
        int coluna = 1;
        int numeroAJogar = 1;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoOito() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 7;
        int coluna = 4;
        int numeroAJogar = 4;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoOito() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 7;
        int coluna = 5;
        int numeroAJogar = 5;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaFalseNumeroExisteNoBlocoNove() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 9;
        int coluna = 7;
        int numeroAJogar = 5;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertFalse(result);
        assertNotNull(result);
    }

    @Test
    public void testeVerificarValidadeDaJogadaTrueNumeroNaoExisteNoBlocoNove() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };
        int[][] matrizMascaraInicial = {
                {1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}
        };
        int linha = 9;
        int coluna = 7;
        int numeroAJogar = 3;

        boolean result = Exercicio19.verificarValidadeDaJogada(matrizJogo, matrizMascaraInicial, linha, coluna, numeroAJogar);

        assertTrue(result);
        assertNotNull(result);
    }

    /**
     * Teste Bloco Invalido
     */

    @Test
    public void testeBlocoInvalido() {
        int linha = 10;
        int coluna = 9;
        int expected = -1;

        int result = Exercicio19.linhaEColunaPertenceAoBloco(linha, coluna);

        assertEquals(expected, result);
    }

    @Test
    public void testeBlocoInvalidoDois() {
        int linha = 9;
        int coluna = 10;
        int expected = -1;

        int result = Exercicio19.linhaEColunaPertenceAoBloco(linha, coluna);

        assertEquals(expected, result);
    }

    // f)

    /**
     * Testes Veririfcar se o jogo terminou com sucesso
     */

    @Test
    public void testeVerificarSeJogoTerminouComSucessoFalseMatrizImcopleta() {
        int[][] matrizJogo = {
                {5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };

        boolean result = Exercicio19.verificarSeJogoTerminouComSucesso(matrizJogo);

        assertFalse(result);
    }

    @Test
    public void testeVerificarSeJogoTerminouComSucessoFalseMatrizCompletaProdutoDaMatrizDiferente() {
        int[][] matrizJogo = {
                {5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 2, 7, 9}
        };

        boolean result = Exercicio19.verificarSeJogoTerminouComSucesso(matrizJogo);

        assertFalse(result);
    }

    @Test
    public void testeVerificarSeJogoTerminouComSucessoTrue() {
        int[][] matrizJogo = {
                {5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}
        };

        boolean result = Exercicio19.verificarSeJogoTerminouComSucesso(matrizJogo);

        assertTrue(result);
    }
}