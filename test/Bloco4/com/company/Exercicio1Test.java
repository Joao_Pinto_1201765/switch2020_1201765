package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio1Test {

    @Test
    public void testeNumeroDeDigitosDeUmNumeroDez() {
        int numero = 10;
        int expected = 2;

        int result = Exercicio1.numeroDeDigitosDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeNumeroDeDigitosDeUmNumeroCentoEUm() {
        int numero = 101;
        int expected = 3;

        int result = Exercicio1.numeroDeDigitosDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeNnumeroDeDigitosDeUmNumeroTrintaESeisMilESetecentosEOitentaEUm() {
        int numero = 36781;
        int expected = 5;

        int result = Exercicio1.numeroDeDigitosDeUmNumero(numero);

        assertEquals(expected, result);
    }
}