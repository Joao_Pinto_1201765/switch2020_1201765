package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio2Test {

    @Test
    public void testeRetornarVetorComElementosDeNumeroDadoDez() {
        int numeroDado = 10;
        int [] expected = {1,0};

        int [] result = Exercicio2.retornarVetorComElementosDeNumeroDado(numeroDado);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarVetorComElementosDeNumeroDadoCentoEUm() {
        int numeroDado = 101;
        int [] expected = {1,0,1};

        int [] result = Exercicio2.retornarVetorComElementosDeNumeroDado(numeroDado);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarVetorComElementosDeNumeroDadoDezMilCentoEUm() {
        int numeroDado = 10100;
        int [] expected = {1,0,1,0,0};

        int [] result = Exercicio2.retornarVetorComElementosDeNumeroDado(numeroDado);

        assertArrayEquals(expected, result);
    }
}