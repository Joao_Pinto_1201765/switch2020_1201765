package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio3Test {

    @Test
    public void testeSomaDosElementosDeUmVetor() {
        int [] vetor = {3,6,7,8,1};
        int expected = 25;

        int result = Exercicio3.somaDosElementosDeUmVetor(vetor);

        assertEquals(expected, result);
    }
}