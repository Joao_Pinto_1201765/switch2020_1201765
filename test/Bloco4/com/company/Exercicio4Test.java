package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio4Test {

    @Test
    public void testeRetornarArrayApenasComParesDeArrayRecebido() {
        int [] numeros = {1,2,3,4,5,6};
        int [] expected = {2,4,6};

        int [] result = Exercicio4.retornarArrayApenasComParesDeArrayRecebido(numeros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarArrayApenasComParesDeArrayRecebidoDois() {
        int [] numeros = {1,5,3,4,5,6,7,7,8};
        int [] expected = {4,6,8};

        int [] result = Exercicio4.retornarArrayApenasComParesDeArrayRecebido(numeros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarArrayApenasComImparesDeArrayRecebido() {
        int [] numeros = {1,2,3,4,5,6};
        int [] expected = {1,3,5};

        int [] result = Exercicio4.retornarArrayApenasComImparesDeArrayRecebido(numeros);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarArrayApenasComImparesDeArrayRecebidoDois() {
        int [] numeros = {1,5,3,4,5,6,7,7,8};
        int [] expected = {1,5,3,5,7,7};

        int [] result = Exercicio4.retornarArrayApenasComImparesDeArrayRecebido(numeros);

        assertArrayEquals(expected, result);
    }
}