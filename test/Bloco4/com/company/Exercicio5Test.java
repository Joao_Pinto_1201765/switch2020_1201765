package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio5Test {

    // Testes somaParesDeUmArray

    @Test
    public void testeSomaParesDeUmArrayUm() {
        int [] array = {1,2,3,4,5,6};
        int expected = 12;

        int result = Exercicio5.somaParesDeUmArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaParesDeUmArrayDois() {
        int [] array = {1,2,3,4,5,6,1,2,3,4,5,6};
        int expected = 24;

        int result = Exercicio5.somaParesDeUmArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaParesDeUmArrayTres() {
        int [] array = {1,1,1,1,1,1,1,2,2};
        int expected = 4;

        int result = Exercicio5.somaParesDeUmArray(array);

        assertEquals(expected, result);
    }

    // Testes somaImparesDeUmArray

    @Test
    public void testeSomaImparesDeUmArrayUm() {
        int [] array = {1,2,3,4,8,7,6,7};
        int expected = 18;

        int result = Exercicio5.somaImparesDeUmArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaImparesDeUmArrayDois() {
        int [] array = {2,4,6,8,2,4,6,8};
        int expected = 0;

        int result = Exercicio5.somaImparesDeUmArray(array);

        assertEquals(expected, result);
    }

    @Test
    public void testeSomaImparesDeUmArrayTres() {
        int [] array = {1,2,1,12,1,13,14,2};
        int expected = 16;

        int result = Exercicio5.somaImparesDeUmArray(array);

        assertEquals(expected, result);
    }
}