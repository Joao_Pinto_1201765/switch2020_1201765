package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio6Test {

    @Test
    public void testeRetornarVetorComOsPrimeirosNElementosDeUmArrayUm() {
        int [] array = {1,2,3,4,5,6};
        int numeroDeElementos = 2;
        int [] expected = {1,2};

        int [] result = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(array, numeroDeElementos);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarVetorComOsPrimeirosNElementosDeUmArrayDois() {
        int [] array = {1,2,3,4,5,6};
        int numeroDeElementos = 5;
        int [] expected = {1,2,3,4,5};

        int [] result = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(array, numeroDeElementos);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarVetorComOsPrimeirosNElementosDeUmArrayTres() {
        int [] array = {1,1,1,1,4,4,4,4,4,5};
        int numeroDeElementos = 5;
        int [] expected = {1,1,1,1,4};

        int [] result = Exercicio6.retornarVetorComOsPrimeirosNElementosDeUmArray(array, numeroDeElementos);

        assertArrayEquals(expected, result);
    }
}