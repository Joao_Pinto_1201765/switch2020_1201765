package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio7Test {

    // Testes criarArrayDadoIntrevalo

    @Test
    public void testeCriarArrayDadoIntrevaloUm() {
        int limiteInferior = 4;
        int limiteSuperior = 10;
        int [] expected = {4,5,6,7,8,9,10};

        int [] result = Exercicio7.criarArrayDadoIntrevalo(limiteInferior, limiteSuperior);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeCriarArrayDadoIntrevaloDois() {
        int limiteInferior = 10;
        int limiteSuperior = 16;
        int [] expected = {10,11,12,13,14,15,16};

        int [] result = Exercicio7.criarArrayDadoIntrevalo(limiteInferior, limiteSuperior);

        assertArrayEquals(expected, result);
    }

    // Testes calcularDimensaoDeArrayParaIntrevalo

    @Test
    public void testeCalcularDimensaoDeArrayParaIntrevalo() {
        int limiteInferior = 4;
        int limiteSuperior = 10;
        int expected = 7;

        int result = Exercicio7.calcularDimensaoDeArrayParaIntrevalo(limiteInferior, limiteSuperior);

        assertEquals(expected, result);
    }

    @Test
    public void testeCalcularDimensaoDeArrayParaIntrevaloDois() {
        int limiteInferior = 1;
        int limiteSuperior = 10;
        int expected = 10;

        int result = Exercicio7.calcularDimensaoDeArrayParaIntrevalo(limiteInferior, limiteSuperior);

        assertEquals(expected, result);
    }

    // Testes multiplosDeNDadoUmIntrevalo

    @Test
    public void testeMultiplosDeNDadoUmIntrevalo() {
        int limiteInferior = 4;
        int limiteSuperior = 10;
        int n = 3;
        int [] expected = {6,9};

        int [] result = Exercicio7.multiplosDeNDadoUmIntrevalo(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMultiplosDeNDadoUmIntrevaloDois() {
        int limiteInferior = 4;
        int limiteSuperior = 10;
        int n = 2;
        int [] expected = {4,6,8,10};

        int [] result = Exercicio7.multiplosDeNDadoUmIntrevalo(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }
}