package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio8Test {

    /*@Test
    public void testeMultiplosDeVariosNumerosDadoUmIntrevalo() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {2,3};
        int [] expected = {6,12};

        int [] result = Exercicio8.multiplosDeVariosNumerosDadoUmIntrevalo(limiteInferior, limiteSupeior, n);

        assertArrayEquals(expected, result);
    }*/

    //Testes CounterMultiploDeNArrayDeArray

    @Test
    public void testeCounterMultiposDeNArrayDeArray() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {2,3};
        int expected = 8;

        int result = Exercicio8.counterMultiposDeNArrayDeArray(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);

    }

    @Test
    public void testeCounterMultiposDeNArrayDeArrayDois() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {2,3,5};
        int expected = 10;

        int result = Exercicio8.counterMultiposDeNArrayDeArray(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);

    }

    @Test
    public void testeCounterMultiposDeNArrayDeArrayTres() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {3,5,6};
        int expected = 7;

        int result = Exercicio8.counterMultiposDeNArrayDeArray(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);

    }

    // Teste CriarArrayComMultiplos

    @Test
    public void testeCriarArrayDosMultiplosdeNArray() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {2,3};
        int [] expected = {4,6,6,8,9,10,12,12};

        int [] result = Exercicio8.criarArrayDosMultiplosdeNArray(limiteInferior, limiteSupeior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeCriarArrayDosMultiplosdeNArrayDois() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {1,2,3};
        int [] expected = {4,4,5,6,6,6,7,8,8,9,9,10,10,11,12,12,12};

        int [] result = Exercicio8.criarArrayDosMultiplosdeNArray(limiteInferior, limiteSupeior, n);

        assertArrayEquals(expected, result);
    }

    // Teste Counter Multiplos Comuns de n []

    @Test
    public void testeCounterMultiplosComunsDeN() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {2,3};
        int expected = 4;

        int result = Exercicio8.counterMultiplosComunsDeN(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);
    }

    @Test
    public void testeCounterMultiplosComunsDeNDois() {
        int limiteInferior = 4;
        int limiteSupeior = 12;
        int [] n = {1,2,3};
        int expected = 6;

        int result = Exercicio8.counterMultiplosComunsDeN(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);
    }

    @Test
    public void testeCounterMultiplosComunsDeNTres() {
        int limiteInferior = 4;
        int limiteSupeior = 20;
        int [] n = {2,5};
        int expected = 4;

        int result = Exercicio8.counterMultiplosComunsDeN(limiteInferior, limiteSupeior, n);

        assertEquals(expected, result);
    }

    // Testes CriarArrayComOsMultiplosComunsNaoRepetidos

    @Test
    public void testeCriarArrayComOsMultiplosComunsNaoRepetidos() {
        int limiteInferior = 4;
        int limiteSuperior = 20;
        int [] n = {2,5};
        int [] expected = {10,10,20,20};

        int [] result = Exercicio8.criarArrayComOsMultiplosComunsRepetidos(limiteInferior,limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeCriarArrayComOsMultiplosComunsNaoRepetidosDois() {
        int limiteInferior = 4;
        int limiteSuperior = 12;
        int [] n = {2,3};
        int [] expected = {6,6,12,12};

        int [] result = Exercicio8.criarArrayComOsMultiplosComunsRepetidos(limiteInferior,limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    // Teste RetornarSoUmaVezARepeticao


    @Test
    public void testeRetornarSoUmaVezARepeticao() {
        int limiteInferior = 4;
        int limiteSuperior = 12;
        int [] n = {2,3};
        int [] expected = {6,12};

        int [] result = Exercicio8.retornarSoUmaVezARepeticao(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarSoUmaVezARepeticaoDois() {
        int limiteInferior = 4;
        int limiteSuperior = 12;
        int [] n = {2,3,4};
        int [] expected = {12};

        int [] result = Exercicio8.retornarSoUmaVezARepeticao(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarSoUmaVezARepeticaoTres() {
        int limiteInferior = 10;
        int limiteSuperior = 30;
        int [] n = {2,3,5};
        int [] expected = {30};

        int [] result = Exercicio8.retornarSoUmaVezARepeticao(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeRetornarSoUmaVezARepeticaoQuatro() {
        int limiteInferior = 10;
        int limiteSuperior = 30;
        int [] n = {3,6};
        int [] expected = {18,24,30};

        int [] result = Exercicio8.retornarSoUmaVezARepeticao(limiteInferior, limiteSuperior, n);

        assertArrayEquals(expected, result);
    }
}