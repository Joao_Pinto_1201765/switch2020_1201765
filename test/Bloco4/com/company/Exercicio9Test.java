package Bloco4.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Exercicio9Test {

    @Test
    public void testVverificarCapicua() {
        int numero = 101;
        boolean expected = true;

        boolean result = Exercicio9.verificarCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testVverificarCapicuaDois() {
        int numero = 100;
        boolean expected = false;

        boolean result = Exercicio9.verificarCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testVverificarCapicuaTres() {
        int numero = 44;
        boolean expected = true;

        boolean result = Exercicio9.verificarCapicua(numero);

        assertEquals(expected, result);
    }
}