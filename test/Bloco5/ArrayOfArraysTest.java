package Bloco5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayOfArraysTest {

    /**
     * Test Constructors
     */

    @Test
    void testEmptyConstructor() {
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays();

        assertNotNull(arrayOfArrays);
    }

    @Test
    void testNotEmptyConstructor() {
        //Array
        Array array = new Array(new int[]{1, 2, 3, 3});
        Array array2 = new Array(new int[]{1, 2, 3, 3});

        //Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        assertNotNull(arrayOfArrays);
    }

    /**
     * Test Add Element to Row
     */

    @Test
    void testAddElementToRow() {
        //Array
        Array array = new Array(new int[]{1, 2, 3, 3});
        Array array2 = new Array(new int[]{1, 2, 3, 3});
        Array array3 = new Array(new int[]{1, 2, 3, 3, 1});

        //Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        //Expected
        Array[] arrays1 = new Array[2];
        arrays1[0] = array;
        arrays1[1] = array3;
        //arrays1[1].addToArray(1);


        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);
        ArrayOfArrays arrayOfArrays1 = new ArrayOfArrays(arrays1);

        arrayOfArrays.addElementInRow(1, 1);

        assertArrayEquals(arrayOfArrays1.toMatrix(), arrayOfArrays.toMatrix());
        assertEquals(arrays.length, arrays1.length);
        assertEquals(arrayOfArrays, arrayOfArrays1);

    }

    @Test
    void testAddElementToRowIsEmpty() {
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays();
        int value = 1;

        assertThrows(IllegalArgumentException.class, () -> arrayOfArrays.removeFristElementOfGivenValue(value));

    }

    /**
     * Test Remove First Elements of Given Value
     */

    @Test
    void testRemoveFirstElementsOfGivenValue() {
        //Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{1, 2, 3, 5});

        //Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        //Array Of Arrays Expected
        int elementToRemove = 5;
        Array[] expected = new Array[2];
        expected[0] = array;
        expected[1] = array2;
        expected[1].removeFirstElementOf(elementToRemove);

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        arrayOfArrays.removeFristElementOfGivenValue(elementToRemove);

        assertArrayEquals(expected, arrayOfArrays.toArrayOfArrays());
    }

    /**
     * Test larger number in Arrays
     */

    @Test
    void testLargerNumberinArrays() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{1, 2, 3, 5});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        int expected = 5;

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act

        int result = arrayOfArrays.largerNumberInArrays();

        // Assert

        assertEquals(expected, result);
    }

    /**
     * Test smaller number in Arrays
     */

    @Test
    void testSmallerNumberinArrays() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{1, 2, 3, -5});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        int expected = -5;

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act
        int result = arrayOfArrays.smallerNumberInArrays();

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test Mean Of Array Of Arrays
     */

    @Test
    void testMeanOfElementsInArrayOfArrays() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{5, 6, 7, 8});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        double expected = 4.5;

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act
        double result = arrayOfArrays.meanOfArrayOfArrays();

        // Assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfElementsInArrayOfArraysWithNegativeValue() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{5, 6, 7, -8});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        double expected = 2.5;

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act
        double result = arrayOfArrays.meanOfArrayOfArrays();

        // Assert
        assertEquals(expected, result, 0.01);
    }

    /**
     * Test Sum of Each Row in Array[]
     */

    @Test
    void testSumOfeachRowInArrayOfArrays() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{5, 6, 7, 8});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        int[] expected = {10, 26};

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act
        int[] result = arrayOfArrays.sumOfEachRowArrayInArrayOfArrays();

        // Assert
        assertArrayEquals(expected, result);
    }

    /**
     * Test Sum of Each Column in Array[]
     */

    @Test
    void testSumOfeachColumnInArrayOfArrays() {
        // Array
        Array array = new Array(new int[]{1, 2, 3, 4});
        Array array2 = new Array(new int[]{5, 6, 7, 8});

        // Array Of Arrays
        Array[] arrays = new Array[2];
        arrays[0] = array;
        arrays[1] = array2;

        // Expected
        int[] expected = {6, 8, 10, 12};

        // Create BidimensionalArray Object
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(arrays);

        // Act
        int[] result = arrayOfArrays.sumOfEachColumnInArrayOfArrays();

        // Assert
        assertArrayEquals(expected, result);
    }

    /**
     * Test indexOfRowWithBiggerSumOfElements
     */

    @Test
    void testIndexOfRowWithBiggerSumOfElements() {
        int[][] matrix = {
                {1, 2, 3},
                {12, 451, 23},
                {123124124},
                {0, 21}
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int expected = 2;

        int result = arrayOfArrays.indexOfRowWithBiggerSumOfElements();

        assertEquals(expected, result);
    }

    /**
     * Test Is Square
     */

    @Test
    void testIsSquareTrue() {
        int[][] matrix = {
                {1, 2, 3},
                {12, 451, 23},
                {123124124, 4, 1},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.isSquare();

        assertTrue(result);
    }

    @Test
    void testIsSquareFalse() {
        int[][] matrix = {
                {1, 2, 3},
                {12, 451},
                {123124124, 4, 1},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.isSquare();

        assertFalse(result);
    }

    /**
     * Test Is Simetric
     */

    @Test
    void testIsSimetricTrue() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.isSimetric();

        assertTrue(result);
    }

    @Test
    void testIsSimetricFalse() {
        int[][] matrix = {
                {1, 5, 4},
                {5, 3, 8},
                {1, 8, 7},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.isSimetric();

        assertFalse(result);
    }

    /**
     * Test numberOfNotNullElementsInDiagonalOfArrayOfArrays
     */

    @Test
    void testNumberOfNotNullElementsInDiagonalOfArrayOfArraysSquareMatrix() {
        int[][] matrix = {
                {1, 5, 4},
                {5, 3, 8},
                {1, 8, 7},
        };
        int expected = 3;

        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int result = arrayOfArrays.numberOfNotNullElementsInDiagonalOfArrayOfArrays();

        assertEquals(expected, result);
    }

    @Test
    void testNumberOfNotNullElementsInDiagonalOfArrayOfArraysSquareMatrixZero() {
        int[][] matrix = {
                {0, 5, 4},
                {5, 0, 8},
                {1, 8, 0},
        };
        int expected = 0;

        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int result = arrayOfArrays.numberOfNotNullElementsInDiagonalOfArrayOfArrays();

        assertEquals(expected, result);
    }

    @Test
    void testNumberOfNotNullElementsInDiagonalOfArrayOfArraysNotSquareMatrix() {
        int[][] matrix = {
                {0, 5, 4},
                {5, 0, 8},
                {1, 8, 0},
                {1}
        };
        int expected = -1;

        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int result = arrayOfArrays.numberOfNotNullElementsInDiagonalOfArrayOfArrays();

        assertEquals(expected, result);
    }

    /**
     * Test primaryDiagonalEqualsSecundaryDiagonal
     */
    @Test
    void testPrimaryDiagonalEqualsSecundaryDiagonalTrue() {
        int[][] matrix = {
                {0, 5, 0},
                {5, 0, 8},
                {0, 8, 0},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.primaryDiagonalEqualsSecundaryDiagonal();

        assertTrue(result);
    }

    @Test
    void testPrimaryDiagonalEqualsSecundaryDiagonalFalse() {
        int[][] matrix = {
                {0, 5, 2},
                {5, 0, 8},
                {0, 8, 0},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        boolean result = arrayOfArrays.primaryDiagonalEqualsSecundaryDiagonal();

        assertFalse(result);
    }

    /**
     * Test arrayWtihElementsWithNumberOfDigitsBiggerThanAverage
     */

    @Test
    void testArrayWtihElementsWithNumberOfDigitsBiggerThanAverage() {
        int[][] matrix = {
                {11, 5, 2},
                {5, 231, 1234},
                {20, 80, 23},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int[] expected = {231, 1234};

        Array result = arrayOfArrays.arrayWtihElementsWithNumberOfDigitsBiggerThanAverage();

        assertArrayEquals(expected, result.toArray());


    }

    @Test
    void testArrayWtihElementsWithNumberOfDigitsBiggerThanAverageZero() {
        int[][] matrix = {
                {1, 5, 2},
                {5, 1, 1},
                {2, 0, 2},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int[] expected = {};

        Array result = arrayOfArrays.arrayWtihElementsWithNumberOfDigitsBiggerThanAverage();

        assertArrayEquals(expected, result.toArray());
    }

    /**
     * Test arrayWithElementsWithPercentageOfEvenDigitsBiggerThanMean
     */

    @Test
    void testArrayWithElementsWithPercentageOfEvenDigitsBiggerThanMean() {
        int[][] matrix = {
                {11, 12, 22},
                {41, 44, 51},
                {60, 80, 23},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);

        int[] expected = {22, 44, 60, 80};

        Array result = arrayOfArrays.arrayWithElementsWithPercentageOfEvenDigitsBiggerThanMean();

        assertArrayEquals(expected, result.toArray());
    }

    /**
     * Test invertionOfElementsInRow
     */

    @Test
    void testInvertionOfElementsInRow() {
        int[][] matrix = {
                {11, 12, 22},
                {41, 44, 51},
                {60, 80, 23},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int[][] expected = {
                {22, 12, 11},
                {51, 44, 41},
                {23, 80, 60}
        };
        arrayOfArrays.invertionOfElementsInRow();
        int[][] result = arrayOfArrays.toMatrix();

        assertArrayEquals(expected, result);
    }

    /**
     * Test invertionOfElementsInColumn
     */

    @Test
    void testThrowInvertionOfElementsInColumn() {
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays();

        assertThrows(IllegalArgumentException.class, () -> arrayOfArrays.invertionOfElementsInColumn());
    }


    @Test
    void testInvertionOfElementsInColumn() {
        int[][] matrix = {
                {11, 12, 22},
                {41, 44, 51},
                {60, 80, 23},
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int[][] expected = {
                {60, 80, 23},
                {41, 44, 51},
                {11, 12, 22}
        };
        arrayOfArrays.invertionOfElementsInColumn();
        int[][] result = arrayOfArrays.toMatrix();

        assertArrayEquals(expected, result);
    }

    /**
     * Test ninetyDegresRotation
     */

    @Test
    void testNinetyDegresRotation() {
        int[][] matrix = {
                {1, 2},
                {3, 4},
                {5, 6}
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int[][] expected = {
                {5, 3, 1},
                {6, 4, 2}
        };
        arrayOfArrays.ninetyDegresRotation();
        int[][] result = arrayOfArrays.toMatrix();

        assertArrayEquals(expected, result);
    }

    /**
     * Test ninetyDegresRotationNegative
     */

    @Test
    void testNinetyDegresRotationNegative() {
        int[][] matrix = {
                {1, 2},
                {3, 4},
                {5, 6}
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int[][] expected = {
                {2, 4, 6},
                {1, 3, 5}
        };
        arrayOfArrays.ninetyDegresRotationNegative();
        int[][] result = arrayOfArrays.toMatrix();

        assertArrayEquals(expected, result);
    }

    /**
     * Test oneHundredAndEitghtyDegresRotation
     */

    @Test
    void testOneHundredAndEitghtyDegresRotation() {
        int[][] matrix = {
                {1, 2},
                {3, 4},
                {5, 6}
        };
        ArrayOfArrays arrayOfArrays = new ArrayOfArrays(matrix);
        int[][] expected = {
                {6,5},
                {4,3},
                {2,1}
        };
        arrayOfArrays.oneHundredAndEitghtyDegresRotation();
        int[][] result = arrayOfArrays.toMatrix();

        assertArrayEquals(expected, result);
    }
}