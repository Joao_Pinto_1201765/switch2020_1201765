package Bloco5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    void testInitializeArrayWithValues() {
        int[] ints = {1, 2, 3, 4, 5, 6};

        Array array = new Array(ints);

        assertArrayEquals(ints, array.toArray());
    }

    @Test
    void testInitializeArrayWithoutValues() {
        Array array = new Array();

        int[] expected = {};

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testeSetArray() {
        int[] array1 = {1, 2, 3, 4, 5, 6};

        Array array2 = new Array();
        array2.setArray(array1);

        assertArrayEquals(array1, array2.toArray());
    }

    @Test
    void testAddToArrayEmpty() {
        int intToAdd = 1;
        Array array = new Array();
        int[] expected = {1};

        array.addToArray(intToAdd);

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testAddToArrayNotEmpty() {
        int intToAdd = 5;
        int[] intArray = {1, 2, 3, 4};
        Array array = new Array(intArray);
        int[] expected = {1, 2, 3, 4, 5};

        array.addToArray(intToAdd);

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testRemoveFirstElementOfArrayNotEmptyFromLeft() {
        int[] intArray = {1, 2, 3, 4, 5};
        int elementToRemove = 1;
        Array array = new Array(intArray);
        int[] expected = {2, 3, 4, 5};

        array.removeFirstElementOf(elementToRemove);

        assertArrayEquals(expected, array.toArray());

    }

    @Test
    void testRemoveFirstElementOfArrayNotEmptyFromMiddle() {
        int[] intArray = {1, 2, 3, 4, 5};
        int elementToRemove = 3;
        Array array = new Array(intArray);
        int[] expected = {1, 2, 4, 5};

        array.removeFirstElementOf(elementToRemove);

        assertArrayEquals(expected, array.toArray());

    }

    @Test
    void testRemoveFirstElementOfArrayNotEmptyFromRigth() {
        int[] intArray = {1, 2, 3, 4, 5};
        int elementToRemove = 5;
        Array array = new Array(intArray);
        int[] expected = {1, 2, 3, 4};

        array.removeFirstElementOf(elementToRemove);

        assertArrayEquals(expected, array.toArray());

    }

    @Test
    void testRemoveFirstElementOfArrayWithTwoEqualsOfElementToRemove() {
        int[] intArray = {1, 2, 3, 4, 5, 5, 6};
        int elementToRemove = 5;
        Array array = new Array(intArray);
        int[] expected = {1, 2, 3, 4, 5, 6};

        array.removeFirstElementOf(elementToRemove);

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testThrowRemoveElementOfEmptyArray() {
        Array array = new Array();
        int elementToRemove = 1;


        assertThrows(IllegalArgumentException.class, () -> array.removeFirstElementOf(elementToRemove));
    }

    /*@Test
    void testThrowRemoveFirstElementOfArrayElementNotInArray() {
        int[] intArray = {1, 2, 3, 4, 5};
        int elementToRemove = 6;
        Array array = new Array(intArray);

        assertThrows(IllegalArgumentException.class, () -> array.removeFirstElementOf(elementToRemove));
    }
     */

    @Test
    void testValueInPosition() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        int position = 0;
        int expected = 1;

        int result = array.valueInPosition(position);

        assertEquals(expected, result);

    }

    @Test
    void testPositionOutOfBoundsSuperior() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        int position = 5;

        assertThrows(IllegalArgumentException.class, () -> array.valueInPosition(position));

    }

    @Test
    void testPositionOutOfBoundsInferior() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        int position = -1;

        assertThrows(IllegalArgumentException.class, () -> array.valueInPosition(position));
    }

    @Test
    void testNumberOfElementsInArray() {
        int[] intArray = {0, 0, 0, 0, 0};
        Array array = new Array(intArray);
        int expected = 5;

        int result = array.length();

        assertEquals(expected, result);
    }

    @Test
    void testLargerValueInArray() {
        int[] intArray = {1, 2, 35, 4, 2};
        Array array = new Array(intArray);
        int expected = 35;

        int result = array.largerValueInArray();

        assertEquals(expected, result);
    }

    @Test
    void testThrowLargerValueInEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, array::largerValueInArray);
    }

    @Test
    void testSmallerValueInArray() {
        int[] intArray = {23, 41, 35, 10, 51};
        Array array = new Array(intArray);
        int expected = 10;

        int result = array.smallerValueInArray();

        assertEquals(expected, result);
    }

    @Test
    void testThrowSmallerValueInEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.smallerValueInArray());
    }

    @Test
    void testMeanArrayOne() {
        int[] intArray = {23, 41, 35, 10, 51};
        Array array = new Array(intArray);
        double expected = 32;

        double result = array.meanOfElementsInArray();

        assertEquals(expected, result, 0.1);
    }

    @Test
    void testMeanArrayTwoWithDecimals() {
        int[] intArray = {6, 5, 5};
        Array array = new Array(intArray);
        double expected = 5.33;

        double result = array.meanOfElementsInArray();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanArrayTwoWithNegativeElement() {
        int[] intArray = {1, 2, -3, 4, 5};
        Array array = new Array(intArray);
        double expected = 1.8;

        double result = array.meanOfElementsInArray();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testThrowMeanInEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.meanOfElementsInArray());
    }

    @Test
    void testMeanOfEvenNumbers() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        double expected = 3;

        double result = array.meanOfEvenElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfEvenNumbersWithNoEvenNumbers() {
        int[] intArray = {1, 3, 5, 0, 1};
        Array array = new Array(intArray);
        double expected = 0;

        double result = array.meanOfEvenElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfEvenNumbersWithAllEvenNumbers() {
        int[] intArray = {2, 4, 6, 8, 2};
        Array array = new Array(intArray);
        double expected = 4.4;

        double result = array.meanOfEvenElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testThrowMeanOfEvenInEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.meanOfEvenElements());
    }

    @Test
    void testMeanOfOddNumbers() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        double expected = 3;

        double result = array.meanOfOddElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfOddNumbersWithNoOddNumbers() {
        int[] intArray = {2, 2, 2, 2, 2};
        Array array = new Array(intArray);
        double expected = 0;

        double result = array.meanOfOddElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfOddNumbersWithNoOddNumbersAndAZero() {
        int[] intArray = {2, 2, 0, 2, 2};
        Array array = new Array(intArray);
        double expected = 0;

        double result = array.meanOfOddElements();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testThrowMeanOfOddInEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.meanOfOddElements());
    }

    @Test
    void testMeanOfMultiplesOfAGivenNumber() {
        int[] intArray = {1, 2, 3, 4, 5};
        Array array = new Array(intArray);
        int givenNumber = 2;
        double expected = 3;

        double result = array.meanOfMultiplesOfAGivenNumber(givenNumber);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testMeanOfMultiplesOfAGivenNumberTwo() {
        int[] intArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Array array = new Array(intArray);
        int givenNumber = 3;
        double expected = 6;

        double result = array.meanOfMultiplesOfAGivenNumber(givenNumber);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testThrowMeanOfMultiplesOfAGivenNumberZero() {
        int[] intArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Array array = new Array(intArray);
        int givenNumber = 0;

        assertThrows(IllegalArgumentException.class, () -> array.meanOfMultiplesOfAGivenNumber(givenNumber));
    }

    @Test
    void testMeanOfMultiplesOfAGivenNumberNoMultiples() {
        int[] intArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Array array = new Array(intArray);
        int givenNumber = 10;
        double expected = 0;

        double result = array.meanOfMultiplesOfAGivenNumber(givenNumber);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void testSortAscEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.sortAsc());
    }

    @Test
    void testSortAsc() {
        int[] intArray = {5, 4, 2, 1};
        Array array = new Array(intArray);
        int[] expected = {1, 2, 4, 5};

        array.sortAsc();


        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testSortAscWtihSameNumbers() {
        int[] intArray = {1, 1, 1, 1};
        Array array = new Array(intArray);
        int[] expected = {1, 1, 1, 1};

        array.sortAsc();


        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testSortAscWithNegativeNumbers() {
        int[] intArray = {5, 4, -10, 2, 1};
        Array array = new Array(intArray);
        int[] expected = {-10, 1, 2, 4, 5};

        array.sortAsc();

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testSortDescEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.sortDesc());
    }

    @Test
    void testSortDesc() {
        int[] intArray = {5, 3, 4, 1};
        Array array = new Array(intArray);
        int[] expected = {5, 4, 3, 1};

        array.sortDesc();

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testSortDescWtihSameNumbers() {
        int[] intArray = {1, 1, 1, 1};
        Array array = new Array(intArray);
        int[] expected = {1, 1, 1, 1};

        array.sortDesc();

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testSortDescWithNegativeNumbers() {
        int[] intArray = {5, 4, -10, 2, 1};
        Array array = new Array(intArray);
        int[] expected = {5, 4, 2, 1, -10};

        array.sortDesc();

        assertArrayEquals(expected, array.toArray());
    }

    @Test
    void testHaveJustOneElementTrue() {
        int[] intArray = {999};
        Array array = new Array(intArray);

        assertTrue(array.haveJustOneElement());
    }

    @Test
    void testHaveJustOneElementFalse() {
        int[] intArray = {999, 1};
        Array array = new Array(intArray);

        assertFalse(array.haveJustOneElement());
    }

    @Test
    void testThrowEmptyArrayAllElementsAreEven() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.allElementsAreEven());
    }

    @Test
    void testAllElementsAreEvenFalse() {
        int[] intArray = {5, 4, -10, 2, 1};
        Array array = new Array(intArray);

        assertFalse(array.allElementsAreEven());
    }

    @Test
    void testAllElementsAreEvenFalseWithZero() {
        int[] intArray = {2, 4, -10, 0, 6};
        Array array = new Array(intArray);

        assertFalse(array.allElementsAreEven());
    }

    @Test
    void testAllElementsAreEvenTrue() {
        int[] intArray = {2, 4, -10, 2, 6};
        Array array = new Array(intArray);

        assertTrue(array.allElementsAreEven());
    }

    @Test
    void testThrowEmptyArrayAllElementsAreOdd() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.allElementsAreOdd());
    }

    @Test
    void testAllElementsAreOddFalse() {
        int[] intArray = {5, 4, -10, 2, 1};
        Array array = new Array(intArray);

        assertFalse(array.allElementsAreOdd());
    }

    @Test
    void testAllElementsAreOddFalseWithZero() {
        int[] intArray = {1, 3, -9, 0, 5};
        Array array = new Array(intArray);

        assertFalse(array.allElementsAreOdd());
    }

    @Test
    void testAllElementsAreOddTrue() {
        int[] intArray = {1, 3, -9, 3, 5};
        Array array = new Array(intArray);

        assertTrue(array.allElementsAreOdd());
    }

    @Test
    void testThrowHaveDuplicatesEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.haveDuplicates());
    }

    @Test
    void testHaveDuplicatesTrue() {
        int[] intArray = {1, 0, -9, 0, 5};
        Array array = new Array(intArray);

        assertTrue(array.haveDuplicates());
    }

    @Test
    void testHaveDuplicatesFalse() {
        int[] intArray = {1, 0, -9, 2, 5};
        Array array = new Array(intArray);

        assertFalse(array.haveDuplicates());
    }

    @Test
    void testHaveDuplicatesFalseOneElement() {
        int[] intArray = {1};
        Array array = new Array(intArray);

        assertFalse(array.haveDuplicates());
    }

    @Test
    void testThrowElementsBiggerThanTheMeanLengthEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsBiggerThanTheMeanLength());
    }

    @Test
    void testElementsBiggerThanTheMeanLength() {
        int[] intArray = {1, 20, 20, 20, 300};
        Array array = new Array(intArray);
        int[] expected = {300};

        int[] result = array.elementsBiggerThanTheMeanLength();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsBiggerThanTheMeanLengthMoreThanOne() {
        int[] intArray = {1, 20, 20, 30, 40, 50, 250, 300, 350};
        Array array = new Array(intArray);
        int[] expected = {250, 300, 350};

        int[] result = array.elementsBiggerThanTheMeanLength();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsBiggerThanTheMeanWithOneElement() {
        int[] intArray = {1};
        Array array = new Array(intArray);
        int[] expected = {};

        int[] result = array.elementsBiggerThanTheMeanLength();

        assertArrayEquals(expected, result);
    }

    // To test While public
    /*@Test
    void testPercentageOfEvenDigits() {
        int number = 12;
        Array array = new Array();
        double expected = 0.5;

        double result = array.percentageOfEvenDigits(number);

        assertEquals(expected, result, 0.001);
    }*/

    @Test
    void testThrowElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElementsEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements());
    }

    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElementsEmptyArray() {
        int[] intsArray = {1, 12, 22, 21};
        Array array = new Array(intsArray);
        int[] expected = {22};

        int[] result = array.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements();

        assertArrayEquals(expected, result);

    }

    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElementsEmptyArrayWithZero() {
        int[] intsArray = {1, 12, 0, 21};
        Array array = new Array(intsArray);
        int[] expected = {12, 21};

        int[] result = array.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElementsEmptyArrayWithNoEvenDigits() {
        int[] intsArray = {1, 13, 0, 51};
        Array array = new Array(intsArray);
        int[] expected = {};

        int[] result = array.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElementsEmptyArrayWithNegativeElements() {
        int[] intsArray = {1, 12, 22, 21, -24};
        Array array = new Array(intsArray);
        int[] expected = {22, -24};

        int[] result = array.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfDigitsOfAllEvenElements();

        assertArrayEquals(expected, result);

    }

    /**
     * Tests u)
     * elementsWithAllEvenDigits
     */

    @Test
    void testThrowElementsWithAllEvenDigitsEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithAllEvenDigits());
    }

    @Test
    void testElementsWithAllEvenDigits() {
        int[] ints = {10, 22, 37, 41, 62};
        Array array = new Array(ints);
        int[] expected = {22, 62};

        int[] result = array.elementsWithAllEvenDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAllEvenDigitsWithNoAllEvenElemtns() {
        int[] ints = {10, 27, 37, 41, 69};
        Array array = new Array(ints);
        int[] expected = {};

        int[] result = array.elementsWithAllEvenDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAllEvenDigitsWithAllElemntsInNewArray() {
        int[] ints = {24, 44, 68, 60, 86};
        Array array = new Array(ints);
        int[] expected = {24, 44, 68, 60, 86};

        int[] result = array.elementsWithAllEvenDigits();

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Tests v)
     * elementsWhithAscSequenceOfDigits
     */

    @Test
    void testThrowElementsWhithAscSequenceOfDigitsEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsWhithAscSequenceOfDigits());
    }

    @Test
    void testElementsWhithAscSequenceOfDigits() {
        int[] ints = {23, 101, 521, 123, 429};
        Array array = new Array(ints);
        int[] expected = {23, 123};

        int[] result = array.elementsWhithAscSequenceOfDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWhithAscSequenceOfDigitsWithNoAscSequence() {
        int[] ints = {21, 101, 521, 143, 429};
        Array array = new Array(ints);
        int[] expected = {};

        int[] result = array.elementsWhithAscSequenceOfDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWhithAscSequenceOfDigitsWithAllAscSequence() {
        int[] ints = {12, 123, 56789, 123456789, 2};
        Array array = new Array(ints);
        int[] expected = {12, 123, 56789, 123456789, 2};

        int[] result = array.elementsWhithAscSequenceOfDigits();

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Test w)
     * CapicuaInArray
     */

    @Test
    void testThrowCapicuaInArrayEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.capicuaInArray());
    }

    @Test
    void testCapicuaInArray() {
        int[] ints = {312, 303, 1235, 8008};
        Array array = new Array(ints);
        int[] expected = {303, 8008};

        int[] result = array.capicuaInArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void testCapicuaInArrayWithNoCapicuas() {
        int[] ints = {312, 342, 1235, 8208};
        Array array = new Array(ints);
        int[] expected = {};

        int[] result = array.capicuaInArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void testCapicuaInArrayWithAllCapicuas() {
        int[] ints = {1, 11, 202, 808808};
        Array array = new Array(ints);
        int[] expected = {1, 11, 202, 808808};

        int[] result = array.capicuaInArray();

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Test x)
     * ElementsWithAllSameDigits
     */

    @Test
    void testThrowElementsWithAllSameDigitsEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithAllSameDigits());
    }

    @Test
    void testElementsWithAllSameDigits() {
        int[] ints = {21, 44, 123, 444, 4567, 1111111};
        Array array = new Array(ints);
        int[] expected = {44, 444, 1111111};

        int[] result = array.elementsWithAllSameDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAllSameDigitsWithNoElementsWithAllSameDigits() {
        int[] ints = {21, 14, 123, 434, 4567, 11211111};
        Array array = new Array(ints);
        int[] expected = {};

        int[] result = array.elementsWithAllSameDigits();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAllSameDigitsWithAllElementsWithAllSameDigits() {
        int[] ints = {22, 44, 555, 444, 999999, 11111111};
        Array array = new Array(ints);
        int[] expected = {22, 44, 555, 444, 999999, 11111111};

        int[] result = array.elementsWithAllSameDigits();

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Test y)
     * ElementsNotArmstrong
     */

    @Test
    void testThrowElementsNotArmstrongEmptyArray() {
        Array array = new Array();

        assertThrows(IllegalArgumentException.class, () -> array.elementsNotArmstrong());
    }

    @Test
    void testElementsNotArmstrong() {
        int[] ints = {100, 153, 241, 370, 1023, 1634};
        Array array = new Array(ints);
        int[] expected = {100, 241, 1023};

        int[] result = array.elementsNotArmstrong();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsNotArmstrongWithAllArmstrong() {
        int[] ints = {371, 153, 407, 370, 1634};
        Array array = new Array(ints);
        int[] expected = {};

        int[] result = array.elementsNotArmstrong();

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsNotArmstrongWithAllNotArmstrong() {
        int[] ints = {341, 1512, 487, 3412421, 12314};
        Array array = new Array(ints);
        int[] expected = {341, 1512, 487, 3412421, 12314};

        int[] result = array.elementsNotArmstrong();

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Test z)
     * ElementsWithAscSequenceOfDigitsOfAtLeastNDigits
     */

    @Test
    void testThrowElementsWithAscSequenceOfDigitsOfAtLeastNDigitsEmptyArray() {
        Array array = new Array();
        int numberOfDigits = 1;

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits));
    }

    @Test
    void testThrowElementsWithAscSequenceOfDigitsOfAtLeastNDigitsNumberNotBiggerThanZeroWithZero() {
        int[] ints = {1, 2, 3, 4, 5};
        Array array = new Array(ints);
        int numberOfDigits = 0;

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits));
    }

    @Test
    void testThrowElementsWithAscSequenceOfDigitsOfAtLeastNDigitsNumberNotBiggerThanZeroWithNegativeOne() {
        int[] ints = {1, 2, 3, 4, 5};
        Array array = new Array(ints);
        int numberOfDigits = -1;

        assertThrows(IllegalArgumentException.class, () -> array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits));
    }

    @Test
    void testElementsWithAscSequenceOfDigitsOfAtLeastNDigits() {
        int[] ints = {1, 12, 123, 1234, 12345};
        Array array = new Array(ints);
        int numberOfDigits = 3;
        int[] expected = {123, 1234, 12345};

        int[] result = array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits);

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAscSequenceOfDigitsOfAtLeastNDigitsTwo() {
        int[] ints = {1, 12, 122, 121234, 411234592};
        Array array = new Array(ints);
        int numberOfDigits = 3;
        int[] expected = {121234, 411234592};

        int[] result = array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits);

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAscSequenceOfDigitsOfAtLeastNDigitsNoElements() {
        int[] ints = {1, 12, 122, 124, 41};
        Array array = new Array(ints);
        int numberOfDigits = 4;
        int[] expected = {};

        int[] result = array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits);

        assertArrayEquals(expected, result);
    }

    @Test
    void testElementsWithAscSequenceOfDigitsOfAtLeastNDigitsAllElements() {
        int[] ints = {1, 12, 122, 121234, 411234592};
        Array array = new Array(ints);
        int numberOfDigits = 1;
        int[] expected = {1, 12, 122, 121234, 411234592};

        int[] result = array.elementsWithAscSequenceOfDigitsOfAtLeastNDigits(numberOfDigits);

        assertArrayEquals(expected, result);
        assertNotSame(array.toArray(), result);
    }

    /**
     * Test aa)
     * equals
     */



    @Test
    void testEqualsTrue() {
        int[] argumentArray = {1, 2, 3, 4};
        int[] ints = {1, 2, 3, 4};
        Array array = new Array(ints);
        Array array1 = new Array(argumentArray);


        assertEquals(array1, array);
        assertNotSame(array.toArray(), ints);
    }

    @Test
    void testEqualsFalse() {
        int[] argumentArray = {1, 3, 3, 4};
        int[] ints = {1, 2, 3, 4};
        Array array = new Array(ints);


        assertFalse(array.equals(argumentArray));
        assertNotSame(array.toArray(), ints);
    }

    @Test
    void testEqualsFalseDiferentLength() {
        int[] argumentArray = {1, 3, 3, 4};
        int[] ints = {1, 2, 3};
        Array array = new Array(ints);


        assertFalse(array.equals(argumentArray));
        assertNotSame(array.toArray(), ints);
    }

    @Test
    void testConstructorNullArray() {

        assertThrows(IllegalArgumentException.class, () -> new Array(null));
    }
}