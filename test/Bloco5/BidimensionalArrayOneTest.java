package Bloco5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BidimensionalArrayOneTest {

    /**
     * Test Constructors
     */

    @Test
    void testConstructorNotEmpty() {
        int[][] ints = {
                {1,2,3},
                {1,2,3}
        };
        
        BidimensionalArrayOne bidimensionalArrayOne = new BidimensionalArrayOne(ints);
        
        assertArrayEquals(ints, bidimensionalArrayOne.getMatrix());
    }

    @Test
    void testConstructorEmpty() {
        int[][] expected = {};

        BidimensionalArrayOne bidimensionalArrayOne = new BidimensionalArrayOne();

        assertArrayEquals(expected, bidimensionalArrayOne.getMatrix());
        assertNotNull(bidimensionalArrayOne);
    }

    /**
     * Test Add Element to Row
     */
    /*
    @Test
    void testAddElementToRow() {
        int[][] ints = {
                {1,2,3},
                {1,2,3}
        };
        int element = 1;
        int row = 1;
        int[][] expected = {
                {1,2,3},
                {1,2,3,1}
        };

        BidimensionalArrayOne bidimensionalArrayOne = new BidimensionalArrayOne(ints);
        bidimensionalArrayOne.addElementToRow(row, element);

        assertArrayEquals(expected, bidimensionalArrayOne.getMatrix());
    }

     */
}