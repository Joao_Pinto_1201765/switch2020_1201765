package Bloco5.SopaDeLetras;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameGrideTest {

    @Test
    void testConstructor() {
        char[][] gameGrid = {
                {'A', 'B'},
                {'A', 'B'}
        };
        char[][] expected = {
                {'A', 'B'},
                {'A', 'B'}
        };
        GameGride gameGride = new GameGride(gameGrid);

        char[][] result = gameGride.toMatrix();

        assertArrayEquals(expected, result);
        assertNotSame(expected, result);
    }

    @Test
    void testConstructorThrowEmptyCharMatrix() {
        char[][] gameGrid = {};

        assertThrows(IllegalArgumentException.class, () -> new GameGride(gameGrid));
    }

    @Test
    void testConstructorThrowInitialGridNotSquared() {
        char[][] gameGrid = {
                {'Q'},
                {'A', 'T'}
        };

        assertThrows(IllegalArgumentException.class, () -> new GameGride(gameGrid));
    }

    @Test
    void length() {
        char[][] gameGrid = {
                {'A', 'B'},
                {'A', 'B'}
        };
        GameGride gameGride = new GameGride(gameGrid);
        int expected = 2;

        int result = gameGride.length();

        assertEquals(expected, result);

    }

    @Test
    void columnLength() {
        char[][] gameGrid = {
                {'A', 'B'},
                {'A', 'B'}
        };
        GameGride gameGride = new GameGride(gameGrid);
        int expected = 2;

        int result = gameGride.columnLength();

        assertEquals(expected, result);
    }

    @Test
    void charInPosition() {
        char[][] gameGrid = {
                {'A', 'B'},
                {'A', 'C'}
        };
        GameGride gameGride = new GameGride(gameGrid);
        char expected = 'C';
        int row = 1;
        int column = 1;

        char result = gameGride.charInPosition(row, column);

        assertEquals(expected, result);
    }
}