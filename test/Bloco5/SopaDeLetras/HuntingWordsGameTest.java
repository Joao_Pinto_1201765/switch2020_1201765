package Bloco5.SopaDeLetras;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HuntingWordsGameTest {

    @Test
    void testConstructor() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        GameGride gameGride = new GameGride(matrizLetras);
        ValidWords validWords = new ValidWords(palavras);

        char[][] expected = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };

        HuntingWordsGame huntingWordsGame = new HuntingWordsGame(validWords, gameGride);

        char[][] result = huntingWordsGame.getGameGride();

        assertArrayEquals(expected, result);
    }

    @Test
    void numberOfWordsLeft() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        GameGride gameGride = new GameGride(matrizLetras);
        ValidWords validWords = new ValidWords(palavras);
        HuntingWordsGame huntingWordsGame = new HuntingWordsGame(validWords, gameGride);

        int expected = 4;

        int result = huntingWordsGame.numberOfWordsLeft();

        assertEquals(expected, result);
    }

    @Test
    void sequenceIsValid() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        GameGride gameGride = new GameGride(matrizLetras);
        ValidWords validWords = new ValidWords(palavras);
        HuntingWordsGame huntingWordsGame = new HuntingWordsGame(validWords, gameGride);

        boolean result = huntingWordsGame.sequenceIsValid(1,1,4,1);

        assertTrue(result);
    }

    @Test
    void sequenceIsValidFalse() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        GameGride gameGride = new GameGride(matrizLetras);
        ValidWords validWords = new ValidWords(palavras);
        HuntingWordsGame huntingWordsGame = new HuntingWordsGame(validWords, gameGride);

        boolean result = huntingWordsGame.sequenceIsValid(1,1,1,4);

        assertFalse(result);
    }

    @Test
    void testDecreseInNumberOfWordsLeft() {
        char[][] matrizLetras = {
                {'A', 'B', 'C', 'Q', 'B', 'Z'},
                {'D', 'F', 'G', 'C', 'L', 'C'},
                {'H', 'I', 'J', 'O', 'W', 'R'},
                {'A', 'G', 'Q', 'B', 'L', 'H'},
                {'A', 'O', 'C', 'R', 'B', 'A'},
                {'B', 'U', 'J', 'A', 'W', 'R'}
        };
        String[] palavras = {"FIGO", "OLA", "COBRA", "BUJA"};

        int expected = 3;
        int expecetPrimaryResult = 4;

        GameGride gameGride = new GameGride(matrizLetras);
        ValidWords validWords = new ValidWords(palavras);
        HuntingWordsGame huntingWordsGame = new HuntingWordsGame(validWords, gameGride);
        int primaryResul = huntingWordsGame.numberOfWordsLeft();

        huntingWordsGame.sequenceIsValid(1,1,4,1);

        int resul = huntingWordsGame.numberOfWordsLeft();

        assertEquals(expected,resul);
        assertEquals(expecetPrimaryResult, primaryResul);
    }
}