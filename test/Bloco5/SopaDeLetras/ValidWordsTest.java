package Bloco5.SopaDeLetras;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidWordsTest {

    @Test
    void toArray() {
        String[] words = {"OLA", "ADEUS", "ATE", "UM", "DIA"};
        ValidWords validWords = new ValidWords(words);
        String[] expected = {"OLA", "ADEUS", "ATE", "UM", "DIA"};

        String[] result = validWords.toArray();

        assertArrayEquals(expected, result);
        assertNotSame(expected, result);
    }

    @Test
    void removeWord() {
        String[] words = {"OLA", "ADEUS", "ATE", "UM", "DIA"};
        ValidWords validWords = new ValidWords(words);
        String word = "UM";
        String[] expected = {"OLA", "ADEUS", "ATE", "DIA"};

        validWords.removeWord(word);

        assertArrayEquals(expected, validWords.toArray());
    }

    @Test
    void length() {
        String[] words = {"OLA", "ADEUS", "ATE", "UM", "DIA"};
        ValidWords validWords = new ValidWords(words);
        int expected = 5;

        int result = validWords.length();

        assertEquals(expected, result);
    }

    @Test
    void position() {
        String[] words = {"OLA", "ADEUS", "ATE", "UM", "DIA"};
        ValidWords validWords = new ValidWords(words);
        int position = 1;
        String expected = "ADEUS";

        String result = validWords.position(position);

        assertEquals(expected, result);
    }

    @Test
    void testeExistInStringArrayFalse() {
        String[] words = {"OLA", "ADEUS", "ATE", "UM", "DIA"};
        ValidWords validWords = new ValidWords(words);
        String word = "ORA";

        boolean result = validWords.existInStringArray(word);

        assertFalse(result);
    }

    @Test
    void testThrowIsEmpty() {
        String[] words = {};

        assertThrows(IllegalArgumentException.class, () -> {ValidWords validWords = new ValidWords(words);});
    }

    @Test
    void testThrowIsEmptyTwo() {
        String[] words = null;

        assertThrows(IllegalArgumentException.class, () ->  new ValidWords(words));
    }
}