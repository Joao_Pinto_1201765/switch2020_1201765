package OOPIniciacao;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class StudentListTest {

    @Test
    public void addWhenNotEmptyButDifferent() {
        // Arrange
        Student student1 = new Student(12000, "Moreira");
        Student student2 = new Student(12001, "Sampaio");
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        // Act
        boolean result = stList.addStudent(student2);
        Student[] content = stList.toArray();

        //Assert
        Assert.assertTrue(result);
        Assert.assertEquals(2, content.length);
    }

    @Test
    public void addIngSameStudentTwice() {
        // Arrange
        Student student1 = new Student(12003, "Moreira");
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        // Act
        boolean result = stList.addStudent(student1);
        Student[] content = stList.toArray();

        //Assert
        Assert.assertFalse(result);
        Assert.assertEquals(1, content.length);
    }

    @Test
    public void addIngStudentWithSameNumber() {
        // Arrange
        Student student1 = new Student(12003, "Moreira");
        Student student2 = new Student(12003, "Sampaio");
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        // Act
        boolean result = stList.addStudent(student2);
        Student[] content = stList.toArray();

        //Assert
        Assert.assertFalse(result);
        Assert.assertEquals(1, content.length);
    }

    @Test
    public void addIngNull() {
        // Arrange
        Student student1 = new Student(12007, "Moreira");
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        // Act
        boolean result = stList.addStudent(null);
        Student[] content = stList.toArray();

        //Assert
        Assert.assertFalse(result);
        Assert.assertEquals(1, content.length);
    }

    @Test
    public void removeIngSameStudentTwice() {
        // Arrange
        Student student1 = new Student(12000, "Moreira");
        StudentList stList = new StudentList();
        stList.addStudent(student1);
        // Act
        boolean result1 = stList.removeStudent(student1);
        boolean result2 = stList.removeStudent(student1);
        Student[] content = stList.toArray();
        //Assert
        Assert.assertTrue(result1);
        Assert.assertFalse(result2);
        Assert.assertEquals(0, content.length);
    }

    @Test
    public void removeIngMiddleStudentInSeveral() {
        // Arrange
        Student student1 = new Student(12345, "Moreira");
        Student student2 = new Student(12346, "Sampaio");
        Student student3 = new Student(12347, "Costa");
        Student student4 = new Student(12348, "Lidia");
        Student student5 = new Student(12349, "Maria");

        Student[] all = {student1, student2, student3, student4, student5};
        Student[] expected = {student1, student2, student3, student5};
        StudentList stList = new StudentList(all);
        // Act
        boolean result = stList.removeStudent(student4);
        Student[] content = stList.toArray();
        //Assert
        Assert.assertTrue(result);
        Assert.assertArrayEquals(expected, content);
    }
}