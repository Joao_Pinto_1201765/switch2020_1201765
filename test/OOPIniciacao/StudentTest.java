package OOPIniciacao;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StudentTest {

    @Test
    void testeCriacaoDeStudentComNumeroInvalido() {
        int number = 1;
        String name = "Alexandre";

        assertThrows(IllegalArgumentException.class, () -> new Student(number, name));

    }

    @Test
    void testeCriacaoDeStudentComNomeInvalido() {
        int number = 11234;
        String name = "Toni";

        assertThrows(IllegalArgumentException.class, () -> new Student(number, name));

    }

    @Test
    void testeCriacaoDeStudentComNomeInvalidoTestarMensagem() {
        int number = 11234;
        String name = "Toni";

        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));

        String expected = "Invalid Name";

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testGetNumber() {
        Student st1 = new Student(12345, "Zé Maria");
        int expected = 12345;

        int result = st1.getNumber();

        assertEquals(expected, result);
    }

    @Test
    void testSettGrade() {
        Student st1 = new Student(11234, "Zé Maria");
        st1.setGrade(1);

        int result = st1.getGrade();

        assertEquals(1, result);
    }

    @Test
    void testSetGradeExeption() {
        Student st = new Student(10001, "Samuel");
        assertThrows(IllegalArgumentException.class, () -> st.setGrade(-1));
    }

    @Test
    void testCompareByNumber() {
        Student st = new Student(10001, "Samuel");
        Student st2 = new Student(50000, "Cristina");

        Integer expected = -1;

        Integer result = st.compareToByNumber(st2);


        assertEquals(expected, result);
    }

    @Test
    void testeGetName() {
        Student st = new Student(12345, "Avó Zeca");

        String expect = "Avó Zeca";

        assertEquals(expect, st.getName());
    }
}